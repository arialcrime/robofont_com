---
layout: page
title: Licensing Options
menuOrder: 3
---

* Table of Contents
{:toc}

RoboFont offers 4 different kinds of licenses:

<table>
  <tr>
    <th width='32%'>license type</th>
    <th width='23%'>valid for…</th>
    <th width='18%'>extendable?</th>
    <th width='27%'>payment mode</th>
  </tr>
  <tr>
    <td><a href='#trial-license'>Trial License</a></td>
    <td>30 days</td>
    <td><span class='red'>no</span></td>
    <td>free</td>
  </tr>
  <tr>
    <td><a href='#student-license'>Student License</a></td>
    <td>one year</td>
    <td><span class='red'>no</span></td>
    <td>free</td>
  </tr>
  <tr>
    <td><a href='#student-license-service'>Student License Service</a></td>
    <td>one year</td>
    <td><span class='green'>yes</span></td>
    <td>yearly subscription</td>
  </tr>
  <tr>
    <td><a href='#full-license'>Full License</a></td>
    <td>unlimited time</td>
    <td>—</td>
    <td>one-time fee</td>
  </tr>
</table>

Each type of license is described in detail below.

> - {% internallink 'eula' %}
> - {% internallink 'documentation/workspace/license-window' %}
{: .seealso }

## Trial License

The Trial License allows anyone to try out and evaluate a fully functional version of RoboFont, for free, for a limited period of time (30 days).

**The evaluation period cannot be extended.**

The app can be downloaded [here][Download], or using the *Download* button in the homepage.

> To continue using RoboFont after the evaluation period is completed, please upgrade to a [Full license](#full-license).
{: .note }

[Download]: http://static.typemytype.com/robofont/RoboFont.dmg

## Educational Licenses

### Student License

**RoboFont supports education by providing a free one-year license for students.**

Student Licenses can be requested by teachers (not by students) using the [Student License Form].

Once the request is approved, teachers will receive a customized license which they can distribute to their students.

> **Student licenses cannot be extended.** See the [Student License Service](#student-license-service) if your students need RoboFont for more than one year.
{: .note }

[Student License Form]: http://education.robofont.com/page/student-license-form/

### Student License Service

The Student License Service is a subscription-based service which gives teachers and school administrators more flexibility and control over student licenses.

For more information, see [Student License Service].

[Student License Service]: http://education.robofont.com/page/educational-licenses/

## Full License

The Full License is an **individual license** which gives one user full access to RoboFont.

A Full License can be purchased [here][Buy], or using the *Buy* button in the homepage.

> Payment is made via the [FastSpring] platform, which supports credit cards, PayPal, and bank transfers.
{: .note }

[Buy]: http://sites.fastspring.com/typemytype/instant/robofont
[FastSpring]: http://fastspring.com/

### Volume Discounts

If you wish to order a large amount of Full Licenses at once (for example, to equip a large studio or research institution), the following volume discounts are provided:

<table>
  <tr>
    <th>quantity</th>
    <th>unit price*</th>
    <th>discount</th>
  </tr>
  <tr>
    <td>1-10 license(s)</td>
    <td>€ 400</td>
    <td>—</td>
  </tr>
  <tr>
    <td>+10 licenses</td>
    <td>€ 300</td>
    <td>-25%</td>
  </tr>
  <tr>
    <td>+20 licenses</td>
    <td>€ 240</td>
    <td>-40%</td>
  </tr>
  <tr>
    <td>+30 licenses</td>
    <td>€ 200</td>
    <td>-50%</td>
  </tr>
  <tr>
    <td>+75 licenses</td>
    <td>€ 160</td>
    <td>-60%</td>
  </tr>
</table>

*\* the unit price will be multiplied by the number of licenses*

### Upgrade Discounts

Users who bought a RoboFont 1.* license after the 8th of March 2017 get a full discount on RoboFont 3. [Get in touch](../contact) with your order number for more details.

## Resellers

> **Reseller discounts are not available.** RoboFont licenses must be acquired directly from the developer.
{: .warning .no-title }
