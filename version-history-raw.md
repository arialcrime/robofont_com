{%comment%}
last updated: build 1910091627

- Fixing bug a glyph could be None in GlyphPreview. (?)
- Flip image while drawing inside a glyph cell. (?)
- Add extension to dropped images. (?)
- Adding notarizing. (?)

{%endcomment%}

## RoboFont 3.3b

* **Glyph Editor**
    - Adding support for individual guideline colors and anchor colors.
    - Keep constraints with Shift down while skewing.
    - Clear the whole glyph when copying as component.
    - Improved removal of off-curve points.
    - Improved preserving point smoothness when deleting points.
    - Don’t smooth line corner points.
    - Snap anchors to on-curve points while dragging.
    - Fixed component sheet when adding a component to a layer.
    - Improved display of anchor coordinates.
    - Fixed the dashed dragging lines in the Glyph Editor.
    - Fixed open contour converting to curve segment.
    - Improved removing trailing off curve point.
    - After a reselect, restore the current mouse down point.
    - Ignore open contours on union (booleanOperations).
    - Don’t remove the identifiers when joining two contours.
    - Remove identifier when joining a single contour.
    - Improved setting points to ‘smooth’ after removing line-curve combo.
    - Move anchors inside empty glyphs when spacing.
* **Font Overview**
    - Preserve glyph order after removing a glyph without pressing Alt.
    - Press Ctrl + delete to convert to a template glyph and remove all layers.
    - Keep the glyph order after external changes.
    - Adding support for creating missing glyphs when sorting by character set.
    - Prevent overwriting glyph names in list mode.
* **Space Center**
    - Adding tracking to the Space Center.
    - Improved single line drawing in Space Center.
    - Set correct layer while setting a font in the Space Center.
    - Improved modifying empty glyphs behavior in Space Center.
    - Update the Glyph Line View in a Space Center after setting a font.
* **Inspector**
    - Adding a new *Guideline* panel to the Inspector.
    - Improve switching between global and local guidelines using the Inspector.
    - Improved color cells in Inspector window.
    - Fixed bug when removing a guideline in the guideline pane.
* **Kern Center**
    - Allow using single glyphs when creating kerning pairs in the Kern Center.
    - Prefer group kerning when adding a fresh pair, look up in existing groups.
    - Don’t allow duplicated glyphs in kerning groups.
* **Scripting Window**
    - Improved closing empty scripting document.
    - When the Scripting Window is empty, reset the change count.
* **Preferences**
    - Adding `glyphViewCanZoomWhileDragging` preference setting.
    - Fixing typo in `PyDEBackgroundColor` preference setting.
    - Moving background colors for index labels in the Glyph View (point, segment, contour, anchor, component) to the Preferences.
    - Escape unicode character descriptions in the Preferences Editor.
    - Ignore empty menu shortcuts in the Preferences Editor.
* **Extensions**
    - Adding support for expiration date in `ExtensionBundle` and `ExtensionBuilder`.
    - Adding support for Markdown files (`.md`) as source format for extension docs.
    - Adding support for custom `style.css` when building markdown extension docs.
    - Don’t show a popup when the user cancelled the installation of an extension.
    - Improved extension banners.
    - Improved picking the correct Python version for extensions.
    - Adding folder naming scheme `lib.x.y` to support different versions of Python in compiled `.pyc` extensions.
* **APIs**
    - Upgrading embedded Python to 3.7. <span class='red'>Warning: all binary extensions (MetricsMachine, Prepolator) will become invalid and must be upgraded.</span>
    - Adding support for drag & drop in a Canvas view.
    - Updating embedded AFDKO.
    - Switching to `WKWebView` as the default HTMLView.
    - Adding support for notification banners in `mojo.UI.PostBannerNotification`.
    - Adding `RGlyph.holdChanges()` to hold and release all notifications.
    - Adding `showProgress` keyword to `font.testIntall`.
    - Adding `centerInView` for `StatusInteractivePopUpWindow`.
    - Improved point labels: tuples, use `point.clearLabels()` to remove them.
    - Adding `allowsMovingMargins` for a subclass of the `EditingTool`.
    - Make `dragTransformMatrix` available in a subclass of the `EditingTool`.
    - Fixing `ufo2fdk`’s glyph name normalizer.
    - Improved `fontParts` and `mojo` dialogs.
    - Adding `GlyphSequenceEditText` and `splitText` to `mojo.UI`.
    - Adding docstrings.
    - Removing `>>>` from docstrings.
    - Improved FontParts glyph name in font logic for template glyphs.
    - Return tuples for `font.templateSelectedGlyphNames`.
    - Adding support for FontParts’ `fileStructure` argument.
    - Change `applicationDidFinishLaunching_` to `applicationWillFinishLaunching_`.
    - Switching to experimental `vanilla` branch with support for auto layout.
    - Embedding `markdown` module.
    - Improved support for `glyphWindow.setGlyph(glyph)`.
    - Improved FontParts’ `hasOverlaps` function.
    - Adding tracking areas to `Canvas` to support `mouseEntered` and `mouseExited` events.
    - Embedding `fontmake`.
    - Adding support for `pytest` in the Scripting Window.
    - Adding `renameGlyph` test.
    - Add `getQueryResult` for SmartSet objects to retrieve the result of a query.
    - Read `.py` files as utf-8 to parse the menu title name.
    - Removing `__future__` and `fontTools.misc.py23`.
    - Removing all `PY2` and `PY3` variables.
    - Change `basestring` to `str` (py3).
    - Change `unichr` to `chr` (py3).
    - Adding a helper to trace where a `print` statement is coming from.
    - Don’t embed standalone `ufoLib` anymore, as this could lead to errors in extensions.
    - Improved docstrings for `getDisplayOption` and `setDisplayOption`.
    - Improved error message when an alias cannot be resolved.
    - Improved and fixed `layer.setDisplayOptions()`, must be a dict now.
    - Add the FontParts document to the document controller even when no UI is provided, so it ends up in `AllFonts` and `CurrentFont`.
    - Switching to FontTools’ `otRound` to round coordinates.
    - `font.glyphOrder` changes automatically when a glyph is renamed by notification.
    - `font.glyphOrder` must return a tuple.
    - Adding glyphs to `stemHist` callback.
    - `GlyphPreview` can preview anything that looks like a glyph object and works with a pen.
    - Use glyph objects to check if a collection view contains a glyph.
    - Don’t iterate and change a dict at the same time while renaming in kerning and groups.
    - Ignore empty glyphs in `font.bounds`.
    - Adding `allObservers` to `mojo.events`.
    - Updating the code editor’s lexer to Python 3.
* **Bugs**
    - Fixing `AccordionView` display bug in macOs 10.14.
    - Fixing issue with jumping to line number in a code editor.
    - Set the unicode formatter correctly (internal macOS change).
    - Don’t crash hard when something fails while drawing in a multiline scrollview.
    - `Canvas` now sends the correct pasteboard types.
* **Misc.**
    - Improved extracting data from AGL/GNFUL lists.
    - Adding XML glyph diff for External Changes window.
    - Prevent *Open script* menu items if the shortcut key is already taken.
    - Improved setting mark color swatches in search fields.
    - Allow loading arbitrary external data (for example external help files).
    - Improved preserving identifiers while using undo/redo.
    - Remove identifiers from removed points while inserting a point.
    - Point labels are a list, not a dictionary.
    - Reset selection on transformation of a FontParts object.
    - Adding a friendly warning when deleting a Smart Set.
    - Improved required `font.info` settings.
    - Improved *Revert to Saved* dialog.
    - Build template glyphs only if they don’t exist yet.
    - Raise `KeyError` when the glyph name is not found.
    - Improved shell script so that the application file can be renamed.
    - No release mode for `.ttf` export.
    - Switching Accordion View to a flat UI style.
    - Fixed external save inside RF to prevent an External Changes pop-up window when saving from the menu.
    - Fixed layer order when jumping to next layer in Space Center.
    - Set layer color based on the Preferences settings (*foreground* and *background* only).
    - Set modification date in internal document when saving programatically.
    - Don’t use the AppKit localization.
    - Remove guideline lib keys when removing the guide.
    - Improved tests.
    - Move anchors in empty glyphs when spacing.
    - Update the file browser only when it becomes visible (RoboFont becomes active).
    - Set auto save name for HTML window.
    - Renaming ambiguous menu title: *New Sets Folder*.
    - Fixing conversion of groups with group names identical to UFO2.
    - The application is now [notarized] as required by Apple for macOS 10.15+.

[notarized]: http://developer.apple.com/documentation/xcode/notarizing_your_app_before_distribution?language=objc

## RoboFont 3.2

* **Font Overview**
    * Show layer contents even if a glyph is not present in the default layer.
    * Show all glyphs in all layers, even if they are not listed in the glyph order.
    * Alt+delete a glyph will remove the glyph from all layers.
    * Copy unicodes when pasting a glyph in a glyph cell.
    * `font.glyphOrder` has precedence over sort descriptors.
    * Don’t update the window header when the saving path could be `None`.
* **Glyph Editor**
    * Improved drawing speed.
    * Improved display of anchors, labels, component info box, outline errors, anchor point coordinates.
    * Improved updating components when the base glyph has changed.
    * Improved measurements on open paths.
    * Move image when dragging side bearings.
    * Adding option to copy metrics in Layer popup window (shortcut key `l`).
    * Improved copy and swap to layers.
    * Draw layer image before checking if the glyph is empty.
    * Improved setting of default layers and updating related views.
    * While setting `glyph.width`, flag glyph as changed if it’s a template and if the width is the same.
    * Fixing flipping unicodes when flipping glyph layers.
    * Fixing drawing image from layer with an alpha value.
    * Fixing shift image when changing left margin.
    * Reuse/cache `NSAttributedString` and `NSBezierPath` objects.
    * Don’t update ruler views everytime the glyph view is refreshed.
    * Fixing font size issue and drawing of measurements.
    * Improved drawing of guidelines and end points.
* **Space Center**
    * Update Space Center input field when toggling beam with a script.
    * Improved Space Center updating and performance.
* **Code editor**
    * Improved emoji support.
    * Improved auto completion.
    * Disable auto replacements in a code editor.
    * Check if a file exists when opening the file browser in the Scripting Window.
* **Preferences**
    * Adding `glyphViewDrawLayerBackgroundColor` to disable layer background color.
    * Adding `normalizeWriteModTimes` default option to normalize UFOs on save.
    * Adding `checkExternalChanges` as a default setting.
    * Preferences Editor requires fallback for shortcuts like `"` → `\"` and `\` → `\\`.
    * Fixing preferences export.
    * Fixing Preferences Editor.
* **Extensions**
    * Removing old Mechanic methods from ExtensionBundle.
    * Adding icon for .mechanic files.
    * Improved Extension Bundle validation.
    * Adding support for requirements and specific versions in ExtensionBundle.
    * Load extension before setting keyboard shortcuts.
    * Load exension bundle from the extension object instance.
* **UI & UX**
    * Glyph View and Feature Editor now have a status bar instead of placard scrollviews.
    * Improved slider in menus.
    * Improved UI for 10.14.
    * New icons for Dark Mode.
    * Fixing display issues in the AccordionView.
    * Improving steppers.
* **Core**
    * Updating internal packages.
    * Improvements for macOS 10.14.
    * Moving ufoLib to `fontTools.ufoLib`.
    * Switching to [psautohint](http://github.com/adobe-type-tools/psautohint) instead of the AFDKO autohint.
    * Removed embedded library `designspaceDocument` (now included in fontTools).
    * Huge speed bump in generating font binaries!
    * Adding tests.
    * Adding `getGlyphViewHotKey` and `setGlyphViewHotKey` methods.
    * Adding `spaceCenter.getLineHeight()` and `spaceCenter.setLineHeight(value)`.
    * Improved reading of `.fea` files with non-ascii characters in comments.
    * Adding support for [Single File UFOs](http://unifiedfontobject.org/roadmap/#single-file-ufo) (UFOz).
    * Removing py2 related code.
    * Adding `uharfbuzz` as an embedded package.
    * Set `objc` to verbose, so tracebacks are comming back.
    * Print out the error message when executing tests.
    * Don’t use patterns to make gradients.
    * Don’t cache color gradients, keep a reference to the color instead.
    * Fixing copy of `glif` XML (`lxml` uses single quotes instead of double quotes).
    * Also normalize when the `font.save(path)` is `None`.
    * Solving deprecated warning and use `CoreServices` instead of `LaunchServices`.
* **FontParts**
    * Fixing call to super class method in fontParts.
    * Improved `RFont.newGlyph()` so it tries to keep the same object if the glyph already exists.
    * Adding `glyph.selectedPoints` and deprecate `glyph.selection`.
    * Use `allFonts` as keyword in `SelectFont`.
    * Update components when renaming glyphs.
    * Improved setting unicode for glyph.
    * Improved subclassing of FontParts objects.
    * Improved `contour.removeSegment()` on open paths with a trailing off curve.
    * Improved warning when provided unicode is not valid.
    * Adding `flatKerning` to fontParts.
    * Don’t overwrite `setStartSegment`.
    * Set file path in `fontParts.RFont.save` when the path is different.
    * Reset selection after transforming a FontParts object.
* **Observers etc.**
    * Set index of active tool when building the toolbar item in the Glyph Window.
    * Send refresh notification when labels are edited.
    * Fixing unwanted change notifications.
    * Adding `applicationScreenChanged` to base tool.
    * Improved sending notification while holding the preview key.
    * Wrap drawing notification in a `try/except` statement.
    * Fixing typo in notification: `will` must be `did`…
* **Bugs**
    * Fixing hidden `long` callback.
    * Fixing error on deleting points.
    * Fixing issues when setting Character Set Preferences.
    * Fixing checkbox selection issue in the Inspector Panel.
    * Fixing error when compiling TrueType fonts.
    * Fixing cursor issues while changing screens with different resolutions.
    * Fixing issue with macOS numbers localization in Space Center.
    * Fixing bug while converting colors to hex notation.
    * Fixing bug with split views in Scripting Window.
    * Fixing autohint bug.
    * Fixing issue while removing glyphs when removing from kerning and groups is enabled.
    * Fixing issue with remove overlap in booleanOperations.
    * Fixing changing shortcut when there is an alternate menu item.
    * Fixing problems with autosaving.
    * Fixing `glyphWindow.setGlyphViewScale` by force setting the scale value.
    * Fixing crash when dragging the column headers of the list view in Font Overview.
    * Fixing issue when restoring a document.
    * Fixing bug when getting external changes.

## RoboFont 3.1

* Any layer can be the top layer (removed restriction to have foreground at the top).
* Added new keyboard shortcut `Ctrl+Alt+Cmd+O` to reveal the log file in Finder.
* Improved Bezier drawing tool.
* Copy paste also copies `glyph.width`, and uses it when the destination glyph is empty.
* Don’t set image attributes in `glyph.lib` if there is no image.
* **Scripting**
    * Improved support for `contour.removePoint`.
    * EditingTool is easier to subclass and disable editing while keeping selection.
    * Added support for custom menu items in any contextual menu.
    * Added [ufoProcessor](https://github.com/LettError/ufoProcessor) (successor of designSpaceDocument) to the embedded packages.
    * Updates to [ufoLib](https://github.com/unified-font-object/ufoLib) to allow fine-grained control over UFO validation.
    * Fixing `extractEvent` by removing tuple values.
    * Added `mojo.tools.roboFontUnitTest` to unit test inside RF.
    * Added `RFont.hasChanged()`.
* **Space Center**
    * Send out `CurrentGlyphChanged` notification.
    * Fixing bug in Space Center when using `/?` and `/!`
* **Preferences**
    * Menu Preferences now shows a warning for duplicated keyboard shortcuts.
    * Added a color preference for echo paths.
    * Added new Preferences settings:
        * `spaceCenterMaxGlyphsForLiveUpdate`
        * `loadFontGlyphLimit`
        * `defaultGuidelineShowMeasurment`
        * `defaultGuidelineMagnetic`
        * `ufoLibReadValidate`
        * `ufoLibWriteValidate`
* Fixing reading `.enc` files.
* Fixing importing binary PS font files with control characters in the name recods.

## RoboFont 3.0

* Uses UFO3 as its native source format.
* Written entirely in Python 3.
* Lots of internal rewrites.
* Opens UFO files with lightning speed.
* Replaces `roboFab` with `fontParts`.
* Layers are more independent.
* Components refer to glyphs in the same layer.
* Validating and converting incoming UFO2 data:
    * Removes duplicates in kern groups.
    * Removes glyphs that occur in too many kern groups.
    * Converts RoboFont 1+ lib keys to UFO3 objects.
* **Glyph View**
    * Use Cmd + Shift to add a diagonal constrain for line segments.
    * Use Alt + click on a line segment to convert it to a curve. Click near an on-curve point to select the off-curve point.
* **Space Center**
    * Speed improvements.
    * Show guidelines and blues.
    * New input syntax: `/<glyphName>@<layerName>` to select a glyph from a given layer. For example: `/A@background`.
* **Scripting**
    * Moving `fontWillGenerate` and `fontDidGenerate` to a more internal level, so it sends a notification when generating with a script.
    * Speeding up `print()` statement.
    * Adding Cmd + period to stop a script (like in DrawBot, thanks Just!!!).
    * Autocomplete `([{}])` in the code editor.
    * Adding [glyphConstruction](http://github.com/typemytype/glyphconstruction) as a module!!
    * Renaming `sys_path_override` to `external_packages` – a folder to overwrite embedded packages.
    * Moving lots of internal modules to `mojo.*`.
    * Update of internal packages to UFO3, py3 and much more.
    * Adding `RFont.hasInterface()`.
* **Preferences**
    * Use drag-and-drop to sort Miscellaneous Colors.
    * New raw Preferences Editor (press Alt while opening the *RoboFont* menu item).
    * New *Preferences > Menus* section, listing all menu items and keyboard shortcuts.

> - [Changes from RoboFont 1.8 to 3]({{site.baseurl}}/documentation/RF3-changes)
{: .seealso }

> **A new license is required for RoboFont 3.**
>
> If you purchased a RoboFont 1.8 license after the 08/03/2017, you will get a full discount. [Get in touch](mailto:info@robofont.com) with your order number for more details.
{: .note }

## RoboFont 1.8.4

* Updating internal packages.

## RoboFont 1.8.1

* Fixing the auto-updater.

## RoboFont 1.8

* Updating internal packages.
* Use logging for logs, saved to `Application Support/RoboFont/robofont.log`.
* Adding `spaceCenterSelectionChanged` notification.
* Show a warning when adding glyphs with already existing unicodes.
* Show an outline error when smooth points aren't really smooth.
* Adding support for swipes and other events to `mojo.canvas`.
* New Space Center notifications: `spaceCenterDrawBackgroundLineView` and `spaceCenterDrawLineView`
* Ignore Components menu when there are too many items.
* Adding font and glyph metrics to guides measurements.
* Improving Inspector transform pane input fields.
* Ignore `autoSavePath` on Preferences export.
* A scripting window can become a floating window.
* Adding `sys_path_override` to the RoboFont application support folder, to overwrite embedded packages and modules. Be careful!

## RoboFont 1.7

* **Glyph Editor**
    * Glyph View should feel a lot faster (almost 4 times faster on 10.10).
    * Optimized behavior between mouse and arrow keys point modifications.
    * Making the guides popup menu a bit wider.
    * Move multiple tangent points selection according to their angle.
    * Fixing background color bug in Preview mode.
    * A glyph note can be `None`, so don’t copy it.
    * Fixing measurement in layer glyphs.
    * Changing behavior of the arrow keys based on if the cap lock key is down. When cap lock is down, the arrow keys behave like a mouse.
    * Fixing a slicing issue.
    * Fixing disappearing view on small zoom levels.
    * Rename glyph is possible when the glyph already exists as a template glyph.
    * Improving off curve while modifying an open contour.
    * Fixing starting point issue while setting extreme points.
    * Improving undo.
    * Improving drawing tool.
    * Fixing bug when pulling BCP out of two overlapping on-curve points.
    * Improving color overlay for images on 10.11.
    * Prevent circular component references.
    * Support jumping to first and last glyphs with home and end key.
    * Optionally disable snapping of off-curve points.
    * Show contextual guide menu when the guide is locked.
    * Draw end points of open paths differently.
    * Dragging a guide with an italic angle if there is one set.
    * Adding clear guides to contextual menu.
    * Adding skew + scale for scaling italics. Use “shift lock” to enable. An `italicAngle` must be set.
    * Show transform info while transforming.
    * Improving transform, more focus on the proportional constrain.
* **Font Overview**
    * Support for mark colors (as color swatches) in the rule editor.
    * Ignore template flag when importing empty glyphs from a binary font.
    * Improving checks in fonts with quadratic curves.
    * Improving group sheet to make it more flexible.
    * Update glyph cell representation when a layer is removed.
    * The Font Overview's search query should not change while show/hide the Find bar.
    * Adding support for displaying big unicodes in Space Center and glyph cells.
    * Improving selection in Font Overview.
    * Adding a new notification `glyphCollectionDraw`, sent when redrawing the ice cube glyph view. Be careful and don't draw heavy stuff. Cache before!
    * Delay updating of components in the font overview (this should speed up editing a lot!).
    * Small optimizations in the rule editor (aka Search).
    * Store custom unicode → glyph names mapping in the Preferences (only those which aren't in the GNFUL list).
* **Inspector**
    * Show a nice warning when trying to assign a unicode that already exists.
    * Round the center points when transforming a glyph.
    * Snap to grid even when no transformation is set.
    * Store Inspector pane sizes and reuse them when reopening window.
    * When renaming a glyph, also update the glyph order.
    * Improving setting data in Inspector list views. (`Anchor.name` cannot by `None` or empty, `baseGlyph` is required for a component).
    * Set italic angle as skew value (see options).
* **Preferences**
    * Adding hot key to toggle measurement info.
    * Adding hot key to toggle upside down.
    * Fixing reordering extensions in the Preferences window.
    * Adding glyph window tool sorting in the Preferences.
    * Check `autoSavePath` default key and revert the expanded user back to `~/…`.
    * Adding vertical metrics color to the Preferences.
    * Reset all warning buttons in the Preferences.
* **Space Center**
    * Adding support for centered text alignment.
    * Set point size correctly when opening a new Space Center.
    * Adding support for hiding template glyphs.
    * Recalculate beam position when setting it with a script.
    * Fixing `spaceCenter.getPointSize()`.
    * Adding support for display options in Kern Center (same hot keys as in Space Center).
    * Improving performance when a current glyph (aka `/?`) is not needed.
    * Fixing display of left margin while changing it in a Space Center.
    * Support for `/!` to display the current glyph selection.
    * Fixing issue when the input box is still active.
    * Disabling drag, use `CurrentSpaceCenter().disableDrag(True)`.
    * Adding notification `spaceCenterAdditionContextualMenuItems` for additional contextual menu items.
* **Scripting**
    * Jump to the github version of fontTools.
    * Open the UI of an existing font that has been created without one.
    * Adding `mutatorMath` and the `*.desingSpace` icon.
    * RoboFab kerning can be set to zero for a specific pair (breaking the old robofab API…).
    * Extension developer’s name can be unicode.
    * Extension name must be ascii only.
    * Fixing `font.cmp(other)`.
    * Better support for setting display states in a glyph line view.
    * Send `spaceCenterInputChanged` notification.
    * Support for some extensions time logging.
    * Adding `from mojo.canvas import CanvasGroup` (same as Canvas but with no scrollview).
    * Jump to definition (Alt + Cmd + J).
    * Support word wrap in code editors (py, fea, …).
    * Fixing fontTools `recalcBounds` when there is no extreme point.
    * Adding `KernCenter`, `GroupCenter` and `SpaceMatrix` to `mojo.UI`.
    * Adding a UI to an existing font object without a UI: `font.showUI()`.
    * Wrap internal defcon objects.
    * Fixing robofab `font.kerning.remove(pair)` and `del font.kerning[pair]`.
    * Support for custom extension root – use default key: `extensionsDir`.
    * Adding `removeDefault` in `lib.tools.defaults`.
    * Fixing `SmartSet` object.
    * Support for custom `glyphOrderAndAliasDB`: store it in the font.lib with the key `com.typesupply.ufo2fdk.glyphOrderAndAliasDB`.
    * Support for the `hdmx` table: store it in the font.lib with key `com.robofont.robohint.hdmx`.
    * Support for the `VDMX` table: store it in the font.lib with the key `com.robofont.robohint.VDMX`.
    * Improved `font.copy()` by also copying layers.
    * Sort the all fonts list: `AllFonts("familyName", "styleName", "italicAngle", "isItalic", myCustomFunction, useDefault=True, reverse=False)`.
    * Adding support for cap lock modifier key in event tools.
    * Adding contextual menu event in font overview with this notification: `fontOverviewAdditionContectualMenuItems`
    * Send notification when a testInstall is done: `didTestInstallFont`.
    * Fixing some small `mojo.compile.ttfautohint` issues.
    * Fixing bug in checking component `baseGlyph` names for robofab interpolation.
    * Optionally disable event stream use default `enableExternalFileCheck` (True/False).
    * Enable automatic text replacements in code editors.
    * Adding `AllWindows()` and `CurrentWindow()` to `mojo.UI`.
    * Adding support for getting a `CurrentGlyph()` from a Space Center selection.
    * Improving `mojo.canvas`.
    * Adding `glyphWindow.addAdditionalView(vanillaView)`.
    * Adding `glyphWindow.getVisibleRect()`to a glyph window. This is the actual visible rect without the scrollers and rulers.
    * Adding glyphs should clear the glyph when overwrite is enabled.
    * drag-and-drop should copy the width of the dropped glyphs.
    * Adding vertical metrics color to the Preferences.
    * Adding `roundedRect` to `mojo.drawingTools`.
    * Adding `glyphWindow.getVisibleRect()` to a glyph window. This is the actual visible rectangle without the scrollers and rulers.
    * Support for `Data()` object in json.
    * Installations of extensions is based on extension in the default folder.
    * Adding find bar to Output window.
    * Improved `glyph.setParent(font)`.
    * Adding local Python lib folder to `sys.path`.
    * Improving `SliderEditStepper` with support for floats.
    * Adding `MenuBuilder` to `mojo`.
    * Send notification when drawing the background when the view is inactive.
    * Fixing compiling scripts where the file name contains unicode characters.
    * Improving SmartSet support.
    * Adding `selectSmartSets(selection)` to set a selection in a Smart List (selection is a list of indexes).
    * Adding `updateAllSmartSets()` to update a Smart Set list in the UI.
    * Support for period + tab → `self.` in a code editor.
    * Fixing bug when removing a last smooth bPoint in an open contour.
    * Fixing weird Cmd + grave issue.
    * Fixing issue when something wants to set a lib object in a RFont.
    * Keep BPoint anchors smooth when inserting another bPoint.
    * Improved code editor syntax highlighting.
    * Embedding [feaPyFoFum](http://github.com/typesupply/feapyfofum). Use the font.lib key `com.typesupply.feaPyFoFum.compileFeatures` to enable it while generating.
    * Update glyph after `move(x, y)`.
    * Building extensions with *.pyc only* enabled copies scripts linked to the menu as normal .py files.
    * Adding `RFont.templateGlyphOrder`.
    * Fixing drag-and-drop issue with files in a code editor.
    * A glyph view has `drawTextAtPoint` and `drawTextInRect` to assist you to draw text in the glyph view.
    * Adding magnetic attribute to a guide object.
    * Fixing flipping glyph.
    * Fixing bug in comparing components in the robofabWrapper.
    * Improved regenerating of the scripting menu, prevent loss of hot keys.
    * Remove kerning pair with `kerning.eliminate(…)`.
    * `font.templateGlyphs` is an attribute, not a method.
    * Send notification when RoboFont will terminate.
    * Improving API on collection view, mainly used externally in the `glyphCollectionDraw` notification.
    * `autoUnicode()` uses uni<4-hex> and u<5-hex> glyph names to set the unicode value.
    * Fixing issue while decomposing a selected component.
    * Adding glyph cell notification: `glyphCellDraw` and `glyphCellDrawBackground`. Use it wisely.
* New Icon.
* Updating the embedded AFDKO.
* Adding GNFUL, [glyphNameFormatter](http://github.com/LettError/glyphNameFormatter) (with a : script separator)
* Fixing some high unicode input bugs.
* Small UI improvements in 10.10.
* Touch `.ufo` file to update the file modification date.
* Fixing autosave.
* Optionally disable event stream – use `enableExternalFileCheck` (`True`/`False`).
* Remove empty dictionaries in the `glyph.lib` when saving.
* Show a warning when a UFO is badly written (but with correct syntax, like 2 `moveTo` points in the same contour).
* Improving group sheet and make it more flexible.
* Solve small bug in setting extreme points.
* Friendly warning when assigning a unicode that already exists.
* Extension name must be ascii only.
* Transform matrixes in components must be tuples.
* Use the width of the glyph to check if a component can be used with the flag `USE_METRICS` (only while generating TTF).
* Improving glyph view updates on 10.9.
* Adding menu when launching in safe mode (shift down).
* Update vanilla, compositor and fontTools.
* Fixing big unicodes issue by updating `ufo2fdk`.
* Removing limitation for ttfautohint requiring a lowercase 'o'.
* Add GDEF if necessary while building TTF binary.
* Improving support for cursors on retina displays, when creating a cursor please use `CreateCursor(pathOrNsImage, hotspot)`.
* Set the text field as first responder, whatever settings are used.
* Only show Find bar when the font window is the main window.
* Do not add a unicode value for a template glyph if the unicode already exists.
* Show a message when font generation has failed.
* Fixing issue setting `maxComponentElements`.
* When no glyph name is given, don't analyze while pasting.
* Better support for orphan glyphs.
* Adding an optional check when the font does not contain an 'o', so ttfautohint recognizes it as a symbol font.
* Adding version of embedded packages in the About window.
* Adding clear buttons to Extension Builder.
* Don't show Output Window when a print statement is not in the main thread. (Fixing here and in Mechanic as well)
* Fixing a bug that removed duplicated guides with same angle and position.
* Adding support for dummy DSIG table. (see Font Info sheet > RoboFont).

> This update only supports macOS 10.9 or higher.
>
> Older versions of macOS are supported by RoboFont 1.6.
{: .warning }

## RoboFont 1.6

* **Font Overview**
    * Improving Alt copy of glyph selection to glyph names only.
    * Preserve checkboxes in the Add Glyph sheet.
    * Adding support for glyph note in the Font Overview list view and font search.
    * Glyph cell slider + Alt key will resize cells in all open fonts.
    * Improving Add Glyph sheet with options to import glyph names from external encoding file or from other open fonts.
* **Glyph Editor**
    * Better support for orphan point selection during copy paste.
    * Support drag-and-drop glyphs to a Scripting Window.
    * Undo support for flip layers.
    * Use italic slant offset as a display option.
    * Fixing bug with transformation of components selection.
    * Reset selection after locking an image and switching from Transform mode.
    * Slice tool snaps to guides.
    * Reset image selection after locking an image.
    * Improved support for copy/paste and preserving contour order.
    * Jump to glyph pushes exact matches to the top.
    * Fixing bug with joining contours when points are on top of each other.
    * Don't jump to hidden layers.
* **Space Center**
    * Enable flip.
    * Live preview when editing the left margin.
    * Fixing bug when using the beam.
    * Display font size as a combo box.
    * Fixing bug in window order when double-clicking a Space Center.
    * Add in-between values when beam is active.
* **Scripting**
    * Fixing bug when setting `x`, `y` values in guides.
    * Support for adding Inspector window panes and toolbar items in both a font window or a glyph window with: `inspectorWindowWillShowDescriptions`, `glyphWindowWillShowToolbarItems` and `fontWindowWillShowToolbarItems` event notifications.
    * Fixing bug when comparing components with `glyph.isCompatible(otherGlyph)`.
    * Adding a new `bundle.html` attribute to ExtensionBundle, to indicate if the bundle has html help files. It must be a bool.
    * Adding `font.showUI()` (only for fonts which have no UI yet).
    * Improving `glyph.center()` to work better with layers.
    * Improving getting bcpIn and bcpOut in bPoints with open contours.
    * Adding `OpenScriptWindow(path.py)` to `mojo.UI`.
    * Adding `obj.getRepresentation` to robofab glyph and font wrapper.
    * Fixing bug when getting an extension icon.
    * Improving support for `font.save()` when no path is given and font has no path.
    * Remove closed documents from `AllFonts`.
    * Fixing bug in rounding kerning.
    * Fixing bug in diving a glyph.
    * Fixing bug in copying orphan glyphs.
    * Add a space after `#` when commenting lines of code.
    * Improving support for code completions.
    * `mojo.CurrentFontWindow().setGlyphNamesAsSet(glyphNames)`
    * `font.setLayerDisplay` has an option for `all`.
    * `mojo.UI.getTestInstalledFonts()` and `mojo.UI.testDeinstallFont(font)`
    * Script file browsers hide empty .py files in the menu, and support alias files.
    * Fixing bug in `font.copy()`.
    * Adding `font.info.update(otherInfoObject)`.
    * Adding support for sub-classing internal font objects.
    * Improving messages when installing and uninstalling extensions (with version numbers).
    * Adding `currentFontOnly` option for getting windows with mojo: `CurrentGlyphWindow(currentFontOnly=True)` works for all windows in mojo.
    * Make it easier to add a UI to an existing font without a UI.
    * Adding `mutatorMath` and `mutatorMath.designSpace` icon :)
* **Inspector**
    * Visual support for multiple unicodes in Inspector window.
    * Fixing bug when setting the transform matrix for components.
    * Transform pane now accepts Enter to apply a transformation.
    * Support for transform location while flipping.
    * Add a nice warning when assigning a unicode that already exists.
* **Preferences**
    * Adding AGL-file path for custom unicode to glyph name mapping.
    * Check if short key path exists.
    * Adding option to toggle `__future__.division` while executing a script.
    * Adding color preferences for preview fill and preview background.
    * Improving support for sorting extensions.
    * Adding hot key to toggle measurement info.
* Fixing bug: rounding error while compiling a ttf.
* Improving Test Install.
* Search Box in Info Sheet.
* Support for vfb import/export if users have `vfb2ufo` installed in `/usr/local/bin`.
* Update the embedded FDK.
* Fixing bug when generating an unsaved font.
* Improving conversion from cubic to quadractic curves.
* Support for conversion from quadratic to cubic curves with the same logic.
* Improving support for Test Install when there is no UI.
* Update color marks in the rule editor with the colors in the current font.
* Holding Shift down while launching the app will open RoboFont without any extensions installed (Safe Mode).
* UI uniformity on list cell colors.

## RoboFont 1.5.1

* Fixing Preferences window bug for 10.6 users.

## RoboFont 1.5

* **Font Overview**
    * Adding support for deletion of multiple Smart Sets.
    * Adding option for choosing a mark color when generating new glyphs.
    * Adding support for selection of multiple Smart Sets.
    * Adding support for navigation with arrow keys in Smart Sets.
    * New code editor for OpenType features.
* **Glyph Editor**
    * Fixing direction of keys for layer up/down. :)
    * Adding Cmd + Ctrl in the Components contextual menu, to copy the base glyph metrics.
    * Adding display option *Show Segment index*.
    * Remove Overlap now works faster.
    * Separating display options: *Guides* and *Rulers*.
    * Fixing bug in beam factory when two intersections are on top of each other.
    * Adding *Show Image Info*.
    * Glyphs with more than 5 unicodes are renamed to `uXXXXX`.
    * Add Components sheet now accepts both unicode value and glyph name when searching for glyphs.
    * Better support for selection of multiple tangent points.
    * Adding Ctrl + Cmd + A to select all components.
    * Fixing bug in joining contours with line and curve segment.
    * Fixing bug in exporting smart list to groups.
    * Improving sliders for image colors.
    * Set components as selected after decomposing.
    * Adding *Lock Image* to image contextual menu.
    * Join a contour with another contour (only when two end/start points are selected).
    * `Alt` + click on a straight line to generate BCPs with the rule of thirds.
    * Set points as selected after slicing.
* **Space Center**
    * Same default settings as a glyph view for jumping to the next glyphs.
    * Adding hot key to toggle Space Matrix.
    * Preventing PUA from converting characters in the input field back to unicode.
    * Improving `esc` menu.
    * Handling Space Window titles similarly as other windows.
    * Fixing small bug with drag-and-drop.
    * Adding `setBeam(pos)` and `beam()` methods.
    * Adding flexible cell widths.
    * Adding hot keys to toggle kerning.
* **Inspector**
    * Adding support for drag-and-drop reordering of contours.
    * Fixing bug with setting a note in a glyph.
    * Improving layer panel: drag-and-drop and reordering.
* **Generating**
    * Better output for .ttf compiler.
    * Faster generation for all formats.
    * Fixing bug in .ttf splines. (It's possible to have a contour without any on-curves, doh)
    * Fixing bug with double points in .ttf compiler.
* **Preferences**
    * Adding 'Reveal Python Script folder' button to *Preferences > Python*.
    * New default colors and options for the new code editor.
    * Allow any kind of shortcut to be assigned to a script.
* **Scripting**
    * Adding a `selected` attribute to Robofab `bPoints`.
    * Adding `glyph.removeSelection` to RoboFab.
    * Allowing `ExtensionBundle` to save an extension or make one from scratch.
    * New Code Editor, with new syntax highlighting.
    * New mouse and keyboard shortcuts in a code editor:
        * select number → Cmd + drag
        * select tuple → Cmd + drag
        * select number → Cmd + arrow up/down
        * select bool (True, False) + Cmd + click
        * select bool (True, False) + Cmd + arrow up/down
    * Remember settings for 'Show Line Numbers'.
    * Fixing bug in exporting preferences in `mojo.UI`.
    * Improved UI for Extension Builder.
    * Adding support for sending custom events.
    * Set and get a transform matrix from a Robofab component.
    * Check if the coding font used in the scripting window is installed.
    * Adding `AllSpaceCenterWindows` and `CurrentSpaceCenterWindow`.
    * Adding option to import/export Preferences to/from a dict.
    * Adding shortcuts for indenting and unindenting code.
    * Fixing Robofab's `rotate`, `skew` and `offset` attributes.
    * Adding support for applying transformation matrixes to an image object.
    * Publish event when an extension is built.
    * Adding support for inserting BPoints in a contour.
    * Fixing bug in `robofab.contour.breakContour(point)`.
    * Adding support for glyph, component and selection colors in `mojo.glyphPreview`.
    * `mojo.UI.multiLineView` returns a proper Robofab glyph.
    * Adding `SpaceCenterToPDF` and `GlyphWindowToPDF`.
    * Improving Smart Sets in mojo.UI
* Fixing typos!
* 'Update found' bugs should be completely Fixing. :)
* Massively improved memory management.
* Adding support for `robofont://path` and `robofont-script://path.py`.
* Fixing bug with saving images.
* Adding support for opening `.ttc` TrueTypeCollection files.

## RoboFont 1.4

* **Font Overview**
    * Fixing bug in the conversion of Smart Set to group.
    * Don't mark template glyphs.
    * Fixing bug in setting glyph order by dragging glyphs.
    * Fixing bug in removing glyphs and template glyphs.
    * Building components with anchors.
    * Adding anchor syntax: `Agrave=A+grave@top`.
    * Adding search by mark color.
    * Adding Smart Set binders.
    * Fixing bug with `include(myFile.fea)`.
* **Glyph Editor**
    * Fixing bug in remove overlap.
    * Fixing bug in slicing glyphs.
    * Correctly perform layer up / layer down hot keys.
    * Swap complete glyph to layer when there is no selection.
    * Round components after decomposing to the value set in the Preferences.
    * Fixing bug in beam glyphs.
    * Fixing bug in Jump to Layer popup window.
    * Page up / down jump through component selection.
    * Page begin / end jump through anchor selection.
    * Support for Shift constrain when scaling images.
    * Fixing bug with horizontal and vertical flipping.
    * Draw anchor coordinates when 'show point coordinates' is active.
    * Enter on anchor selection.
    * Transform position is written back to the defaults.
    * Fixing bug when slicing glyphs.
    * Adding anchor indexes and component indexes.
    * Fixing bugs with constrains.
    * Better undo support for global guides.
    * Improving break contour.
* **Generating**
    * Optimizing `.pfa` compiler.
    * Check contour direction while generating `.ttf` or `.otf` from different sources.
* **Preferences**
    * Adding *Close Contour* to hot keys.
    * Adding *Space Center Input Selection Color*.
* **Scripting**
    * Support for opening binary fonts without UI and scripting.
    * Clear Output Window before running a script.
    * Adding `stemHist` to `mojo.compile`.
    * Backup code if code makes RoboFont crash :)
    * Debug window is accessible through mojo.
    * Support for `isCompatible` with nice error report.
    * Support for tabs in Scripting Window.
    * `OpenFont(…, showUI=False)` can be opened in a Glyph Window or Space Center.
    * `font.templateSelection` in robofab.
    * Adding `hasOverlap()` to robofab (both in glyph and contour object).
    * `font.copy(showUI=False/True)` to make a copy with or without UI.
    * Adding `QTKit`.
    * Fixing bugs in Cocoa drawing tools.
    * `setGlyphViewScale` and `centerGlyphInView` for a glyph window object.
    * Open the MultiLine view as a vanilla object.
    * Support for glyph math and boolean operations with `|` union, `%` difference, `&` intersection and `^` xor.
    * Support for tracking in Space Center `aSpaceCenter.setTracking(10)`.
    * New event callbacks in Space Center: `spaceCenterDraw`, `spaceCenterKeyDown`, `spaceCenterKeyUp`.
    * Adding `GlyphWindowToPDF` and `SpaceCenterToPDF` to `mojo.UI`.
* **Metrics**
    * Optimizing RTL kerning.
    * Pressing `esc` in Space Center input will cancel setting metrics.
    * Make sure the Space Center input box is continuous.
    * Fixing large linespace frame size in Space Center.
    * Don’t live update Space Center on glyph change when there are lots of characters to display.
    * Fixing an old scroll to selection bug in Space Center.
* Let users know if an UFO is unsaved when switching to another app. (Jumping RoboFont icon in the dock)
* All editable numbers have support for up / down arrow keys.
* Better support for Single Window Mode with additional glyph windows and current glyph.
* Pop up a warning if RoboFont is stored in a folder containing space characters.
* Send event when an external change has happened.
* Adding View short keys for font window (available under *Window > View*).
* Better support in detecting quadratic / cubic glyphs.
* Fixing bug in external updates and save operation.
* Optimizing external changes (example: when a UFO is stored in a DropBox folder and changed when RoboFont is active).
* Toolbars are more flexible and easier to change.
* Remove empty contours during loading.

## RoboFont 1.3

* **Font Overview**
    * `Cmd + F` shows Find Glyph panel in Font Overview.
    * Fixing bug in Smart Sets.
    * Warn about duplicated glyph names when adding glyphs.
    * Add Glyphs by pressing Enter is the same as using the Apply button.
    * Check for template glyphs when adding new glyphs.
    * On glyph change, also check if the glyph name is scheduled for deletion.
    * Fixing bug in Font Info Sheet: reset font info with some basic attributes.
    * Font Overview drag-and-drop:
        * drop on row: add contours
        * drop on row + alt: add as components
        * drop between row: reorder glyph set
    * Alt + click on Smart List to select Smart Set.
    * Alt + delete also removes the template glyph.
    * Cell size slider updates when using keys to change the zoom level of glyph cells.
* **Glyph Editor**
    * Fixing a bug in Editing Tool when glyph was `None`.
    * Ignore Shift + down constrain while Cmd is down
    * Move BCP when closing a contour.
    * Bezier Drawing Tool continues to draw in selection only if the contour is open.
    * Show Add Anchor sheet only when an anchor name is given.
    * Better stroke-hit-by-point calculations.
    * Check for selection while transforming a set of glyphs.
    * Check if grid size is not `0`.
    * Fixing a bug with snapping contours when both points are selected.
    * Don’t send `keydown` notification in Preview mode.
    * Post notification when Glyph Window gets selected/deselected.
    * Don’t join contours when in Transform mode.
    * Fixing bug in Cut with template glyph.
    * Close current contour on double-click (Drawing Tool).
    * Toggle selection on mouse click with magic contour selection.
    * Fixing bug in `removeAnchor`.
    * Allow spaces in anchor names.
    * Fixing bug in Select All when there is no glyph set.
    * Ignore first `mouse down` when in Single Window mode.
    * Better support for new glyph windows, don’t fit to window directly.
    * Tab with hot key through BCP-anchor-BCP.
    * Better support for layers visibility when layer is selected.
    * Update rulers when a new glyph has been set.
    * Fixing an annoying bug in Transform.
    * Fixing bug with shift constrain on y-axis (jumped over the first 5 units).
    * Join contours takes the Glyph View scale into the snapping distance. The snapping distance can be stored as a new Preferences setting.
    * Smaller increase rate of grid disappearing.
    * Fixing bug in slicing: bounds should be checked as bounds, not as rectangles.
    * Make anchors visible if one anchor is added.
    * During Select All, show anchors in the selection, even if they aren’t visible.
    * Fixing bug in joining contours.
    * Fixing bug in Copy&Paste with different kind of objects.
    * Support Undo while adding anchors.
    * Take the closest distance when joining multiple contours.
    * `addComponent` copy metrics will be disabled when the glyph contains some contours.
    * Don’t break contour, but delete single on-curve point in the contour.
    * Don’t move when switching layers up & down :)
    * Select layers with double-click.
    * Better support for preserving point attributes during remove overlap.
    * Fixing bug in Editing Tool: dragging a mix of off-curves and on-curves.
    * Fixing ray margins.
    * Adding join contours in contextual menus.
    * Adding break contours in contextual menu.
    * Adding layer data in copy paste from Font Overview.
    * Scroll with control to zoom faster with scroll wheel.
    * Adding crosshair to anchor, component dragging.
    * Clear also all layers when a glyph gets deleted.
    * Fixing bug in setting glyph and keeping the same scroll amount, so switching glyphs is less jumpy.
    * Use `esc` during transform to reset the transformation.
    * Allow transformation of single points and multiple points not even in a segment.
    * Magic selection tool should also select single `moveTo` points.
    * Better support for transform and guides.
    * Mixed selection is possible (points, segments, contours, anchors, components, image).
    * Slanted grid based on italic slant angle.
* **Inspector**
    * Solving weird layer color syncing.
    * Only reset selection if it has contours, so components keep their selection.
    * Resizable column headers in the Inspector view.
    * Reverse how transformations are added.
    * Prepare to save transformations in the user defaults.
    * Fixing bug when glyph is empty and transform metrics is on.
    * Write transforms back to defaults and recover them while opening the Inspector window.
    * In move transforms, ignore *do metrics* while transforming.
    * Removed Action button in layers Inspector pane.
    * Close the Inspector window when Cmd + i is pressed.
    * Check if there is a document open in the Inspector window.
    * Unicode value can be `None` when set in the Inspector.
    * Adding option Transform Again, to repeat the previous transformation.
* **Preferences**
    * Python panel gets tabs.
    * Adding `defaultForeGroundLayerColor` and `defaultBackGroundLayerColor` to the Preferences.
    * Better support for changing scripting hot keys in the Preferences.
    * Check for Cmd + F shortcut in Scripting hot keys and disable the Full Screen shortcut.
    * Set starting points hot key and better support from the menu.
    * 45° constraints as a Preference setting.
    * Always add *Update menu* in Extensions menu.
    * Marque selection color in Preferences.
    * Adding `Test Install` hot key.
    * Adding Preference *starting point color for open contours*.
    * Adding options to reset, export and import Preferences.
    * Check if the chosen font used in to display code and features is still installed.
* **Space Center**
    * Space Center input text could be `None`.
    * Improving scrolling in single line mode in Space Center.
    * Auto suffix detection based on current glyph.
    * Show/hide space matrix.
    * Set the current value when tabbing to another value.
    * Improving Undo support in Space Center input fields.
    * Require Enter after a value has been edited.
    * Support for Italic angle in beams and measurements.
    * Space Center keeps track of the current glyph when opening.
    * Adding Alt + drag to move glyph along the y axis, Alt + Cmd + drag to move glyph along the x axis.
    * Improving how "Jump to Glyph" works.
    * Renaming layers.
* **Features**
    * Open `.fea` files natively.
    * Features editor supports drag-and-drop of a `.fea` features file, which is inserted into code as a link: `include(dropFilePath);`.
* **Generating**
    * Only move contours when shifting with an italic offset.
    * Always check for mixed components before generating.
    * Converting TrueType off-curve points must be inside the bounds of the original Bezier segment.
    * Fixing bug in generating `.ttf` files.
    * Updating to AFDKO 2.5, with better support for mark-to-mark (thanks Miguel and Erik).
    * ttf compiler: space can be anywhere in the glyph order.
    * ttf compiler: check if anything goes wrong in generating the empty font, and report nicely.
    * While generating decompose when a component has a scale transformation
* **Scripting**
    * Run Python files directly from the file browser with Cmd + R or contextual menu.
    * Adding support for external scripting with shell command: see Preferences, `robofont -h`.
    * `CurrentFont().glyphOrder` creates and removes template glyphs.
    * Better handling of notifications for `viewWillChangeGlyph` and `viewDidChangeGlyph`.
    * Solved bugs in `mojo.UI` involving getting and setting Character Sets.
    * Adding `mojo.tools`.
    * `glyph.pointInside()` is a lot faster.
    * Cmd + g in Scripting window for Find Next.
    * Save `.py` files encoded as utf-8.
    * Improved support for Smart Set in `mojo.UI`.
    * Adding `addSmartSet`, `removeSmartSet`, `setDefaultCharacterSet`, `getDefaultCharacterSet`,`removeCharacterSet`, `getCharacterSet`, `setCharacterSet`, `setMaxAmountOfVisibleTools`,`getMaxAmountOfVisibleTools`.
    * Scripting Window keeps track of placing and size of the window.
    * Code and feature editors will not convert (p) to ℗.
    * All textEditor have disabled rich text formatting.
    * Support for external editing of `.py` and feature files.
    * Adding support for `CurrentFontWindow()` in mojo.
    * Adding `help` to the scripting namespace.
    * Adding draw, drawPoints and box to robofab components.
    * Adding new notification `binaryFontWillOpen`.
    * Adding `mojo.compile`.
    * Adding `getScriptingMenuNamingShortKey` and `setScriptingMenuNamingShortKey` to `mojo.UI`.
    * Adding `testInstallFormat` for format used in test install (should be `otf` or `ttf`).
    * Adding a `clearDefaults` in `lib.tools.defaults`.
    * Adding mojo.UI `importPreferences` `exportPreferences`.
    * Adding `getToolsOrder`, `setToolsOrder` to order the tools in the toolbar.
* **RoboFab**
    * Return `public.glyphOrder` when there is no document in the RoboFab font object.
    * Revert order of interpolation masters in RoboFab methods.
    * Fixing bug in RoboFab compare font.
    * Don’t assume added glyphs are `robofabwrapperglyph` only :)
    * Support for beams and rays in `robofabwrapper`.
    * Adding path to save notifications.
    * Set a default width for new glyphs `setDefault("newGlyphWidth", 123)`.
    * Use Esc to ignore “don’t show again” dialogs.
    * Fixing bug in remove overlap comparing `numpy` objects.
    * Fixing typos.
    * Adding modules `syncope` and `csv`.
    * Fixing bug in the Extension Bundle.
    * Set modification time for ufo if saved.
    * Improving support for reading `.woff` files.
    * Return the value of `RSegment.smooth`.
    * Support for glyph metrics math in `robofabwrapperglyph`.
    * Adding `breakContour(point)` to `robofabwrapperglyph`.
* **All assets are ready for Retina displays.**

## RoboFont 1.2

* Global app cache for template glyph image cells.
* Separate Revert options for changed, added and removed glyphs.
* Set glyph to unchanged on Revert to saved.
* Sort glyphs based on `public.glyphOrder` from font.lib.
* Application alias added to the `.dmg` file.
* Transparent mark colors are now drawn with a black triangle in the back.
* Show slant angle in the glyph cells.
* Fixing bug which made new glyphs unselectable.
* Check for nested components.
* Solving weird tool cursor issue in fullscreen mode (10.7).
* Better support for the AFDKO, with better feedback when it encounters an error.
* Fixing bugs with adding/removing glyphs when a subset is selected.
* Test Install now also works with fonts which are not Document-based, for example UFOs opened without a UI with `OpenFont("path/to/my/font", showUI=False)`.
* Check glyph order in binary compilers for template glyphs.
* New licensing system.
* Adding Sparkle auto-updater.
* Better support for copying & pasting of EPS data (from and to Illustrator).
* When pasting paste, check `segmentType` and convert if necessary.
* Fixing UI bug when tabbing through the font info sheet (10.6 only).
* Better support for drag-and-drop on Smart Sets (also: drag on top of Smart Set to add the glyph name).
* The app is code-signed for 10.8 usage.
* Fixing a few typos :)
* **Documents**
    * Preserve starting points when converting splines.
    * Open `.fea` files as native documents. This will open a new feature window with no font attached – so it possible to send the feature to all open fonts, or to a selection of all open fonts.
    * Added icons for `.otf` `.ttf` `.fea` `.pfa` `.woff` `.pfb` `.py` files (will only be applied if RF is the default application to open these files).
    * Support for `.ttf` in `.woff` files.
    * Open folders: files are now listed in the tree of a Scripting Window.
    * Disable PS auto-hinting when no blue values are set
    * Saving predefined character set in the font.lib.
    * Saving sorting order in the font.lib.
    * Adding export features.
    * Really remove deleted glyphs :)
* **Glyph View**
    * Select added components.
    * Shift + tab to select previous point.
    * Alt + tab to select off-curve points.
    * Alt + next/previous glyph jumps to glyphs from another Smart Set.
    * Adding BCP minimal mouse moved distance to account for the view scale.
    * Support for hiding measurements info (useful when lots of measurements are added).
    * Scrolling through glyphs in Glyph View: previewKey + Alt + scroll wheel.
    * Draw the grid first in the Glyph View.
    * Copy/paste `.glif` xml.
    * Fixing bug with deletion of points.
    * Components are now added when selecting all.
    * Alt + click + drag to select off-curves only.
    * Draw some component info in the new Component info display option.
    * Refinements when a user has more then 7 tools installed: a drop-down menu will appear in the last segment of the tool button.
    * Nicer defaults for glyph offset in the Glyph View.
    * `aicbtools` (copy paste from Illustrator and FL) now supports open paths.
    * Better rounding point coordinates.
    * Better handling of grid resolutions when zooming out.
    * Fixing bug with dragging smooth on-curve points.
    * Fixing bug with copy/paste: starting point now stays the same.
    * Lock side-bearings in Glyph window.
    * Reverse quadratic curves after removing overlap (so they will have a default TT direction).
    * Don't send `mouseUp` notification when zooming.
    * Extreme points now only adds points between two selected on-curve points, or adds all extreme points when nothing is selected.
* **Space Center**
    * Space Center prevents unintentional click & drag.
    * Alt + drag glyphs into the Space Center to include them in the input string.
    * Support for equalized slanted side-bearings in angled italics.
    * Moving glyph names suffix and kerning to the display options menu in the Space Center.
    * Space Center can go full screen.
    * Adding smart suffixes in Space Center, based on splitting glyph names on `.` (period).
* **Inspector view**
    * Adding Reverse to flipping.
    * Adding Undo support for Transform actions.
    * Use metrics math in Inspector glyph info panel.
    * Reorder components in Inspector with drag-and-drop.
    * Inspector points are displayed as a tree view.
    * Transform options in Inspector window.
    * Transform metrics as option in the Transform panel.
* **Preferences**
    * New option: hide dashed cross line while dragging.
    * New color options added: measurement, contour index, point index.
    * Redesigned Glyph View Preferences panel.
    * Shift and Cmd + Shift are now in the Glyph View Preferences panel.
    * New option: color schemes for quadratic and cubic curves (handle and point strokes are interchangeable).
    * New option: when deleting glyphs, also delete the glyph name from groups and kerning.
* **mojo**
    * New `mojo.UI.UpdateCurrentGlyphView`.
    * `from mojo.extensions import ExtensionBundle, getExtensionDefault, setExtensionDefault, getExtensionDefaultColor, setExtensionDefaultColor`
    * Extension Builder now has support for a resources path: a folder containing images or additional data. (Note: this is *not* a place to store Python files.)
    * Adding new events: `glyphWindowWillOpen`, `glyphWindowWillClose`, `fontWillSave`, `fontWillAutoSave`
    * Adding `mojo.roboFont.version` :)
    * Adding ExtensionBundle: `from mojo.extensions import ExtensionBundle`
    * Send proper `UFObecomeActive` notifications.
    * Adding `extensionValidator`.
    * Defaults system for Extensions (using another Preferences file to save defaults).
    * Adding `AccordionView` to `mojo.UI`.
    * Addint `registerExtensionDefaults` to `mojo.extensions`.
    * Fixing bug in `mojo.drawingTools` involving text with fill and stroke colors set.
    * Adding `SetCurrentLayerByName` to `mojo.UI`.
* **RoboFab**
    * Fixing bug when adding glyphs with Robofab.
    * Adding `removeGuides`.
    * Adding `font.copy()` to the Robofab wrapper.
    * Robofab `rotate` in degrees instead of radians.
    * Handle `obj.selected = True/False` (better).
    * `appendComponent` will check if it is referencing to itself.
    * Fixing bug with scaling of components.
    * Fixing bug with Robofab `segment.selected`.
    * Fixing bug with Robofab set/get bPoint types.
    * `glyph.mark` can accept `None` to remove a mark color.
* **Scripting**
    * Contextual menu in file browser.
    * Output Window now has color preferences, copying & pasting text will ignore rich text attributes.
    * Scripting Window: enter on a file will send the file as current document.
    * Open a folder in the Scripting Window.

## RoboFont 1.1

* Adding support for nested components.
* Improving glyph transform.
* Fixing bug in copy and paste from Illustrator.
* `glyph.mark` can handle a single as input.
* Images can be locked.
* Alt + Cmd down will mirror the handle point.
* Space Center value input fields can handle simple math like `=e+10-z*10` or `-=10+a`.
* New `mojo.canvas` object.
* API support for Smart Sets.
* Adding support for tabbing through the Font Info sheet input fields.
* Adding a checkbox to Add Component sheet for copying glyph metrics.
* Find bar/panel in the Scripting Window’s output area.
* Fixing bug in Space Center preventing notifications.
* Ignore mask color and guides during copy in the Glyph View.
* Fixing bug in kerning sheet when the group name is not in the font.groups.
* Generating choices are enabled/disabled based on the export format.
* Ignore template glyphs in robofab.
* Improving conversions between cubic and quadratic contours.
* Save states for the Font Overview panes.
* Show a warning if groups contain glyph names with `‘` (single quote).
* Adding `HelpWindow` to `mojo.UI`.
* `.py` files are now also document based.
* Fixing typos.
* Extension Builder can handle required major minor version of RoboFont.
* Import extensions in Extension Builder.
* Extensions builder write files automatically.
* Improving file browsers.
* Clear info in the Font Info sheet.
* Ignore anchors during binary compile.
* Single line and waterfall have a horizontal scrollbar.
* New column in Font Overview: mark color.
* Fixing bug with setting extreme points.
* Undo/redo on glyph selection in Font Overview.
* Mirror all/selected glyphs in Transform panel.
* Use a Find bar in 10.7.
* `mojo.UI` can open a glyph in a window and create a new Space Center.
* Scrolling when you jump with tab in the Font Info sheet.
* Remember the developer and developer URL in the Extension Builder.
* Fixing bugs in Space Center.
* Close the window with unsubscribing the glyphs.
* Better accept mouse move so it will not disturb other views.
* Adding `drawPreview` event.
* Space Center can hide “in between” control glyphs.
* Display selection info in the status bar of the Font Overview.
* Fixing bugs in robofab.
* Print font will now print `familyName` and `styleName`.
* Adding `removeGuide`.
* Update the Output Window when you print something directly.
* Use the Glyph View bounds for calculating the length of the guides.
* Better support for generating `.ttf` binaries.
* If you have `ttfautohint` installed, `autohint` is an option. `ttfautohint` cannot be embedded in RoboFont because of license incompatibilities.
* Improved support for preserving same amounts of points.
* Draw glyph coordinates in context of the background, slightly darker on darker backgrounds.
* Opens all files: ttx, T1 binary, T2 ascii, PostScript outlines / T1 suitcases aren't supported (yet).
* Drawing tool: Alt + click (no drag) will convert the last segment to a line.
* ‘Lock guides’ is a global user setting instead of local to the Glyph View.
* Fixing sorting error.
* Revert.
* Fixing bug in Transform.
* Update the glyph view during paste.
* Glyph order issue in `.ttf` generation.
* Use `allowPseudoUnicode` in the super Smart Sort.
* `f.glyphOrder` returns the glyph order.
* Bitmap representation is properly aligned to the grid.
* Grid is a solid line.
* Fixing typos.
* Scripting Window has an *Edit With…* toolbar item.
* Edits to the Kerning and Groups sheets can be canceled, so the changes will not be applied to the font.
* Generating binaries: the order is the same as in the UI. A new option can add Mac Roman in front of the glyph order when generating fonts.
* Ice cube tray view show when a glyph has been edited.
* Adding `SetCurrentGlyphByName` to `mojo.UI` – set a glyph by name in the current glyph view.
* Don’t copy unicode value if that unicode value already exists in the font.
* Adding text color and background to the Scripting Window.
* Save As… will not save template glyphs.
* Mark colors are added the the color palette.
* Columns in the Font Overview can be hidden with a right click on the column header (like iTunes).
* New super Smart Sorting options (thanks to Tal), you’ll never create an `.enc` file again :)
* If the default font for template glyphs isn’t installed on your system, it will take the default system font.
* Images can be saved in the new image folder next to the UFO. (This is similar to UFO3, but not inside the UFO.)
* Glyph cell headers in the Font Overview changes background color when the mark color is too close.

## RoboFont 1.0

Initial public version.
