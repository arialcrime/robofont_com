import inspect
import json
import docutils
import os
import tempfile
import re
import objc
import AppKit
import defconAppKit
import vanilla
from fontParts.base.base import dynamicProperty
import mojo
from mojo.tools import walkDirectoryForFile
from mojo.compile import executeCommand

# --------------
# rst converters
# --------------

def compilePandoc(txt):
    tempPath = tempfile.mkstemp(".rst")[1]
    destTempPath = tempfile.mkstemp(".md")[1]
    f = open(tempPath, "w")
    f.write(txt)
    f.close()
    executeCommand(["pandoc", "--from", "rst", "--to", "markdown_github", tempPath, "-o", destTempPath], shell=True)
    f = open(destTempPath, "r")
    txt = f.read()
    f.close()
    os.remove(tempPath)
    os.remove(destTempPath)
    return txt

def convertRST(txt):
    txt = "\n\n%s\n\n" % txt
    classTagRE = re.compile(r":class:\`(.*?)\`")
    classTagReplacement = "`\\1`"

    refTagRE = re.compile(r":ref:\`(.*?)\`")
    refTagReplacement = "`\\1`"

    methTagRE = re.compile(r":meth:\`(.*?)\`")
    methTagReplacement = "`\\1`"

    attrTagRe = re.compile(r":attr:\`(.*?)\`")
    attrTagReplacement = "`\\1`"

    linkRe = re.compile(r"\`(.*?)[\r\n]?<(.*?)>\`_", re.MULTILINE)
    linkReplacement = "[\\1](\\2)"

    replacements = [
        (classTagRE, classTagReplacement),
        (refTagRE, refTagReplacement),
        (methTagRE, methTagReplacement),
        (attrTagRe, attrTagReplacement),
        (linkRe, linkReplacement),
        ("``", "`"),
        ("\\s", ""),
        ("`type-int-float`", "[`integer/float`](../../value-types/#type-int-float)"),
        ("`type-string`", "[`string`](../../value-types/#type-string)"),
        ("`type-coordinate`", "[`coordinate`](../../value-types/#type-coordinate)"),
        ("`type-transformation`", "[`transformation`](../../value-types/#type-transformation)"),
        ("`type-angle`", "[`angle`](../../value-types/#type-angle)"),
        ("`type-identifier`", "[`identifier`](../../value-types/#type-identifier)"),
        ("`type-color`", "[`color`](../../value-types/#type-color)"),
        ("::", ":"),
    ]

    for search, replacement in replacements:
        if isinstance(search, str):
            txt = txt.replace(search, replacement)
        else:
            txt = search.sub(replacement, txt)

    tableRE = re.compile(r"\n\n\+\-\-\-.*\-\-\-\+\n\n", re.MULTILINE|re.DOTALL)

    tableResult = ""
    i = 0
    for f in tableRE.finditer(txt):
        data = txt[f.start():f.end()]
        data = compilePandoc(data)
        tableResult += txt[i:f.start()]
        tableResult += "\n"
        tableResult += data
        tableResult += "\n"
        i = f.end()
    tableResult += txt[i:]
    txt = tableResult

    return txt.strip()

formatter = """- type: %(type)s
  name: %(name)s
  arguments: %(args)s"""

attributeFormatter = """- type: %(type)s
  name: %(name)s"""

# -----------
# json writer
# -----------

class JSONWriter(object):

    ignore = ["faq", "milkShake",
        "absolute_import",
        "AppKit",
        "wrapClass"
        "frameAdjustments",
        "preferencesChanged",
        "getNSView",
        "getNSVisualEffectView",
        "windowDeselectCallback",
        "additionContextualMenuItemsCallback_",
        "buildAdditionContectualMenuItems",
        "getNSScrollView",
        "respondsToSelector",
        "nsWindowLevel",
        "windowWillClose_",
        "nsWindowStyleMask",
        "contentView",
        "doodleWindowName",
        "closeCallback",
        "windowCloseCallback",
        "getWindowTitle",
        "raiseNotImplementedError",
    ]

    ignore.extend(dir(AppKit.NSObject))
    ignore.extend(dir(AppKit.NSView))

    ignoreSuperClasses = [
        AppKit.NSObject,
        AppKit.NSView,
        vanilla.vanillaBase.VanillaBaseObject,
        vanilla.vanillaWindows.Window,
        defconAppKit.windows.baseWindow.BaseWindowController
    ]

    showClassButIgnoreMethods = [
        #"EditingTool",
        #"MeasurementTool",
        #"SliceTool",
        #"BezierDrawingTool",

        #"PointDataPen",
        #"DecomposePointPen",
    ]
    ignoreIndexes = []

    titleFormatter = ".. _%(title)s:\n\n%(lineAbove)s\n%(title)s\n%(underline)s\n"
    classFormatter = ".. class:: %(name)s%(args)s"
    functionFormatter = ".. function:: %(name)s%(args)s"
    attributeFormater = ".. attribute:: %(name)s"
    describeFormater = ".. describe:: %(name)s"

    def __init__(self, removeDuplicates=False, done=None, parentWriter=None, convertRST=None):
        self.clear()
        if done is None:
            done = []
        self.done = done
        self.removeDuplicates = removeDuplicates
        self.convertRST = convertRST
        if parentWriter:
            self.done = parentWriter.done
            self.convertRST = parentWriter.convertRST
            self.removeDuplicates = parentWriter.removeDuplicates

    def set(self, **kwargs):
        self.content.update(kwargs)

    def append(self, **data):
        if "objects" not in self.content:
            self.content["objects"] = []
        self.content["objects"].append(data)

    def extend(self, other, data=None):
        if not other.content:
            return
        if data is None:
            data = self.content
        data["objects"].extend(other.content["objects"])

    def __nonzero__(self):
        return len(self.content)

    def getData(self):
        return self.content

    def clear(self):
        self.content = dict()

    def write(self, root, name=None, moduleWriter=None, classWriter=None, functionWriter=None, methodWriter=None, attributeWriter=None, addTitle=False):
        if addTitle:
            name = self.getName(root)
            self.set(name=name, type="module", objects=[])
        if moduleWriter is None:
            moduleWriter = self
        if classWriter is None:
            classWriter = self
        if functionWriter is None:
            functionWriter = self
        if methodWriter is None:
            methodWriter = self
        if attributeWriter is None:
            attributeWriter = self

        if inspect.ismodule(root):
            moduleWriter.writeModule(root, name)
        elif inspect.isclass(root):
            classWriter.writeClass(root, name)
        elif inspect.isfunction(root):
            functionWriter.writeFunction(root, name)
        elif inspect.ismethod(root):
            methodWriter.writeMethod(root, name)
        elif isinstance(root, objc.selector):
            methodWriter.writeObjcMethod(root, name)
        else:
            attributeWriter.writeAttribute(root, name)
        self.done.append(name)

    def writeModule(self, module, name):
        attributeWriter = self.__class__(parentWriter=self)
        functionWriter = self.__class__(parentWriter=self)
        methodWriter = self.__class__(parentWriter=self)
        classWriter = self.__class__(parentWriter=self)

        for name, obj in self.getModuleContent(module):
            self.write(obj, name,
                classWriter=classWriter,
                functionWriter=functionWriter,
                methodWriter=methodWriter,
                attributeWriter=attributeWriter)

        self.extend(attributeWriter)
        self.extend(functionWriter)
        self.extend(methodWriter)
        self.extend(classWriter)

    def writeClass(self, cls, name):
        if name is None:
            name = self.getName(cls)
        data = dict(
            name=name,
            type="class",
            arguments=self.getArgs(cls),
            objects=[]
        )
        self.writeDoc(cls, data)
        self.append(**data)

        if name.endswith("Error"):
            return
        if name in self.showClassButIgnoreMethods:
            return

        self.removeDuplicates = True
        superClasses = inspect.getmro(cls)
        attributeWriter = self.__class__(parentWriter=self)
        functionWriter = self.__class__(parentWriter=self)
        methodWriter = self.__class__(parentWriter=self)
        classWriter = self.__class__(parentWriter=self)

        totalSuperClasses = len(superClasses)
        didAddSubclassTag = False
        for i, superClass in enumerate(superClasses):
            if superClass is object:
                continue
            if superClass in self.ignoreSuperClasses:
                continue
            if not didAddSubclassTag and not superClass.__module__.startswith("lib"):
                self.extend(attributeWriter, data)
                self.extend(functionWriter, data)
                self.extend(methodWriter, data)
                self.extend(classWriter, data)
                if attributeWriter or functionWriter or classWriter:
                    data["objects"].append(dict(type="inherit", name="%s.%s" % (superClass.__module__, superClass.__name__)))
                didAddSubclassTag = True
                attributeWriter = self.__class__(parentWriter=self)
                functionWriter = self.__class__(parentWriter=self)
                methodWriter = self.__class__(parentWriter=self)
                classWriter = self.__class__(parentWriter=self)

            for methodName in sorted(superClass.__dict__.keys()):
                if self.removeDuplicates and methodName in self.done:
                    continue
                if self.shouldIgnore(methodName):
                    continue
                try:
                    method = getattr(cls, methodName)
                except:
                    continue
                self.write(method, methodName, classWriter=classWriter, methodWriter=methodWriter, functionWriter=functionWriter, attributeWriter=attributeWriter)
        self.extend(attributeWriter, data)
        self.extend(functionWriter, data)
        self.extend(methodWriter, data)
        self.extend(classWriter, data)

        self.removeDuplicates = False

    def writeFunction(self, function, name):
        data = dict(
            type="function",
            name=self.getName(function),
            arguments=self.getArgs(function)
        )
        self.writeDoc(function, data)
        self.append(**data)

    def writeMethod(self, method, name):
        self.writeFunction(method, name)

    def writeObjcMethod(self, method, name):
        self.writeFunction(method, name)

    def writeAttribute(self, attribute, name):
        data = dict(
            type="attribute",
            name=name
        )
        if isinstance(attribute, (property, dynamicProperty)):
            self.writeDoc(attribute, data)
        self.append(**data)

    def writeDescription(self, name, doc=None):
        data = dict(name=name, type="description")
        if doc:
            data["documentation"] = doc
        self.append(**data)

    def writeDoc(self, obj, data):
        doc = self.getDoc(obj)
        if doc:
            data["documentation"] = doc

    @classmethod
    def shouldIgnore(self, name):
        if name.startswith("_"):
            return True
        elif name.endswith("Class"):
            return True
        elif name in self.ignore:
            return True
        return False

    @classmethod
    def getModuleContent(self, module):
        if hasattr(module, "__all__"):
            return [(name, getattr(module, name)) for name in module.__all__ if not self.shouldIgnore(name)]
        else:
            return [(name, obj) for name, obj in inspect.getmembers(module) if not self.shouldIgnore(name)]

    def getName(self, obj):
        if hasattr(obj, "__name__"):
            return obj.__name__
        else:
            return obj.__class__.__name__

    def getArgs(self, obj):
        if isinstance(obj, objc.selector):
            return "()"
        if inspect.isclass(obj):
            if inspect.ismethod(obj.__init__):
                value = inspect.formatargspec(*inspect.getargspec(obj.__init__))
                value = value.replace("self, ", "").replace("self", "")
                return value
            else:
                return "()"
        value = inspect.formatargspec(*inspect.getfullargspec(obj))
        value = value.replace("self, ", "").replace("self", "")
        return value

    def getDoc(self, obj):
        doc = inspect.getdoc(obj)
        if doc and self.convertRST:
            doc = convertRST(doc)
        return doc

# -------
# globals
# -------

### Frederik's local settings
appRoot = u"/Users/frederik/Documents/apps/roboFont/robofont"
apiDest = u"/Users/frederik/Documents/apps/roboFont/site/robofont_com/_data/API"

if not os.path.exists(appRoot):
    ### Gustavo's local settings
    appRoot = u'/_code/robofont'
    apiDest = u'/_code/robofont_com/_data/API'

fontPartsStuff = [
    "RFont",
    "RLayer",
    "RInfo",
    "RKerning",
    "RGroups",
    "RFeatures",
    "RLib",
    "RGlyph",
    "RContour",
    "RSegment",
    "RAnchor",
    "RComponent",
    "RGuideline",
    "RImage",
    "RPoint",
    "RBPoint",
]

# ----
# mojo
# ----

def extractMojoDocs(fontPartsStuff):

    mojoData = []
    writer = JSONWriter()

    # ignore FontParts objects
    writer.ignore.extend(fontPartsStuff)

    for name, module in writer.getModuleContent(mojo):
        writer.clear()
        writer.write(module, addTitle=True)
        mojoData.append(writer.content)

    return mojoData

def writeMojoDocs(mojoData, apiDest):
    mojoDest = os.path.join(apiDest, "mojo.json")
    with open(mojoDest, 'w') as f:
        json.dump(mojoData, f, indent=2)

# ----------
# font parts
# ----------

def extractFontPartsDocs(fontPartsStuff):

    import lib.fontObjects.fontPartsWrappers as fontPartsWrappers

    fontPartsData = []

    for name in fontPartsStuff:
        obj = getattr(fontPartsWrappers, name)
        writer = JSONWriter(convertRST=True)
        writer.write(obj, addTitle=True)
        fontPartsData.append(writer.content)

    return fontPartsData

def writeFontPartsDocs(fontPartsData, apiDest):
    fontPartsDest = os.path.join(apiDest, "fontParts.json")
    with open(fontPartsDest, 'w') as f:
        json.dump(fontPartsData, f, indent=2)

# ---------
# observers
# ---------

def extractObserversDocs(appRoot):

    # search for all publish tags...
    publishEventRE = re.compile(r'(?:publishEvent|postEvent)\([\"\'](.+?)[\"\'](.*?)\)')
    observers = dict(
        fontWillOpen=set(["font"]),
        newFontWillOpen=set(["font"]),
        fontDidOpen=set(["font"]),
        newFontDidOpen=set(["font"])
    )

    # get all publishEvent callbacks
    for path in walkDirectoryForFile(appRoot):
        with open(path, "r", encoding="utf-8") as f:
            code = f.read()

        for m in publishEventRE.findall(code):
            event, args = m
            args = args.replace(",", "").replace("**_", "").replace("**", "").strip().split()
            args = [a.split("=")[0] for a in args]
            if event not in observers:
                observers[event] = set()
            observers[event] |= set(args)

    return observers

def writeObserversDocs(observers, apiDest):

    cls = mojo.events.BaseEventTool

    writer = JSONWriter()
    writer.set(name="observers")
    for key in sorted(observers):
        doc = []
        args = observers.get(key)
        if hasattr(cls, key):
            externalDoc = writer.getDoc(getattr(cls, key))
            if externalDoc:
                doc.append(externalDoc.split("\n\n")[0])
        if args:
            if doc:
                doc.append("")
            doc.append("Extra notification keys: `%s`" % ("`, `".join(sorted(args))))

        writer.writeDescription(key, "\n".join(doc))

    observersAPIPath = os.path.join(apiDest, "observersAPI.json")

    with open(observersAPIPath, 'w') as f:
        json.dump(writer.content["objects"], f, indent=2)

# ------------
# extract docs
# ------------

print('extracting API docs...')

print('\textracting mojo...')
mojoData = extractMojoDocs(fontPartsStuff)
writeMojoDocs(mojoData, apiDest)

print('\textracting fontParts...')
fontPartsData = extractFontPartsDocs(fontPartsStuff)
writeFontPartsDocs(fontPartsData, apiDest)

print('\textracting observers...')
observers = extractObserversDocs(appRoot)
writeObserversDocs(observers, apiDest)

print('...done.\n')
