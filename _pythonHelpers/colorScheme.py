from grapefruit import Color


sections = {
    'documentation' : Color.NewFromHsl(316, 1, 0.5),
    'extensions'    : Color.NewFromHsl(197, 1, 0.5),
    'news'          : Color.NewFromHsl(120, 1, 0.5),
    'education'     : Color.NewFromHsl(277, 1, 0.5),
}

x, y = 200, 200
w, h = 150, 150

for section, c0 in sections.items():
    print(section)
    save()

    c1 = c0.ColorWithLightness(0.4)
    c2 = c0.ColorWithLightness(0.3)
    c3 = c0.ColorWithLightness(0.6)

    colors = {
        'lighter1' : c3,
        'base'     : c0,
        'darker1'  : c1,
        'darker2'  : c2,
    }

    translate(x, y)
    for color in reversed(list(colors.keys())):
        c = colors[color]
        print('- %s (%s)' % (c.html, color))
        print()
        fill(*c.rgb)
        rect(0, 0, w, h)
        translate(0, h)

    restore()
    translate(w, 0)
    print()
