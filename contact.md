---
layout: page
title: Contact
menuOrder: 7
---

Use the form to get in touch – we will reply as soon as possible!

<form method="post" action="https://formspree.io/info@robofont.com" id="contactForm" class="cleanForm">
    <input type="hidden" name="returnURL" value="">
    <fieldset>
        <label>Name<em>*</em></label>
        <input type="text" name="name" required>

        <label>Your e-mail address<em>*</em></label>
        <input type="email" name="email" required>

        <label>Subject<em>*</em></label>
        <input type="text" name="subject" required>

        <label>Message<em>*</em></label>
        <textarea name="body" required></textarea>

        <input type="hidden" name="_next" value="{{site.baseurl}}/thanks" />
        <input type="hidden" name="_subject" value="RoboFont Contact" />
        <input type="hidden" name="_format" value="plain" />
        <input type="text" name="_gotcha" style="display:none" />

        <input type="submit" id="formSend" value="Send">
    </fieldset>
</form>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
<script>
    $("#contactForm").validate();
</script>
