---
layout: page
title: The UFO format
tags:
  - UFO
level: beginner
---

* Table of Contents
{:toc}

RoboFont uses UFO as its native font format.

{% image introduction/ufo-icon-file.png %}

## What is UFO?

**UFO (Unified Font Object) is a cross-platform, cross-application, human-readable, future-proof format for storing font data.**

The format was developed and is maintained by Erik van Blokland, Tal Leming, and Just van Rossum. The official UFO Specification is available at [unifiedfontobject.org](http://unifiedfontobject.org/).

Because UFO is application neutral, several typeface design and font production tools use it natively; these are referred to as [UFO Tools](#ufo-based-tools). Other applications can import from and export to UFO, using it only as a secondary format.

## Advantages of UFO

UFO is XML
: The format is standards-based and future proof, and easy to read and write using existing XML code libraries.

Open and documented format
: The UFO Specification is [publicly available](http://unifiedobject.org/), and its development process is [open](http://github.com/unified-font-object/ufo-spec/issues). Multiple tool makers and foundries contribute to it.

Each glyph in a file
: The UFO format is modular, with each glyph stored in a separate [.glif](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/) file. This makes it easy to access and track versions.

Cross-application
: One can import and export UFO’s from FontLab, Glyphs, and FontForge.

## UFO3

**RoboFont 3 uses UFO3 as its native source format.**

UFO3 introduces standard support for layers, images, guidelines, data blocks and unique identifiers.

Most of these features were already supported in previous versions of RoboFont, but as custom additions to the UFO2 format.

UFO3 improves interoperability between different editing tools, and enables new workflows based on layers. (The previous implementation of layers in UFO2 had some limitations: for example, all layers of a glyph had to share the same width.)

UFO continues to be actively developed. The [UFO Roadmap](http://unifiedfontobject.org/roadmap/) includes plans for UFO3 updates, and ideas for UFO4.

> - [Changes from RoboFont 1.8 to 3: UFO3 as native source format](http://robofont.com/documentation/RF3-changes/#ufo3-as-native-source-format)
{: .seealso }

## UFO Tools

<table>
  <tr>
    <th width='30%'>tool</th>
    <th width='70%'>description</th>
  </tr>
  <tr>
    <td><a href='http://tools.typesupply.com/'>MetricsMachine</a></td>
    <td>
      <p>A powerful tool to add kerning to your fonts.</p>
      <p><em><a href='http://extensionstore.robofont.com/extensions/metricsMachine/'>Now available as a RoboFont 3 extension.</a></em></p>
    </td>
  </tr>
  <tr>
    <td><a href='http://tools.typesupply.com/'>Prepolator</a></td>
    <td>
      <p>A tool to prepare fonts for interpolation.</p>
      <p><em><a href='http://extensionstore.robofont.com/extensions/prepolator/'>Now available as a RoboFont 3 extension.</a></em></p>
    </td>
  </tr>
  <tr>
    <td><a href='http://superpolator.com/'>Superpolator</a></td>
    <td>A stand-alone application for building multi-dimensional interpolation spaces.</td>
  </tr>
  <tr>
    <td><a href='http://roundingufo.typemytype.com/'>RoundingUFO</a></td>
    <td>A tool to round the corners of glyph shapes, add ink traps, and more.</td>
  </tr>
  <tr>
    <td><a href='http://ufostretch.typemytype.com/'>UFOStretch</a></td>
    <td>
      <p>A tool to simultaneously transform and interpolate glyphs.</p>
      <p>Useful for creating small caps, superiors/inferiors, condensed/extended styles etc.</p>
    </td>
  </tr>
</table>

- - -

> - [UFO3 (Tal Leming, RoboThon 2012)](http://vimeo.com/38328029)
> - [UFO Specification Update (Tal Leming, RoboThon 2015)](https://vimeo.com/123781570)
{: .seealso }
