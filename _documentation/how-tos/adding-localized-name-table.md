---
layout: page
title: Adding a localized name table
tags:
  - mastering
  - OpenType
level: beginner
draft: false
---

* Table of Contents
{:toc}

## Introduction

{% glossary OpenType fonts %} consist of several tables. The `name` table is where information about the font – such as font names, copyright notice, description, sample strings etc. – is stored. The font maker can supply this information in multiple languages, so that German users are presented with texts in German, French users with texts in French, etc.

> - [Naming table (OpenType Specification)](http://www.microsoft.com/typography/otspec/name.htm)
> - [OpenType Name Table Fields (UFO3 Specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-name-table-fields)
{: .seealso }

### Name records

Translations to any `name` table attribute can be added using *name records*.

In UFO fonts, name records are stored as dictionaries with the following format:

<table>
  <tr>
    <th width="20%">key</th>
    <th width="30%">value type</th>
    <th width="45%">description</th>
  </tr>
  <tr>
    <td><code>name ID</code></td>
    <td>non-negative integer</td>
    <td>The name ID.</td>
  </tr>
  <tr>
    <td><code>platform ID</code></td>
    <td>non-negative integer</td>
    <td>The platform ID.</td>
  </tr>
  <tr>
    <td><code>encoding ID</code></td>
    <td>non-negative integer</td>
    <td>The encoding ID.</td>
  </tr>
  <tr>
    <td><code>language ID</code></td>
    <td>non-negative integer</td>
    <td>The language ID.</td>
  </tr>
  <tr>
    <td><code>string</code></td>
    <td>string</td>
    <td>The string value for the record.</td>
  </tr>
</table>

> Records should have a unique combination of `nameID`, `platformID`, `encodingID` and `languageID`. In cases where a duplicate is found, the last occurrence of the `nameID`, `platformID`, `encodingID` and `languageID` combination must be taken as the value.
{: .note }

> - [Name Record Format (UFO3 Specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#name-record-format)
{: .seealso }

## Name record values

This section gives an overview of the possible values for name, platform, encoding and language IDs.

### Name ID

Name IDs identify specific kinds of data stored in the OpenType `name` table.

The following table maps each name ID to its corresponding UFO `info.plist` attribute, where the default value can be found.

<table>
  <tr>
    <th width="10%">NID</th>
    <th>description</th>
    <th>UFO3 info attribute</th>
  </tr>
  <tr>
    <td>0</td>
    <td>Copyright notice</td>
    <td><code>copyright</code></td>
  </tr>
  <tr>
    <td>1</td>
    <td>Font Family name</td>
    <td><code>styleMapFamily</code></td>
  </tr>
  <tr>
    <td>2</td>
    <td>Font Subfamily name</td>
    <td><code>styleMapStyle</code></td>
  </tr>
  <tr>
    <td>3</td>
    <td>Unique font identifier</td>
    <td><code>openTypeNameUniqueID</code></td>
  </tr>
  <tr>
    <td>4</td>
    <td>Full font name</td>
    <td><em>created automatically</em></td>
  </tr>
  <tr>
    <td>5</td>
    <td>Version string</td>
    <td><code>openTypeNameVersion</code></td>
  </tr>
  <tr>
    <td>6</td>
    <td>PostScript name</td>
    <td><code>postscriptFontName</code></td>
  </tr>
  <tr>
    <td>7</td>
    <td>Trademark</td>
    <td><code>trademark</code></td>
  </tr>
  <tr>
    <td>8</td>
    <td>Manufacturer Name</td>
    <td><code>openTypeNameManufacturer</code></td>
  </tr>
  <tr>
    <td>9</td>
    <td>Designer</td>
    <td><code>openTypeNameDesigner</code></td>
  </tr>
  <tr>
    <td>10</td>
    <td>Description</td>
    <td><code>openTypeNameDescription</code></td>
  </tr>
  <tr>
    <td>11</td>
    <td>Vendor URL</td>
    <td><code>openTypeNameManufacturerURL</code></td>
  </tr>
  <tr>
    <td>12</td>
    <td>Designer URL</td>
    <td><code>openTypeNameDesignerURL</code></td>
  </tr>
  <tr>
    <td>13</td>
    <td>License Description</td>
    <td><code>openTypeNameLicense</code></td>
  </tr>
  <tr>
    <td>14</td>
    <td>License URL</td>
    <td><code>openTypeNameLicenseURL</code></td>
  </tr>
  <tr>
    <td>15</td>
    <td>Reserved</td>
    <td>—</td>
  </tr>
  <tr>
    <td>16</td>
    <td>Typographic Family name</td>
    <td><code>openTypeNamePreferredFamilyName</code></td>
  </tr>
  <tr>
    <td>17</td>
    <td>Typographic Subfamily name</td>
    <td><code>openTypeNamePreferredSubfamilyName</code></td>
  </tr>
  <tr>
    <td>18</td>
    <td>Compatible full name</td>
    <td><code>openTypeNameCompatibleFullName</code></td>
  </tr>
  <tr>
    <td>19</td>
    <td>Sample text</td>
    <td><code>openTypeNameSampleText</code></td>
  </tr>
  <tr>
    <td>20</td>
    <td>PostScript unique name</td>
    <td><code>postscriptFontName</code></td>
  </tr>
  <tr>
    <td>21</td>
    <td>WWS Family Name</td>
    <td><code>openTypeNameWWSFamilyName</code></td>
  </tr>
  <tr>
    <td>22</td>
    <td>WWS Subfamily Name</td>
    <td><code>openTypeNameWWSSubfamilyName</code></td>
  </tr>
  <tr>
    <td>23</td>
    <td>Light Background Palette</td>
    <td><em>not available in UFO3</em></td>
  </tr>
  <tr>
    <td>24</td>
    <td>Dark Background Palette</td>
    <td><em>not available in UFO3</em></td>
  </tr>
  <tr>
    <td>25</td>
    <td>Variations PostScript Name Prefix</td>
    <td><em>not available in UFO3</em></td>
  </tr>
</table>

> - [name IDs (OpenType Specification)](http://www.microsoft.com/typography/otspec/name.htm#nameIDs)
> - [Name Record Format (UFO3 Specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-name-table-fields)
{: .seealso }

### Platform ID

Platform IDs identify the platform to which the name records applies.

<table>
  <tr>
    <th width="10%">PID</th>
    <th>description</th>
  </tr>
  <tr>
    <td>0</td>
    <td>Unicode</td>
  <tr>
  </tr>
    <td>1</td>
    <td>Macintosh</td>
  <tr>
  </tr>
    <td>2</td>
    <td>ISO (deprecated)</td>
  <tr>
  </tr>
    <td>3</td>
    <td>Windows</td>
  <tr>
  </tr>
    <td>4</td>
    <td>Custom</td>
  </tr>
</table>

> - [platform IDs (OpenType Specification)](http://www.microsoft.com/typography/otspec/name.htm#platformIDs)
{: .seealso }

### Encoding ID

Encoding IDs are platform-specific: the meaning of each value depends on the current platform ID.

The following table shows the recommended values for Mac and Windows platforms.

<table>
  <tr>
    <th width="10%">EID</th>
    <th>description</th>
  </tr>
  <tr>
    <td>0</td>
    <td>Mac (Roman)</td>
  </tr>
  <tr>
    <td>1</td>
    <td>Windows (Unicode)</td>
  </tr>
</table>

{% comment %}
- What about Linux and other platforms?
{% endcomment %}

> - [encoding ID (Mac)](http://www.microsoft.com/typography/otspec/name.htm#encLangIDs_platID1)
> - [encoding ID (Windows)](http://www.microsoft.com/typography/otspec/name.htm#encLangIDs_platID3)
{: .seealso }

### Language ID

Language IDs identify the language in which a particular string is written.

The list of languages IDs is extensive, and depends on the platform ID. Please see the OpenType specification for all supported languages and their IDs:

- [language ID (Mac)](http://www.microsoft.com/typography/otspec/name.htm#langIDs_platID1)
- [language ID (Windows)](http://www.microsoft.com/typography/otspec/name.htm#langIDs_platID3)

## Adding name records with the Font Info sheet

Name records can be edited manually in the {% internallink "font-info-sheet/opentype" text="OpenType section" %} of the Font Info sheet.

{% image how-tos/adding-localized-name-table_name-records.png %}

### Options

<table>
  <tr>
    <th width="45%">option</th>
    <th width="55%">description</th>
  </tr>
  <tr>
    <td>click the <code>+</code> button</td>
    <td>add a new name record to the list</td>
  </tr>
  <tr>
    <td>double-click individual table cells</td>
    <td>edit their content</td>
  </tr>
  <tr>
    <td>click the <code>-</code> button</td>
    <td>delete the selected name record</td>
  </tr>
</table>

## Adding name records with a script

The following example shows how to add name records with a script.

{% showcode how-tos/addNameRecords.py %}

{% comment %}
- look for real-world examples among open-source fonts
{% endcomment %}
