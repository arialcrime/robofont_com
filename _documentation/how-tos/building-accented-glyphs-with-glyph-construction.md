---
layout: page
title: Building accented glyphs with Glyph Construction
tags:
  - accents
  - components
level: beginner
---

* Table of Contents
{:toc}

*This page contains a step-by-step guide for building precomposed {% internallink 'how-tos/introduction-to-accented-glyphs' text='accented glyphs' %} using the Glyph Construction language and the Glyph Builder interface.*

## Before you start

Make sure that the [Glyph Construction] extension is installed.
: If it isn’t, see instructions for installing extensions [manually](../../extensions/installing-extensions) or {% internallink 'extensions/installing-extensions-mechanic' text='using Mechanic' %}.

Prepare a list with names of accented glyphs to build.
: See {% internallink 'how-tos/defining-character-sets' %} for information about language support.

### Our example font and glyph names list

To keep things simple, we’ll start from a very basic font containing only lowercase/uppercase Latin characters and a few accents. Of course, your font may include as many other glyphs as you like.

{% image how-tos/building-accented-glyphs_starting-font.png %}

> - The font also contains the `dotlessi` glyph, which is needed to build accented versions of `i`.
> - The font does not contain any anchors or guidelines.
{: .note }

These are the names of the accented glyphs we are going to build. Your list of glyph names can be a lot longer.

```text
# uppercase
Aacute
Acircumflex
Atilde
Agrave
Ccedilla
Eacute
Ecircumflex
Iacute
Oacute
Ocircumflex
Otilde
Uacute

# lowercase
aacute
acircumflex
atilde
agrave
ccedilla
eacute
ecircumflex
iacute
oacute
ocircumflex
otilde
uacute
```

## Defining base glyphs and accents

The first step in the creation of glyph constructions is to define each accented glyph as the ‘sum’ of its components. For example, the accented glyph `Aacute` is the combination of the base glyph `A` with the accent glyph `acute`. Putting it together: `Aacute = A + acute`.

Here is the same list of glyph names with the components for each glyph:

```text
# uppercase
Aacute = A + acute
Acircumflex = A + circumflex
Atilde = A + tilde
Agrave = A + grave
Ccedilla = C + cedilla
Eacute = E + acute
Ecircumflex = E + circumflex
Iacute = I + acute
Oacute = O + acute
Ocircumflex = O + circumflex
Otilde = O + tilde
Uacute = U + acute

# lowercase
aacute = a + acute
acircumflex = a + circumflex
atilde = a + tilde
agrave = a + grave
ccedilla = c + cedilla
eacute = e + acute
ecircumflex = e + circumflex
iacute = dotlessi + acute
oacute = o + acute
ocircumflex = o + circumflex
otilde = o + tilde
uacute = u + acute
```

> Accented glyphs may have more than one accent, for example: `Aringacute = A + aring + acute`. See also the [Vietnamese examples](https://github.com/gferreira/GlyphConstruction/blob/master/examples/vietnamese.glyphConstruction) included in the repository.
{: .note }

To preview the glyph constructions we just wrote, open your font, open the Glyph Builder, and paste the glyph construction rules into the left column. Then click on the *Update* button to refresh the preview. You should see something like this:

{% image how-tos/building-accented-glyphs_glyph-builder_1.png %}

This does not look right yet, but it’s a start. Each accented glyph is already being assembled from its components; the accents, however, are simply placed in their original positions. We still need to add instructions to position the accents.

## Adjusting vertical positions

As the next step, we’ll add a positioning rule for each accent. The Glyph Construction language offers several positioning methods; we’ll use positioning by *reference*, which takes two values (one for `x` and other for `y` alignment).

The vertical alignment will be controlled by two variables with `y`-values for upper- and lowercase glyphs. The horizontal alignments, for now, will be specified using the calculated `center` position.

Putting it all together:

```text
# define alignment positions
$top_uc = 770
$top_lc = 570

# uppercase
Aacute = A + acute@center,{top_uc}
Acircumflex = A + circumflex@center,{top_uc}
Atilde = A + tilde@center,{top_uc}
Agrave = A + grave@center,{top_uc}
Ccedilla = C + cedilla@center,bottom
Eacute = E + acute@center,{top_uc}
Ecircumflex = E + circumflex@center,{top_uc}
Iacute = I + acute@center,{top_uc}
Oacute = O + acute@center,{top_uc}
Ocircumflex = O + circumflex@center,{top_uc}
Otilde = O + tilde@center,{top_uc}
Uacute = U + acute@center,{top_uc}

# lowercase
aacute = a + acute@center,{top_lc}
acircumflex = a + circumflex@center,{top_lc}
atilde = a + tilde@center,{top_lc}
agrave = a + grave@center,{top_lc}
ccedilla = c + cedilla@center,bottom
eacute = e + acute@center,{top_lc}
ecircumflex = e + circumflex@center,{top_lc}
iacute = dotlessi + acute@center,{top_lc}
oacute = o + acute@center,{top_lc}
ocircumflex = o + circumflex@center,{top_lc}
otilde = o + tilde@center,{top_lc}
uacute = u + acute@center,{top_lc}
```

Click on the *Update* button to refresh the preview. You should see something like this:

{% image how-tos/building-accented-glyphs_glyph-builder_2.png %}

This looks a lot better already. You can tweak the values of `top_uc` and `top_lc` until you are satisfied with the vertical position of the accents.

The horizontal positions are not quite there yet, though. Look at the accents over `a`, for example – they don’t really look centered. This is because we’re using the mathematical (calculated) center, not the optical one. We’ll improve that in the next step.

## Adjusting horizontal positions

The horizontal alignment of accents were specified with the [reference] positioning mode and the keyword `center`, which, in the absence of any anchors or guides, will use the calculated reference positions.

[reference]: http://github.com/typemytype/GlyphConstruction#by-reference

In this approach, anchors or guidelines are needed only for handling exceptions – in this case, glyphs in which the horizontal optical center is not the calculated one.

So, in order to fix the horizontal alignment of accents in `a`, we’ll add a {% internallink 'workspace/glyph-editor/guidelines' text='guideline' %} named `center` to override the calculated center position.

{% image how-tos/building-accented-glyphs_guideline.png %}

Update the preview, and see how the accents are now aligned to the guideline:

{% image how-tos/building-accented-glyphs_glyph-builder_3.png %}

That’s it! Quick and easy positioning of accents using just one guide and no anchors :)

> Don’t forget to save your Glyph Construction rules as a `.glyphConstruction` file when you’re done, so you can reuse them later with other fonts.
{: .tip }

## Building the accented glyphs

To build the accented glyphs in the current font, click on the *Build Glyphs* button. The following sheet with options will appear:

{% image how-tos/building-accented-glyphs_glyph-builder-options.png %}

Select the option *Auto Unicodes* if your accented glyphs are named according to the {% glossary AGL %} or {% glossary GNFUL %} lists. If you’ve used custom glyph names, you can define the unicodes directly in the glyph construction rules. For example:

```text
# uppercase
Aacute = A + acute@center,{top_uc} | 00C1
Acircumflex = A + circumflex@center,{top_uc} | 00C2
Atilde = A + tilde@center,{top_uc} | 00C3
...etc...
```

Finally, click on the *Build* button to build all glyphs in the font.

{% image how-tos/building-accented-glyphs_resulting-font.png %}

> - {% internallink 'building-accented-glyphs-with-a-script' %}
{: .seealso }

[Glyph Construction]: http://github.com/typemytype/glyphconstruction
