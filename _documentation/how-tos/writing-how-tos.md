---
layout: page
title: Writing How-To’s
tags:
  - documentation
level: intermediate
---

* Table of Contents
{:toc}

How-To’s are short articles which explain how to accomplish a specific task with RoboFont. They describe actions which the user has to perform in order to achieve a certain result.

RoboFont can do a lot of things, and a complete list of How-To’s would be gigantic. We are trying to cover the most common tasks first. The list of How-To’s will grow over time as users ask questions and/or contribute new topics.

## Recommendations for authors

How-To’s are written by multiple authors. In order to give this chapter a unified style and structure, we provide the following recommendations:

### Use descriptive titles

The title of a How-To should be formulated as a response to a typical user question.

Example:

- user question: *How Do I Do \<something\>?*
- How-To title: *Doing \<something\>*

How-To’s are focused on **actions**. To underline that, it is recommended to start the title of a How-To with a verb in its *-ing* form.

Examples:

- *Generating fonts*
- *Building accented glyphs*
- *Running scripts*
- *Using extensions*
- *Sorting glyphs*

See the {% internallink 'how-tos' text='complete list of How-To’s' %} for more examples.

> There will be cases in which this rule will not produce a good title. When that happens, try to find a way to rewrite the title while preserving the *Doing &lt;something&gt;* structure.
>
> Examples:
>
> - *Test installing* (sounds weird) → *Using Test Install* (better)
> - *Italic slanting* (sounds weird) → *Working with Italic Slant* (better)
{: .note }

### Keep it on-topic

A How-To should describe one task and one task only.

If a complex task can be broken down into two or more separate tasks, do it. Keep things modular whenever possible. Rules for writing good programs usually also apply to writing good documentation.

Example:

We want to explain users how to interpolate fonts. Interpolation involves two distinct steps:

1. making fonts compatible
2. interpolating

That’s a good case for breaking things down into 2 separate How-To’s:

1. *Preparing for Interpolation*
2. *Interpolating Fonts*

### Offer detailed instructions

Don’t assume previous knowledge from users, no matter how simple the subject is to you (author). The reader might be a complete beginner to RoboFont or to typeface design.

If previous knowledge is required, include links to documents (in the RoboFont documentation or elsewhere) where this knowledge is provided.

A How-To may include a brief introduction to offer some context on its subject. For example:

- A How-To explaining how to interpolate fonts may include a short introduction explaining what interpolation is, the basic maths behind it, etc.

- A How-To explaining how to use Test Install may include a short introduction about installing fonts in the OS, why it is often cumbersome, etc.

Most importantly, **a How-To should provide step-by-step instructions on how to perform a given task**. It helps to think of a How-To as a ‘program’ which the user will ‘execute’ manually on his machine, by himself. If a user cannot achieve the desired result after following the described steps, then the How-To is ‘buggy’ and must be amended or rewritten.

### Add structure to your content

Try to present your content in modular way, to make it easier for readers to skim through the page when looking for a particular bit of information.

Make use of the graphic and typographic elements provided by the {% internallink "styles-test" text="style guide" %} to articulate content: images, tables, code examples, notes etc.

### Provide tags

Tags offer an alternative way to organize content based on their *subjects*, not hierarchy. They make it easier for users to find information, so it’s important to use them.

When adding tags, have a look at the [How-To’s index](../) first, and try to use one of the existing tags. Create a new tag only as a last resort, if none of the current tags are applicable.

### Include author credits

How-To’s are written by multiple authors. It is recommended to include author credits in all contributed articles. Some examples:

- {% internallink 'how-tos/working-with-italic-slant' %}
- {% internallink 'how-tos/using-git' %}
- {% internallink 'how-tos/building-a-custom-key-observer' %}

If your How-To has been previously published elsewhere, consider mentioning it in a note at the top of the article.

## Submitting your article

How you choose to write your article is up to you. Some people use a code editor, others prefer to work with a word processor or a typesetting app.

### Submitting content without formatting

It’s fine to submit your contribution as a `.pdf` or `.rtf` file. We can take care of converting your content to [markdown] and adding it to the documentation.

> - how should authors submit their contributions? per email?
{: .todo }

### Submitting formatted content

If you can, please submit your contribution in markdown.

> This documentation uses a superset of markdown called **kramdown**. Have a look at the reference [here][kramdown].
{: .note }

This documentation uses a few custom plugins which add special features to markdown, such as internal links and downloadable code samples. See the source of the {% internallink "styles-test" text="style guide" %} for an overview.

### Submitting a pull request

If you are used to work with Git, you can take one step further and submit your contribution as a pull request. See {% internallink "editing-the-docs" %} for more info.

### Revision

Each submitted article is carefully read and reviewed. Sometimes it’s necessary to make small changes to the text.

> - add a comment about [Contributor Agreement](http://contributoragreements.org/)?
{: .todo }

## Testing and improving

It is important for authors to test their How-To’s with real RoboFont users (students, colleagues), and make improvements based on their feedback.

All RoboFont users can offer feedback on the documentation at any time – see {% internallink "giving-feedback-on-docs" %}.

[markdown]: http://en.wikipedia.org/wiki/Markdown
[kramdown]: http://kramdown.gettalong.org/quickref.html
