---
layout: page
title: Adding and removing glyphs
tags:
  - character set
level: beginner
---

* Table of Contents
{:toc}

*This page gives an overview of multiple ways to add and remove glyphs in RoboFont.*

## Adding glyphs using the Add Glyphs sheet

To open the *Add Glyphs* sheet, choose *Font > Add Glyphs* from the {% internallink "workspace/application-menu" %} or use the shortcut keys ⌃ ⌘ G:

{% image how-tos/adding-and-removing-glyphs_add-glyphs-sheet.png %}

In the main input area, fill in the names of the glyphs you would like to create as a space-separated list. These will be created as empty glyphs, or as template glyphs if the option *Add As Template Glyphs* is selected.

### Options

<table>
  <thead>
    <tr>
      <th width="35%">option</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Import glyph names from…</td>
      <td>Import glyph names from <a href="../../workspace/preferences-window/character-set">saved character sets</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add unicode</td>
      <td>Try to set the unicode values for the given glyph names automatically.</td>
    </tr>
    <tr>
      <td>Sort Font</td>
      <td>Sort the glyphs in the font alphabetically.</td>
    </tr>
    <tr>
      <td>Overwrite Glyphs</td>
      <td>Overwrite glyphs if they already exist in the font.</td>
    </tr>
    <tr>
      <td>Add As Template Glyphs</td>
      <td>Add glyphs as template glyphs only. If this option is unchecked, empty new glyphs will be created instead.</td>
    </tr>
  <tr>
      <td>Mark Color</td>
      <td>Click on the color swatch to open a color picker, and choose a mark color for the new glyphs.</td>
    </tr>
  </tbody>
</table>

### Glyph construction rules

The Add Glyphs sheet supports a very basic glyph construction syntax to build new glyphs using components:

```text
uni0430=a
aacute=a+acute
aringacute=a+ring+acute
```

Use `@` to align components using anchors:

```text
aacute=a+acute@top
```

> For this example to work, the base glyph `a` must have an anchor named `top`, and the accent glyph `acute` must have an anchor named `_top`.
{: .note }

It’s also possible to assign a unicode value to a glyph:

```text
aacute=a+acute@top|00E0
```

> The [Glyph Construction] language provides a more powerful syntax for building new glyphs from components. See {% internallink 'building-accented-glyphs-with-glyph-construction' %}.
{: .seealso }

[Glyph Construction]: http://github.com/typemytype/GlyphConstruction

## Removing glyphs

1. select the glyphs to be removed in the {% internallink 'workspace/font-overview' %}
2. press the backspace key

> When a glyph is deleted from the font, it is also automatically removed from `font.glyphOrder`.
{: .note }

Depending on your settings in {% internallink "workspace/preferences-window/character-set" %}, the deleted glyphs will also be removed from groups and/or kerning pairs.

{% image workspace/preferences_character-set_delete-glyph.png %}

> Removing glyphs from groups and/or kerning applies only when deleting glyphs manually in the Font Overview.
>
> Glyphs which are deleted via scripts are **not** removed from groups or kerning!
{: .warning }

> Deleted glyphs are **not** removed automatically from OpenType features!
>
> If the OpenType compiler encounters a glyph which is not in the font, it will raise an error and the font will not be generated.
{: .warning }

### Removing template glyphs

Use ⌥ + backspace to remove [template glyphs](../../workspace/font-overview/#template-glyphs) from the font.

## Adding and removing glyphs with Python

### Adding glyphs

Here’s an example script showing how to add glyphs to a font, including a check to avoid overwriting existing glyphs:

```python
font = CurrentFont()

# add a new glyph to the font
# if the glyph already exists, it will be overwritten
font.newGlyph('hello')

# use clear=False if you don't want to overwrite an existing glyph
font.newGlyph('A', clear=False)

# add several glyphs at once with a loop
newGlyphNames = ['test1', 'test2', 'test3']
for glyphName in newGlyphNames:
    font.newGlyph(glyphName)
```

### Removing glyphs

Here’s an example script showing how to remove a glyph, including a check to avoid an error if the glyph is not in the font:

```python
font = CurrentFont()

glyphName = 'hello'

# before removing a glyph, make sure it actually exists in the font
if glyphName in font:
    del font[glyphName]
else:
    print("font does not contain a glyph named '%s'" % glyphName)

# if we try to remove a glyph which is not in the font,
# and error will be raised
del font['spam']
```

```console
Traceback (most recent call last):
  File "<untitled>", line 3, in <module>
  File "/Applications/RoboFont.app/Contents/Resources/lib/python3.6/fontParts/base/layer.py", line 136, in __delitem__
ValueError: No glyph with the name 'spam' exists.
```

> `font.removeGlyph(glyphName)` is deprecated!
{: .warning }

> When a glyph is removed from a font with a script, it is **not** automatically removed from groups, kerning, or components. The [UFO3 specification] allows groups, kerning and components to reference glyphs which are not in the font.
{: .note }

[UFO specification]: http://unifiedfontobject.org/versions/ufo3/

### Removing glyphs from groups

Here’s a script showing how to remove all occurrences of a glyph in groups:

```python
font = CurrentFont()

# glyph to be removed
glyphName = 'B'

# iterate over all groups in the font
for groupName in font.groups.keys():

    # get the group as a list of glyph names
    group = list(font.groups[groupName])

    # if the glyph is in the group:
    if glyphName in group:

        # remove the glyph name
        print("removing '%s' from group '%s'..." % (glyphName, groupName))
        group.remove(glyphName)

        # put the group back into the font
        font.groups[groupName] = group
```

### Removing glyphs from kerning

Here’s a script showing how to remove all kerning pairs which include a glyph:

```python
font = CurrentFont()

# the glyph to be removed
glyphName = 'A'

# iterate over all kerning pairs in the font
for kerningPair in font.kerning.keys():

    # if the glyph is in the kerning pair:
    if glyphName in kerningPair:

        # remove the kerning pair
        print('removing kerning pair (%s, %s)...' % kerningPair)
        del font.kerning[kerningPair]
```

### Removing glyphs from components

Here’s an example showing how to remove all components referencing a glyph:

```python
font = CurrentFont()

# the glyph to be removed
glyphName = 'a'

# iterate over all glyphs in the font
for glyph in font:

    # skip glyphs which components
    if not glyph.components:
        continue

    # iterate over all components in glyph
    for component in glyph.components:

        # if the base glyph is the glyph to be removed
        if component.baseGlyph == glyphName:
            # delete the component
            glyph.removeComponent(component)
```
