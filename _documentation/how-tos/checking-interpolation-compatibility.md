---
layout: page
title: Checking interpolation compatibility
tags:
  - interpolation
level: intermediate
draft: true
---

* Table of Contents
{:toc}

The easiest and most efficient way to check (and fix) interpolation compatibility between fonts is using [Prepolator].

[Prepolator]: http://extensionstore.robofont.com/extensions/prepolator/

This page shows how to check interpolation compatiblity with code.

## Checking glyph compatibility

The `RGlyph` object has a `isCompatible` method which checks for interpolation compatibility with another glyph. It returns a boolean (indicating if the glyphs are or not compatible) and a string (a report about the glyphs).

```python
f1 = AllFonts().getFontsByStyleName('LightCondensed')[0]
f2 = AllFonts().getFontsByStyleName('LightWide')[0]
print(f1['a'].isCompatible(f2['c']))
```

```console
>> (False, ["Fatal error: glyph a and glyph c don't have the same number of contours."])
```

```python
print(f1['a'].isCompatible(f2['d']))
```

```console
>> (False, ["Fatal error: contour 1 in glyph a and glyph d don't have the same number of segments."])
```

```python
print(f1['a'].isCompatible(f2['a']))
```

```console
>> (True, [''])
```

## Marking glyphs by compatibility

The following script checks for glyph compatibility between all open fonts and a neutral font, and applies a mark color based on the result (green for compatible, red for not compatible).

{% showcode how-tos/checkCompatibility.py %}

{% image how-tos/check-interpolation-compatibility_mark-glyphs.png %}

## Setting starting points automatically

```python
g = CurrentGlyph()

for c in g.contours:
    c.autoStartSegment()
```

> - {% internallink 'how-tos/preparing-for-interpolation' %}
{: .seealso }
