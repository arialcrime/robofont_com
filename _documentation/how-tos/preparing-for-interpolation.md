---
layout: page
title: Preparing for Interpolation
tags:
  - interpolation
level: intermediate
---

> Adapted from the original RoboFab documentation.
{: .note }

* Table of Contents
{:toc}

Building an interpolation system has 2 phases: preparing it, and using it. Though this isn’t necessarily a design / production difference.

In the first phase you need to make sure that all data is set up properly, with compatible glyphs in each master, matching contours in each glyph, path directions, start point locations etc. This can be a lot of work, but FontParts can assist by reporting the problems.

The second phase – using the interpolation – comes when everything works, and rather than look for problems (there shouldn’t be any) you just want to generate all the weights as fast as possible, and get on with proofing and other things.

## Interpolation requirements

Two glyphs are compatible for interpolation if they have:

- the same amount of contours
- the same order of contours
- the same number of points in contour
- matching starting points in contour
- matching contour directions

## Compatibility problems

The following table shows the problems glyphs can have when interpolating.

compatible
: means that the data can interpolate

functioning
: means that the result actually works as a glyph

You’ll see that there are several combinations where glyphs are compatible, but the interpolation is not functional.

<table>
  <tr>
    <th width="40%">masters</th>
    <th width="30%">description</th>
    <th width="30%">result</th>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_01.gif %}</td>
    <td>Same number of points, same direction, same start point location.</td>
    <td><span class='green'>Compatible and functioning.</span></td>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_02.gif %}</td>
    <td>
      The number of off-curve points differ, but these are assumed to be on top of the on-curve when missing.
      <br/><em>This only works for segments with 2 off-curve points.</em>
    </td>
    <td><span class='green'>Unusual, but compatible and functioning.</span></td>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_03.gif %}</td>
    <td>Same number of points, same direction, same start point location, same contour order.</td>
    <td><span class='green'>Compatible and functioning.</span></td>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_04.gif %}</td>
    <td>Different number of points.</td>
    <td><span class='red'>Incompatible and not functioning.</span></td>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_05.gif %}</td>
    <td>Start point is in the wrong place.</td>
    <td><span class='yellow'>Compatible but not functioning.</span></td>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_06.gif %}</td>
    <td>Different number of contours.</td>
    <td><span class='red'>Incompatible and not functioning.</span></td>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_07.gif %}</td>
    <td>One of the contours is turning in the wrong direction.</td>
    <td><span class='yellow'>Compatible but not functioning.</span></td>
  </tr>
  <tr>
    <td>{% image building-tools/interpolation/compatibility-scheme_2_08.gif %}</td>
    <td>Contour order: the paths are in the wrong order.</td>
    <td><span class='yellow'>Compatible but not functioning.</span></td>
  </tr>
</table>

> - {% internallink 'how-tos/checking-interpolation-compatibility' %}
{: .seealso }
