---
layout: page
title: Defining character sets
tags:
  - character set
level: beginner
---

* Table of Contents
{:toc}

A character set is a list of glyph names, in a particular order. It defines which glyphs are included in a font, and determines which languages are supported.

{% comment %}
> Character sets are just one way of looking at the glyphs included in a font. See {% internallink "using-smart-sets" %} for another approach.
{% endcomment %}

> - {% internallink 'workspace/smart-sets-panel' %}
{: .seealso }

## Managing character sets

In RoboFont, character sets can be created, edited and deleted in the {% internallink 'preferences-window/character-set' %}:

{% image how-tos/defining-character-sets_character-set-preferences.png %}

Each character set is defined as a space-separated list of glyph names.

Glyphs which are made out of one or more components can be expressed as simple glyph construction formulas: `ccedilla=c+cedilla`. Unicode values can also be assigned. Here are some more examples:

```text
A grave Agrave=A+grave Aringacute=A+ring+acute acute|00B4 Aacute=A+acute|00C1
```

> {% internallink 'building-accented-glyphs-with-glyph-construction' text="Glyph Construction" %}, a more powerful language for creating glyphs from components.
{: .seealso }

## Using standard character sets

If you are new to making fonts, it’s probably a good idea to use an existing character set as a starting point. This is a good way to learn and benefit from the experience of other font makers. As time goes by, you’ll start making changes based on your own preferences.

> For general purpose fonts, Adobe’s [Latin Character Sets][Adobe Latin Character Sets] are a good place to start. Underware’s [Latin Plus] is another useful reference, with very detailed information about individual languages.
{: .tip }

> If you work for Greek or Cyrillic scripts, have a look at Adobe’s [Greek][Adobe Greek Character Sets] and [Cyrillic][Adobe Cyrillic Character Sets] Character Sets.
{: .seealso }

[Latin Plus]: http://www.underware.nl/latin_plus/
[Adobe Latin Character Sets]: http://github.com/adobe-type-tools/adobe-latin-charsets
[Adobe Greek Character Sets]: http://github.com/adobe-type-tools/adobe-greek-charsets
[Adobe Cyrillic Character Sets]: http://github.com/adobe-type-tools/adobe-cyrillic-charsets

> Note that these character sets include only the basic encoded glyphs; unencoded glyphs which are used by OpenType features (small caps, different sets of figures, stylistic alternates etc.) are not listed.
{: .note }

## Examples

Below are some example character sets, formatted as space-separated lists:

### ASCII

{% showcode how-tos/ascii.txt text %}

### Adobe Latin-1

{% showcode how-tos/adobe-latin-1.txt text %}

### Adobe Latin-2

{% showcode how-tos/adobe-latin-2.txt text %}

### Adobe Latin-3

{% showcode how-tos/adobe-latin-3.txt text %}

### Underware Latin Plus

{% showcode how-tos/underware-latin-plus.txt text %}

## Production names vs. final glyph names

> - {% internallink "using-production-names" %}
{: .seealso }
