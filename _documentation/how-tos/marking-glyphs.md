---
layout: page
title: Marking glyphs with color
tags:
  - character set
  - designing
level: beginner
---

* Table of Contents
{:toc}

Every glyph in a font supports a mark color, which is used to paint the background of a glyph cell in the Font Overview window.

Colors help to visually organize your font.

There are different ways to apply colors to glyphs:

## Using a predefined color

A predefined color is a special kind of color which has a name, and is stored with your Preferences. You can use color names to assign meanings to colors.

For example, colors could signify the status of glyphs in a font: green meaning approved, yellow meaning under development, and red meaning rejected.

{% image how-tos/marking-glyphs_colors-preferences.png %}

You are free to use your imagination and create your own color coding systems.

### Creating predefined colors

Predefined colors are created in {% internallink "preferences-window/miscellaneous" %}:

1. click on the + button to create a new color
2. double-click the color swatch to adjust the color using the color picker
3. double-click the color name to edit it

### Applying predefined colors

Predefined colors appear in the toolbar at the top of the Font Overview window.

{% image how-tos/marking-glyphs_toolbar-icon.png %}

To apply a predefined color:

1. select one or more glyphs in the Font Overview
2. click on the Mark icon in the toolbar to reveal a list of all predefined colors
3. choose the desired color from the list

{% image how-tos/marking-glyphs_colors-toolbar.png %}

## Using an arbitrary color

A glyph’s mark color can be any color, not just predefined ones.

To assign an arbitrary mark color to a glyph, use the {% internallink "inspector#glyph" text="Glyph section of the Inspector panel" %}: click on the Mark color swatch to open a color picker, and choose the desired color.

> It is not possible to apply a color to multiple glyphs at once using the Inspector.
{: .note }

## Applying an arbitrary color with a script

If you need to apply an arbitrary color to several glyphs at once, you can use a simple script:

```python
font = CurrentFont()

color = 1, 0, 0, 0.7

for glyph in font.selectedGlyphs:
    glyph.markColor = color
```

> {% internallink "sorting-glyphs" %}
> : Use mark colors to sort the glyphs in your font.
>
> {% internallink "workspace/search-glyphs-panel" %}
> : Use mark colors to search for glyphs in a font, and to create Smart Sets.
{: .seealso }
