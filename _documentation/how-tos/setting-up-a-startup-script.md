---
layout: page
title: Setting up start-up scripts
tags:
  - scripting
level: beginner
draft: true
---

* Table of Contents
{:toc}

## What is a start-up script?

A start-up script is a script which is executed when RoboFont is initialized.

Start-up scripts are typically used to add custom observers, menu items or toolbar buttons to RoboFont. They can be included in extensions, or added manually in the Preferences.

*This article explains how to add a start-up script manually using the {% internallink 'workspace/preferences-window/extensions' %}.*

## An example script

As an example, we’ll use the script below to make your computer read a welcome message every time RoboFont is launched.

{% showcode how-tos/welcomeToRoboFont.py %}

Save this script somewhere in our computer.

## Editing the Start-Up Scripts Preferences

Now go to the {% internallink 'workspace/preferences-window/extensions' %}, select the *Start Up Scripts* tab, and use the `+` button to add the script to the list.

{% image workspace/preferences_extensions-startup.png %}

Apply the changes, and restart RoboFont.

You should now hear a welcome message every time RoboFont is launched!
