---
layout: page
title: Converting from cubic to quadratic curves
tags:
  - TrueType
  - contours
level: intermediate
draft: true
draft-hidden: true
---

* Table of Contents
{:toc}

## Converting from cubic to quadratic using a script

The script below converts all glyphs in the current (cubic) font to quadratic.

The conversion is made using RoboFont’s cubic-to-quadratic algorithm, which adds an implied on-curve point and splits each curve segment in two.

{% showcode how-tos/convertCubicToQuadratic.py %}

## Converting to quadratic with QuadraticConverter

[QuadraticConverter] is a RoboFont extension to convert from cubic to quadratic contours. It provides some options and a live preview to fine-tune the result.

QuadraticConverter can be {% internallink 'extensions/installing-extensions-mechanic' text='installed with Mechanic 2' %}.

Once installed, the QuadraticConverter dialog can be opened from the *Extensions* menu:

{% image how-tos/converting-from-cubic-to-quadratic_quadratic-converter.png %}

Use the sliders to adjust the *Maximum Distance* and *Minimum Segment Length* parameters.

If a glyph window is open, a live preview will be displayed for the current glyph.

{% image how-tos/converting-from-cubic-to-quadratic_quadratic-converter-preview.png %}

Click on *Convert Font* to convert all glyphs in the current font to quadratic, and save the result as a new UFO file.

{% image how-tos/converting-from-cubic-to-quadratic_quadratic-converter-result.png %}

[QuadraticConverter]: http://github.com/BlackFoundry/QuadraticConverter

> - {% internallink "drawing-with-quadratic-curves" %}
{: .seealso }
