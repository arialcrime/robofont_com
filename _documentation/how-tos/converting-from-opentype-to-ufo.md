---
layout: page
title: Converting from OpenType to UFO
tags:
  - UFO
  - OpenType
level: intermediate
draft: true
---

* Table of Contents
{:toc}

## Opening OpenType fonts

OpenType fonts can be opened in RoboFont in the same way as UFOs: using the *Open Font* dialog, or with a script:

```python
f = OpenFont('myFont.otf')
```

### What is imported

Glyph data
: from the `glyf` and `cff ` tables

Kerning & Groups
: from the `kern` table

Font Info
: ^
  from various tables:

  - `head`
  - `name`
  - `OS/2`
  - `hhea`
  - `post`
  - `gasp`

> - [extractor > OpenType](http://github.com/robotools/extractor/blob/master/Lib/extractor/formats/opentype.py)
{: .seealso }

### What is not imported

OpenType features
: present only in compiled form, cannot be imported as readable code

## Saving OpenType fonts to UFO

Use the *Save* dialog to save an open OpenType font to UFO, just as you would do with an UFO font.

### UFO lib key

When saving an OpenType font to UFO, RoboFont stores the path of the original binary font in the `font.lib` under the key:

```text
com.typemytype.robofont.binarySource
```

> This key is used by the [Batch] extension, for example.
{: .note }

[Batch]: http://github.com/typemytype/batchRoboFontExtension
