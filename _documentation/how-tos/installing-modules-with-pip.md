---
layout: page
title: Installing Python modules with pip
tags:
  - TrueType
  - contours
level: intermediate
draft: true
draft-hidden: true
---

get all the latest versions and updates directly from published packages or directly from git

```text
pip3.6 install -r requirement.txt
```

example:

```text
http://github.com/typemytype/drawbot/blob/master/requirements.txt
```

get a specific version:

```text
pip install sphinx==1.5.6
```

super easy to install for py2

```text
pip2 …
```

or

```text
py3 pip3
```

or even

```text
pip3.6 pip3.7
```

> - [pip User Guide](http://pip.pypa.io/en/stable/user_guide/)
{: .seealso }
