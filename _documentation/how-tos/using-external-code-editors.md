---
layout: page
title: Using external code editors to run scripts in RoboFont
tags:
  - scripting
level: intermediary
---

* Table of Contents
{:toc}

RoboFont provides its own {% internallink 'workspace/scripting-window' %} for writing and running scripts. Thanks to the RoboFont shell command, you can also use an external code editor to write your scripts and have them execute inside RoboFont.

## Installing RoboFont as a shell command

In order to use your code editor’s build system with RoboFont, you’ll need to install the RoboFont shell command first. To do that, go to *[Preferences > Extensions > Shell]* and click on the *Install* button.

To check if the command is working: open the Terminal, type `robofont -h` and hit return. The command line help will be printed to the console.

> For the RoboFont shell command to work correctly you’ll probably need to install [PyObjC] in Terminal using `pip3`.
{: .note }

[Preferences > Extensions > Shell]: ../../workspace/preferences-window/extensions/#shell
[PyObjC]: http://pyobjc.readthedocs.io/

## Configuring your external editor

> In order to run a script in RoboFont, you’ll need to save it first.
{: .warning}

### Sublime Text

*Steps for setting up a RoboFont build system in [SublimeText], contributed by [Bahman Eslami][SublimeText gist].*

{% showcode how-tos/Robofont.sublime-build %}

1. Save the file above inside the SublimeText user folder:

    ```text
    Library/Application Support/Sublime Text 3/Packages/User
    ```

2. In the SublimeText main menu, go to *Tools > Build System* and select *RoboFont*.

3. Run the script using the standard keyboard shortcut ⌘ + b, and the code will be executed inside RoboFont.

[SublimeText]: http://www.sublimetext.com/3
[SublimeText gist]: http://gist.github.com/typoman/1180765bc0ef06432d4640f41604acc9

### Atom

*Steps for setting up a RoboFont build system in [Atom], contributed by [Rafał Buchner][Atom gist].*

{% showcode how-tos/atom-shell-commands-robofont-config.cson %}

1. Install [atom-shell-commands](http://atom.io/packages/atom-shell-commands).

2. In the Atom main menu, go to *File > Open Your Config* to open your config file.

3. To add a command to your `atom-shell-commands` configurations, copy the above code after the line `*:`.

4. Run the script, and the code will be executed inside RoboFont.

[Atom]: http://atom.io/
[Atom gist]: http://gist.github.com/RafalBuchner/5affdfafc76785c637ea7c5d2a035787

### Visual Studio Code

*Steps for setting up a RoboFont build system in [VS Code], contributed by [Stephen Nixon][VS Code gist].*

{% showcode how-tos/tasks.json %}

1. In VS Code, open a folder that you wish to run scripts from. This might be a type project, the central RoboFont scripts folder, or somewhere else. VS Code calls this a “workspace”, and you can give it specific settings in a `.vscode` folder.

2. Use the shortcut ⌘ + ⇧ + p to open the *Command Palette*.

3. Search for *Tasks: Configure Default Build Task* and select it.

4. Select *Create tasks.json file from template*, and finally *Other*. This will create the file `tasks.json` in a `.vscode` workspace folder.

5. Paste the above `tasks.json` code in place of the template.

6. Run the script using the standard keyboard shortcut ⌘ + ⇧ + b, and the code will be executed inside RoboFont.

> - Yes, the version number is `2.0.0` because it refers to VS Code’s task system.
> - If you are managing the workspace as a Git project, you should probably add `.vscode` to your `.gitignore` file.
{: .note }

[VS Code]: http://code.visualstudio.com
[VS Code gist]: http://gist.github.com/thundernixon/588273076abfee345ad17c21a6b70389

### PyCharm

*Steps for setting up a RoboFont build system in [PyCharm], contributed by [Rafał Buchner][PyCharm gist].*

1. In the PyCharm main menu, go to *Preferences > External Tools*.

2. Click on the + button to create a new external tool.

3. Fill in the *Name* field with the name of the tool, for example “RoboFont”.

4. Fill in the remaining fields like this:

    Program
    : `roboFont`

    Arguments
    : `-p $FilePath$`

    Working directory
    : `$Projectpath$`

5. Optionally, you can choose the name of the group for this tool – it is set to *External Tools* by default.

6. Click on *OK* and then on *Apply* to save your changes.

7. Run the script using the main menu *Tools > External Tools > RoboFont*, or with *External Tools > RoboFont* from the contextual menu (use right click to display it).

> You can define a keyboard shortcut for this command in the PyCharm Preferences.
{: .tip }

[PyCharm]: http://www.jetbrains.com/pycharm/
[PyCharm gist]: http://gist.github.com/RafalBuchner/1eeec1105b5eb15215a8a7c9e7790fab

### VIM

*Steps for setting up a RoboFont build system in [VIM], contributed by [Jan Šindler].*

1. Open your `.vimrc` config file.

2. Add `nnoremap <silent> <F3> :!robofont "%:p"<CR>` to run your files from **normal** mode.

3. Add `inoremap <silent> <F3> <ESC> :!robofont "%:p"<CR>` to run your files from **insert** mode.

4. Save the changes.

5. Run the script using the key to which the commands were assigned (in the example above, F3). The code will be executed inside RoboFont.

> - You can choose other keys to run your files in RoboFont.
> - In macOS, this example [.vimrc file] can be found in the `~` folder (user home directory).
{: .tip }

[VIM]: https://www.vim.org/
[Jan Šindler]: https://github.com/jansindl3r
[.vimrc file]: https://vim.fandom.com/wiki/Example_vimrc
