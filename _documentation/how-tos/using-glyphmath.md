---
layout: page
title: Using GlyphMath
tags:
  - interpolation
level: intermediate
---

> Adapted from the original RoboFab documentation.
{: .note }

If they’re compatible, Glyph objects can be used in Python math expression: you can add, subtract, multiply and divide them like normal numbers or variables. The math is applied to the coordinates of each point in the glyph. The result of a GlyphMath operation is a new glyph. You can then insert this glyph in a font, or use it for other math operations.

<table>
  <tr>
    <th width="20%">operation</th>
    <th width="45%">GlyphMath</th>
    <th width="35%">description</th>
  </tr>
  <tr>
    <td>addition</td>
    <td>{% image building-tools/interpolation/glyphmath_examples_01.gif %}</td>
    <td>The coordinates of each point are added.</td>
  </tr>
  <tr>
    <td>subtraction</td>
    <td>{% image building-tools/interpolation/glyphmath_examples_02.gif %}</td>
    <td>
      <p>The coordinates of each point are subtracted.</p>
      <p>Note that although the glyph looks unrecognisable, all points are still there. Literally the difference between the two glyphs.</p>
    </td>
  </tr>
  <tr>
    <td>multiplication</td>
    <td>{% image building-tools/interpolation/glyphmath_examples_03.gif %}</td>
    <td>
      <p>Scaling the glyph up.</p>
      <p>When you multiply with a tuple like <code>(1.3,1.05)</code>, the first value is used to multiply the x coordinates, the second value is used for the y coordinates.</p>
    </td>
  </tr>
  <tr>
    <td>division</td>
    <td>{% image building-tools/interpolation/glyphmath_examples_04.gif %}</td>
    <td>
      <p>Scaling the glyph down.</p>
      <p>When you divide with a tuple like <code>(30,29)</code>, the first value is used to divide the x coordinates, the second value is used for the y coordinates.</p>
    </td>
  </tr>
  <tr>
    <td><em>various</em></td>
    <td>{% image building-tools/interpolation/glyphmath_examples_05.gif %}</td>
    <td>Combination of operations to make a real interpolation.</td>
  </tr>
</table>

You can use GlyphMath to create interpolation effects, transplant transformations from one glyph to another and superimpose several effects at once.

{% showcode building-tools/interpolation/GlyphMathExample.py %}
