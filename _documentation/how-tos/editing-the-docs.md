---
layout: page
title: Editing the documentation
tags:
  - documentation
level: intermediate
---

* Table of Contents
{:toc}

The RoboFont documentation is open-source, and is [hosted on Gitlab]. Users familiar with Git-based workflows can fork the repository, make changes, and submit a pull request.

> You’ll need to [create an account on GitLab].
{: .note }

[hosted on Gitlab]: http://gitlab.com/typemytype/robofont_com/
[create an account on GitLab]: http://gitlab.com/users/sign_in

## Documentation folder structure

The documentation is a large project with many files and folders, so it may be difficult to find what you’re looking for the first time.

This is the folder structure of the project:

> robofont_com
> │
> ├── _data
> ├── _documentation
> │   ├── building-tools
> │   │   ├── api
> │   │   ├── extensions
> │   │   ├── python
> │   │   ├── toolkit
> │   │   └── tools
> │   │       ├── drawbot
> │   │       ├── interactive
> │   │       ├── mojo
> │   │       ├── observers
> │   │       └── scripts
> │   ├── extensions
> │   ├── how-tos
> │   ├── introduction
> │   └── workspace
> ├── _sass
> ├── assets
> │   └── code
> ├── images
> └── style.scss
>
> \* only relevant files & folders shown
{: .asCode }

And here are some notes about what you can find in each folder:

<table>
  <tr>
    <th width="25%">file or folder</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td><pre>_data</pre></td>
    <td>
      <p>Contains structured data in <code>.yml</code> format.</p>
      <p>For example:<br/>
        <ul>
          <li>extension metadata from Mechanic, Extension Store</li>
          <li>glossary of common terms and their meaning</li>
        </ul>
      </p>
    </td>
  </tr>
  <tr>
    <td><pre>_documentation</pre></td>
    <td>
      <p>Contains all markdown sources for the documentation.</p>
      <p>Each subfolder corresponds to a chapter.</p>
      <p>The largest chapter, <em>Building Tools</em>, is further subdivided into subchapters and sections.</p>
    </td>
  </tr>
  <tr>
    <td><pre>_sass</pre></td>
    <td>
      <p>Contains <code>.scss</code> variables and modules which are used by the main stylesheet (<code>style.scss</code>).</p>
      <p><code>_variables.scss</code> defines global parameters such as section colors, box margins and colors, corner rounding, fonts etc.</p>
    </td>
  </tr>
  <tr>
    <td><pre>assets/code</pre></td>
    <td>
      <p>Contains downloadable code examples, organized into subfolders.</p>
      <p>Code examples are included into pages with the <code>{% raw %}{% showcode %}{% endraw %}</code> directive.</p>
    </td>
  </tr>
  <tr>
    <td><pre>images</pre></td>
    <td>
      <p>Contains images used in the documentation and website, organized into subfolders.</p>
      <p>Images are included into pages with the <code>{% raw %}{% image %}{% endraw %}</code> directive.</p>
    </td>
  </tr>
  <tr>
    <td><pre>style.scss</pre></td>
    <td>
      <p>The full stylesheet for the website and documentation, importing variables and modules contained in the <code>_sass</code> folder.</p>
    </td>
  </tr>
</table>

## Editing content with Git

1. Fork the [robofont_com](http://gitlab.com/typemytype/robofont_com/) repository (use the *Fork* button on the project’s homepage).

2. Make a local copy of your fork:

   ```text
   cd /path/to/folder/
   git clone git@gitlab.com:myusername/robofont_com.git
   ```

3. Make changes to existing files or add new files.

   > If you have Jekyll installed, you can preview your changes locally in your browser. See {% internallink "how-tos/previewing-documentation" %}.
   {: .seealso }

4. When you’re done, commit your changes to your repository:

   ```text
   git add --all
   git commit -m "fixing typo in Editing the Docs"
   git push
   ```

5. Submit your changes to the main `robofont_com` repository.

   Use the sidebar menu to go to *Merge Requests*, and choose *New merge request*.

   Select your repository as the *source branch*, and `typemytype/robofont_com` (master) as the target repository. Click on *Compare branches and continue*.

   Add a title and description to your merge request, and click on *Submit merge request* when you’re finished.

## Editing content with the GitLab web interface

If you are not comfortable with using Git from the command-line, you can perform the same tasks using GitLab’s web interface.

1. Fork the [robofont_com](http://gitlab.com/typemytype/robofont_com/) repository (use the *Fork* button on the project’s homepage).

2. Navigate through the projects’s folders and go to the the file you wish to edit.

   The document you are reading now, for example, is the file `/_documentation/how-tos/editing-the-docs.md`.

3. Click on the *Edit* button at the top right of the file header. This will open a simple code editor, which you can use to edit the document’s markdown source code.

   Don’t forget to include a clear and concise commit message about your changes.

   When you’re finished, use *Commit Changes* button to store the new version of the document in your repository.

4. Submit a merge request via GitLab (see step 5 in the above section).

## Review and merge

Your merge request will be reviewed, and if the changes are approved, your commit will be merged into the main repository. The RoboFont website is within a few minutes.

> - add a comment about [Contributor Agreement](http://contributoragreements.org/)?
{: .todo }
