---
layout: page
title: Working with mark colors
tags:
  - colors
  - character set
level: beginner
draft: false
---

* Table of Contents
{:toc}

Each glyph in a UFO font can have a mark color attached to it. Mark colors are displayed in the {% internallink 'workspace/font-overview' %}, and are useful for organising and classifying glyphs during production.

{% image how-tos/working-with-colors_font-overview.png %}

*This page gives an overview of how to create and use mark colors in RoboFont.*

## Creating custom mark colors

Custom mark colors can be created in the *Mark Colors* section of {% internallink 'workspace/preferences-window/miscellaneous' text='Preferences > Miscellaneous' %}. Custom colors can have a name, so you can assign meanings to colors and create your own color coding schemes.

{% image how-tos/working-with-colors_preferences.png %}

Custom colors are saved with the user preferences and are available for all fonts.

## Applying mark colors

Custom mark colors can be applied to selected glyphs using the *Mark* button in the Font Overview. Clicking on the button will display a list of custom colors to choose from.

{% image how-tos/working-with-colors_apply.png %}

Mark colors can also be applied using the *Mark* button in the *[Inspector > Glyph]*. Clicking on the color swatch will open a *Colors* window with several options. The custom colors defined in the Preferences are available in the section *Color Palette* (third icon in the toolbar).

{% image how-tos/working-with-colors_colors-window.png %}

[Inspector > Glyph]: ../../workspace/inspector/#glyph

## Searching for colors

Mark colors make it easier to search for glyphs visually in the Font Overview. We can also perform search queries in the {% internallink 'workspace/search-glyphs-panel' %} using custom mark colors as parameters:

{% image how-tos/working-with-colors_search.png %}

## Sorting by colors

Colors can also be used for [sorting the glyphs][Sorting glyphs with List mode] in the Font Overview. This can be done by switching to [List Mode] view and clicking on the *Mark* column header:

{% image how-tos/working-with-colors_sorting-1.png %}

The new sorting order is maintained if you switch back to *Tiles Mode*:

{% image how-tos/working-with-colors_sorting-2.png %}

[Sorting glyphs with List mode]: ../sorting-glyphs/#sorting-glyphs-using-font-overviews-list-mode
[List mode]: ../../workspace/font-overview/#list-mode
