---
layout: page
title: Working with fonts with a large amount of glyphs
tags:
  - UFO
  - character set
level: beginner
---

* Table of Contents
{:toc}

When RoboFont opens a font in the interface, it needs to load data from *all glyphs* in the font – to render the glyph cells of the {% internallink 'workspace/font-overview' %}, and to build the font’s unicode map (used when typing text in the {% internallink 'workspace/space-center' %}, for example).

In the {% internallink 'introduction/the-ufo-format' text='UFO format' %}, each glyph is stored as a separate [.glif] file inside the UFO package; the more glyphs in a font, the more individual files need to be parsed. This is why fonts with a large amount of glyphs can have a negative impact on the speed and performance of RoboFont.

[.glif]: http://unifiedfontobject.org/versions/ufo3/glyphs/glif/

> One of the ideas under consideration for UFO4 is introducing a new `charactermapping.plist` file to collect the font’s *character → glyph name* mapping. (The `<unicode>` element would be removed from `.glif` to prevent duplicate data.) Having a central character map would greatly reduce font loading time for UFO fonts.
{: .note }

To make the opening of large fonts faster, RoboFont has a setting to control how many glyphs are loaded when a font is first opened. The remaining glyphs can be loaded later, if required, by using the *Load All Glyphs* button.

## Setting the maximum amount of glyphs

Use the setting *Max glyph count to read instantly from disk* in the {% internallink "workspace/preferences-window/character-set" %} to control the maximum amount of glyphs loaded when a font is opened. The default value is 2000 glyphs.

{% image how-tos/working-with-large-fonts_max-glyph-count-preference.png %}

> This preference is stored internally as `loadFontGlyphLimit`, and can also be edited with the {% internallink "workspace/preferences-editor" %}.
{: .note }

## Opening Large Fonts warning

When opening a font which contains more glyphs than the limit defined in the *Max glyph count* preference, a pop-up window with a warning will appear:

{% image how-tos/working-with-large-fonts_opening-large-fonts-popup.png %}

Click on the *OK* button to close the dialog. Check the box if you don’t want RoboFont to show this message again.

> If you have previously selected *Don’t show this message again*, and wish to undo this choice: go to the {% internallink "workspace/preferences-window/miscellaneous" %} and click on the button *Reset warnings*.
{: .tip }

## Loading All Glyphs

When a font containing more glyphs than the *Max glyph count* limit is opened, a small button labeled *Load All Glyphs* will appear at the bottom left of the {% internallink 'workspace/font-overview' %} window:

{% image workspace/font-overview_load-all-glyphs.png %}

Click the *Load All Glyphs* button to force RoboFont to load data from all glyphs in the font.

## Simple font window

Another approach for dealing with fonts with a large amount of glyphs is using a *simplified font window*. Instead of the usual {% internallink 'workspace/font-overview' %} interface with glyph cells, we can use Python to create a simpler (and faster) font window in which the glyphs overview is a simple list of glyph names.

{% image how-tos/working-with-large-fonts_simple-font-window.png %}

Double-click a glyph name to open a {% internallink 'workspace/glyph-editor' %} for that glyph.

{% showcode how-tos/simpleFontWindow.py %}
