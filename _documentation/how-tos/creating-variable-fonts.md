---
layout: page
title: Creating variable fonts
tags:
  - variable fonts
  - interpolation
  - designspace
level: advanced
draft: true
---

* Table of Contents
{:toc}

## Planning the designspace

Before we start, it’s a good idea to make a sketch of the designspace we’re going to build:

- How many masters will it contain? Along how many axes?
- Where will the masters be positioned?
- Are there any named instances? Where?
- Will there be any special rules? (for example shape substitutions)

We’ll be using the [MutatorSans] family as an example. Its designspace contains:

- 4 masters along two axes, width and weight
- 1 glyph substitution rule
- 1 instance named *Medium*

{% image how-tos/creating-variable-fonts_MutatorSans-design-space.png %}

> Notice how the interpolation space does **not** include a Regular master, only extreme styles – this is a design decision. There are other ways to setup a weight/width designspace, give it a try!
{: .tip }

## Designing the masters

Having a blueprint of the designspace, we can now focus on designing the masters.

These are the 4 masters in MutatorSans, each one stored as a separate UFO font.

{% image how-tos/creating-variable-fonts_MutatorSans-masters.png %}

> It’s also possible to store all masters as layers of a single font. If you choose to work this way, keep in mind that these layers do not have their own kerning.
{: .note }

The usual rules and recommendations about drawing for interpolation apply:

- glyphs must be compatible across all masters (see [next section](#making-sure-all-masters-are-compatible))
- use overlapping paths to have more control over the shapes

{% image how-tos/creating-variable-fonts_MutatorSans-contours.png %}

> - [Drawing for interpolation](http://superpolator.com/drawing-2/)
{: .seealso }

It’s usually a good idea to draw the masters and build the designspace iteratively: draw, preview interpolations, adjust the drawings and/or parameters, preview again etc.

### Useful tools

[Delorean: Interpolation Preview][Delorean], [Interpolation Slider]
: preview interpolation result while drawing

[Overlay UFOs]
: view other masters in the background layer

[ShowSparks]
: view one-to-one relations between points in all open fonts

[EditThatNextMaster]
: easily switch between masters in glyph, space or font windows

> - extensions tagged with [interpolation][interpolation extensions] in Mechanic 2
{: .seealso }

[Delorean]: http://github.com/cjdunn/RoboFontExtensions/tree/master/Delorean/Delorean.roboFontExt
[Interpolation Slider]: http://github.com/andyclymer/InterpolationSlider-RoboFontExt
[Overlay UFOs]: http://github.com/FontBureau/fbOpenTools/tree/master/OverlayUFOs
[ShowSparks]: http://github.com/LettError/showSparks
[EditThatNextMaster]: http://github.com/LettError/editThatNextMaster
[interpolation extensions]: http://robofontmechanic.com/#interpolation

## Making sure all masters are compatible

Interpolation requires all master fonts to be *compatible*. This means that:

1. all masters must have the same glyphs as the neutral
2. in each glyph in all masters, the following must match:
   1. number of contours
   2. direction of each contour
   3. number of on-curve and off-curve points

> Older interpolation tools may interpolate a straight segment with a curve by assuming there are off-curve points on top of the on-curves. In variable fonts this is not possible, and it is a good design practice to have all the points in the masters.
{: .note }

> - {% internallink "preparing-for-interpolation" %}
{: .seealso }

### Useful tools

{% image how-tos/creating-variable-fonts_MutatorSans-Prepolator.png %}

[Prepolator]
: an extension to detect and fix compatibility problems between fonts

{% internallink 'how-tos/checking-interpolation-compatibility' %}
: some examples of how to check interpolation compatibility with code

## Creating the designspace

The `.designspace` file is where all information required to build a family of fonts or a variable font is stored.

- each axis is given a name and a range of values
- together, all axes define a *coordinate space*
- masters are inserted into specific locations of this coordinate space
- instances are defined as new locations with names

These are the axes and locations in the example MutatorSans designspace:

{% image how-tos/creating-variable-fonts_MutatorSans-design-space-2.png %}

### Useful tools

Different tools can be used to create `.designspace` files:

[Superpolator]
: a stand-alone application with interactive preview of instances and designspace

[DesignSpaceEditor]
: an open-source extension to edit raw designspace data

[DesignSpaceDocument]
: a Python library to read & write `.designspace` files with code

> - {% internallink "creating-designspace-files" %}
{: .seealso }

## Adding glyph substitution rules

Interpolation produces *continuous variation* between masters. Glyph substitution rules allow us to introduce *local discontinuous changes* to areas of the designspace, switching one set of glyph contours for another.

MutatorSans includes a glyph substitution rule to ‘fold’ the serifs of the `I` if the width parameter is below a certain value:

{% image how-tos/creating-glyph-substitution-rules_substitution-axis.png %}

{% image how-tos/mutatorSans.gif %}

> - {% internallink 'creating-glyph-substitution-rules' %}
{: .seealso }

## Generating variable fonts

The recommended way of building variable fonts in RoboFont is using the [Batch] extension. Besides generating static fonts, Batch can also generate a variable font from a `.designspace` file (and a set of compatible UFO sources).

The Batch extension is open-source and can be {% internallink 'extensions/installing-extensions-mechanic' text='installed with Mechanic 2' %}.

> The requirements for building variable fonts are stricter than those for building static fonts. Batch does a lot of work behind the scenes to fix compatibility issues between the masters. See the [Batch documentation][Batch] for more details.
{: .note}

### Generating variable fonts with the Batch extension

Use the menu *File > Batch* to open the Batch window:

{% image how-tos/creating-variable-fonts_Batch.png %}

1. drag the `.designspace` file into the list (or use the *Open* button)
2. open the section *Variable Fonts*, and select the desired options
3. click on the *Generate* button to generate the variable font

### Generating variable fonts with code

The variable font generator included inside the [Batch] extension can also be used programmatically: you can import it into your scripts, and use it to generate variable fonts from `.designspace` files – without opening the Batch window.

> The `lib` folder of every installed extension is added to the `sys.path` – so code from one extension can be accessed by another extension or by a script.
{: .tip }

The example script below generates a variable font from the [MutatorSans] `.designspace` file and linked UFO sources.

{% showcode how-tos/generateVariableFont.py %}

## Testing variable fonts

### Testing with DrawBot

Variable fonts are supported natively in macOS 10.3 and higher.

DrawBot supports variable fonts too. The following commands are available:

[`fontVariations(wdth=0.6, wght=0.1, ...)`](http://www.drawbot.com/content/text/textProperties.html#drawBot.fontVariations)
: selects a location in the current variable font using parameters

[`listFontVariations(fontName=None)`](http://www.drawbot.com/content/text/textProperties.html#drawBot.listFontVariations)
: returns a dictionary with variations data for the current font

The MutatorSans repository includes two DrawBot scripts for testing and previewing variable fonts:

{% image how-tos/creating-variable-fonts_testing-DrawBot.png %}

[previewMutatorSans.py](http://github.com/LettError/mutatorSans/blob/master/drawbot/previewMutatorSans.py)
: preview the variable font interactively using sliders

[animateMutatorSans.py](http://github.com/LettError/mutatorSans/blob/master/drawbot/animateMutatorSans.py)
: animate font variations along the width and weight axes

> - [Tutorial: How to animate a variable font](http://forum.drawbot.com/topic/50/tutorial-request-how-to-animate-a-variable-font)
{: .seealso }

### Testing in the browser

Variable fonts are supported in all major browsers.

Use the CSS property `font-variation-settings` to select an instance in the font’s design space using parameters:

```css
.example { font-variation-settings: "wght" 500, "wdth" 327; }
```

The MutatorSans repository includes a simple [HTML test page] which shows how to:

- animate font variation parameters with CSS
- control parameters interactively using sliders

{% image how-tos/creating-variable-fonts_testing-html.png %}

[HTML test page]: http://github.com/LettError/mutatorSans/blob/master/test.html

[Batch]: http://github.com/typemytype/batchRoboFontExtension
[DesignSpaceDocument]: http://github.com/LettError/designSpaceDocument/blob/master/scripting.md
[DesignSpaceEditor]: http://github.com/LettError/designSpaceRoboFontExtension
[MutatorMath]: http://github.com/LettError/mutatorMath
[MutatorSans]: http://github.com/LettError/mutatorSans
[Prepolator]: http://extensionstore.robofont.com/extensions/prepolator/
[Superpolator]: http://superpolator.com/
[cu2qu]: http://github.com/googlei18n/cu2qu

> - add new section: Defining static instances
{: .todo }
