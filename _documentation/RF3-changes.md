---
layout: page
title: Changes from RoboFont 1.8 to 3
tags:
  - UFO
  - RF3
  - py3
level: intermediate
---

* Table of Contents
{:toc}

*RoboFont 3 introduces several changes in relation to RoboFont 1.8.*

*This page offers some background and gives an overview of the main changes in RF3.*

## Background

When RoboFont was released in 2011, it was the first font editor to have UFO as its native source format. RoboFont 1 implemented version 2 of the UFO font format, with a few additional features such as support for layers, images, guidelines etc.

Since then, there have been several important changes in type technology:

### Extensions to the OpenType format

OpenType was extended to allow colors ([SVG], [COLR] and [sbix] tables) and [variable fonts]. These extensions created new requirements for tools and workflows, which in turn increased the need for a better support for layers in font editors and source formats.

[SVG]: http://microsoft.com/typography/otspec/svg.htm
[COLR]: http://microsoft.com/typography/otspec/colr.htm
[sbix]: http://microsoft.com/typography/otspec/sbix.htm
[variable fonts]: http://medium.com/@tiro/https-medium-com-tiro-introducing-opentype-variable-fonts-12ba6cd2369

### UFO3

The [UFO3] specification was published and gradually refined based on feedback from toolmakers. Among the main changes in relation to [UFO2], it includes support for layers, unique point identifiers, kerning groups, images, guidelines and binary data.

> - [Tal Leming introducing UFO3 (Robothon 2012)](http://vimeo.com/38328029)
> - {% internallink "introduction/the-ufo-format#ufo3" %}
{: .seealso }

[UFO3]: http://unifiedfontobject.org/versions/ufo3/
[UFO2]: http://unifiedfontobject.org/versions/ufo2/

### FontParts

With [RoboFab] showing signs of age, the [FontParts] library was developed as its successor. FontParts is not an update of RoboFab – it was written from scratch to replace it, so it’s free from old legacy code which was no longer needed. It also adds support to the UFO3 format, and introduces minor API changes (improvements) in relation to RoboFab.

[RoboFab]: http://github.com/robotools/robofab
[FontParts]: http://github.com/robotools/fontparts

### Python 3

In parallel to changes in font technology, there were also important developments in the Python world. Python 3 was released in 2008. The final 2.x release, version 2.7, came out in mid-2010 as an end-of-life release. New development moved to Python 3.

The language was properly cleaned up, with less regard for backwards compatibility. The most drastic improvement is better Unicode support, with all text strings being Unicode by default. Several aspects of the core language have been adjusted to be easier for newcomers to learn, and to be more consistent with the rest of the language. And old cruft has been removed.

> - [What’s New In Python 3.0](http://docs.python.org/3/whatsnew/3.0.html)
{: .seealso }

## Main changes in RoboFont 3

### UFO3 as native source format

RoboFont’s source format is upgraded from UFO2 to UFO3. Doing so required internal rewriting of the program, because of the differences between the layer model of UFO3 and the previous layer model in RoboFont.

Other custom additions to the UFO2 format – images, unique point identifiers, guidelines – have now also found their place in UFO3. Moving these kinds of data from custom libs to default attributes makes RoboFont UFO3-compliant, and improves interoperability with other tools.

### New implementation of layers

The new layer model in RoboFont 3 is more flexible and more powerful than the previous one. Layer glyphs can now have their own widths, instead of being forced to inherit the width of the foreground layer. This opens new possibilities for the use of layers – for example, saving different masters as layers of the same font, or different width alternates as layers of the same glyph.

> The new layers model affects the way components work:
>
> - In RoboFont 3, components always refer to glyphs in the *same layer*.
> - In RoboFont 1, components in any layer refer to glyphs in the *foreground* layer.
{: .note }

### Written in Python 3

Following the steps of DrawBot, RoboFont 3 was upgraded from Python 2.7 to Python 3.6. This was a big change, which involved making sure that all underlying libraries were also converted to Python 3. It was worth the effort: RoboFont 3 can now benefit from all the good things in Python 3, and is ready for the future.

> Yes, some of your RoboFont 1.8 (Python 2) scripts will need to updated to Python 3. But it is an easy process, and we provide information and tools to make your transition easier.
{: .warning }

### Switch to FontParts API

RoboFont 3 implements the FontParts API. Your RoboFont 1.8 scripts will still work, but RoboFont 3 will issue a deprecation warning when the old API is used.

**Developers are advised to upgrade their code to the FontParts API and Python 3.**

We provide recommendations, tools and extensive documentation of the API changes.

> - {% internallink "building-tools/toolkit/robofab-fontparts" %}
> - {% internallink "recommendations-upgrading-RF3" %}
> - [oneToThreeRoboFontExtension](http://github.com/typemytype/oneToThreeRoboFontExtension)
{: .seealso}

### Speed improvements

As the result of the internal rewriting, Python 3, and other code optimizations, RoboFont 3 is the fastest RoboFont ever!

### Updates to the Preferences

The {% internallink 'workspace/preferences-window' %} includes a new {% internallink 'workspace/preferences-window/menus' text="Menus" %} section which allows users to modify or assign new keyboard shortcuts to any menu item.

A new {% internallink 'workspace/preferences-editor' %} was also added, giving access to *all* user Preferences in editable JSON format – including many settings which are not available in the Preferences window.

### Updates to the Font Info panel

The {% internallink "workspace/font-info-sheet" %} includes new fields for font info attributes which are new to UFO3:

- {% internallink "workspace/font-info-sheet/woff" text='WOFF metadata' %}
- {% internallink "workspace/font-info-sheet/opentype#name-records" text='name table localization' %}

### New website & documentation

The release of RoboFont 3 is joined by a brand new documentation and website, new forum, and the new Extension Store.

Documentation
: The documentation has been restructured, rewritten and expanded.

  Some highlights of the new version:

  - new superchapter dedicated to [Building Tools](../building-tools/)
  - new chapter about [Building Extensions](../building-tools/extensions/), including a demo {% internallink "building-tools/extensions/boilerplate-extension" text="Boilerplate Extension" %}
  - new <a href="../how-tos">How To’s</a> chapter with step-by-step guides on common type design and font production tasks in RoboFont

Website
: The new website has an updated look & feel and new custom fonts (RoboType). The new infra-structure offers unified Search and {% internallink 'tags' %}, making it easier to find content about specific subjects. The [forum] was also moved to a new engine.

Extension Store
: The [Extension Store] is a place where RoboFont users can get commercial RoboFont extensions. It aims to stimulate extension development by offering developers a way to get compensated for their work.

[forum]: http://forum.robofont.com/
[Extension Store]: http://extensionstore.robofont.com/

{% comment %}
note for later: these links are not working correctly:
{% internallink "documentation/how-tos" %}
{% internallink "building-tools" %}
{% internallink "building-tools/extensions" %}
{% endcomment %}
