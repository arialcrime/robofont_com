---
layout: page
title: How-To’s
excludeSearch: true
excludePrevNext: false
level: beginner
tree:
  - "*"
treeCanHide: true
---

**How to accomplish specific tasks with RoboFont.**

> *This chapter is under continuous development.*<br/>
> Would you like to help out? See articles with the tag [documentation](#documentation).<br/>
> For questions, comments, suggestions, etc. please use the [Forum](http://forum.robofont.com/).
{: .note }

<div class="sorting-controls">
  <label>
    <span class="reset">Reset</span>
  </label>
</div>

{% include globItems %}

<div class="sorter-checkboxes">
    {% assign tags = "" | split:"|" %}
    {% for item in globItems %}
        {% for tag in item.tags %}
            {% assign tags = tags | push: tag %}
        {% endfor %}
    {% endfor %}
    {% assign tags = tags | uniq | sort %}
    {% for tag in tags %}
        <label class="sorter-checkbox filter">
            <input type="checkbox" id="{{ tag | slugify }}" value="{{ tag | slugify }}"><span style="border: 1px solid hsl({{ forloop.index | times: 300 | divided_by: forloop.length }}, 100%, 50%); background-color:hsla({{ forloop.index | times: 300 | divided_by: forloop.length }}, 100%, 50%, 0.2);">{{ tag }}</span>
        </label>
    {% endfor %}
</div>
<div class="sorter">
{% for item in globItems %}
{% assign slugifiedTags = "" | split:"|" %}
{% for tag in item.tags %}
    {% assign slugifiedTag = tag | slugify %}
    {% assign slugifiedTags = slugifiedTags | push: slugifiedTag %}
{% endfor %}
<div class="sorter-item" data-title="{{ item.title }}" data-groups='["{{ slugifiedTags | join: '", "' }}"]'>
    <div class="sorter-item-color{% unless item.tags %} item-no-tags{% endunless %}">
    {% for tag in tags %}
        {% if item.tags contains tag %}
            <div style="background-color:hsla({{ forloop.index | times: 300 | divided_by: forloop.length }}, 100%, 50%, 0.2);"></div>
        {% endif %}
    {% endfor %}
    </div>
    <div class="sorter-item-inner">
        <div class="sorter-item-title">
            <a href="{{ site.baseurl }}{{ item.url }}">{{ item.title }}</a>
        </div>
        <div class="sorter-item-single-info">
            <div class="sorter-item-tags">
            {% for tag in item.tags %}
                <div class="tag">{{ tag }}</div>
            {% endfor %}
            </div>
        </div>
    </div>
</div>
{% endfor %}
</div>

<script src="{{ site.baseurl }}/js/shuffle.min.js"></script>
<script src="{{ site.baseurl }}/js/filter-helpers.js"></script>
