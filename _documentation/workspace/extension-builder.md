---
layout: page
title: Extension Builder
tags:
  - extensions
level: beginner
---

The Extension Builder is an interface for building extensions and editing extension metadata. It can be opened from the menu *Python > Extension Builder*.

{% image workspace/extension-builder.png %}

> - {% internallink "extensions/building-extensions-with-extension-builder" %}
{: .seealso }

## Properties

<table>
  <thead>
    <tr>
      <th width="35%">title</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Plugin Name</td>
      <td>The name of the extension</td>
    </tr>
    <tr>
      <td>Plugin Icon</td>
      <td>An icon for the extension. <em>(optional)</em></td>
    </tr>
    <tr>
      <td>Version</td>
      <td>The extension’s version number.</td>
    </tr>
    <tr>
      <td>Developer</td>
      <td>Name of the developer.</td>
    </tr>
    <tr>
      <td>Developer’s URL</td>
      <td>Site URL of the developer.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>HTML help root</td>
      <td>Root folder where the HTML help can be found. An <code>index.html</code> file is required.</td>
    </tr>
    <tr>
      <td>Resources root</td>
      <td>Root folder where all the resources can be found. Resources are all necessary files except Python files.</td>
    </tr>
    <tr>
      <td>Script Root</td>
      <td>
        <p>Root folder where all the necessary Python files can be found.</p>
        <dl>
          <dt>Launch during start up</dt>
          <dd>A <code>bool</code> indicating that a script should be executed during RoboFont start up. Use the list to select the script.</dd>
          <dt>Add Script to main menu</dt>
          <dd>Select which scripts should be added to the Extensions menu item. Additionally, a name and shortcut can be set for each Python file in the script root.</dd>
        </dl>
      </td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Requires RoboFont</td>
      <td>The minimum required version of RoboFont.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>License</td>
      <td>The full license text for the extension.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Requirements</td>
      <td>A list of extensions which are required for the current extension to work.</td>
    </tr>
    <tr>
      <td>Expiration date</td>
      <td>The extension’s expiration date. <em>new in RoboFont 3.3b</em></td>
    </tr>
  </tbody>
</table>

## Actions

<table>
  <tr>
    <th width="35%">title</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>Build</td>
    <td>Build the extension in a given folder.</td>
  </tr>
  <tr>
    <td>Import</td>
    <td>Import data from an existing extension.</td>
  </tr>
  <tr>
    <td>Clear</td>
    <td>Empty all fields in the Extension Builder.</td>
  </tr>
</table>
