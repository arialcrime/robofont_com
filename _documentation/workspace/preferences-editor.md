---
layout: page
title: Preferences Editor
level: beginner
tags:
  - preferences
---

The Preferences Editor is a special window for editing raw Preferences settings in [json] format. It can be opened from the *RoboFont > Preferences Editor* menu while pressing ⌥, or using the shortcut keys ⌥ ⌘ , (Alt + Command + comma).

{% image workspace/preferences-editor.png %}

The Prefences Editor gives access to more settings than available via the {% internallink "preferences-window" %}.

[json]: http://en.wikipedia.org/wiki/JSON

Use the editor to modify the values. When you close the window, you will be asked to save or discard the changes.
