---
layout: page
title: Sort Glyphs sheet
tags:
  - character set
  - glyph names
level: beginner
---

* Table of Contents
{:toc}

The *Sort Glyphs* sheet offers different methods to sort the glyphs in a font.

To open the sheet, choose *Font > Sort* from the {% internallink "workspace/application-menu" %} or click on the *Sort* icon in the {% internallink "workspace/font-overview" %} toolbar.

{% image workspace/sort-glyphs-sheet.png %}

When the *OK* button is pressed, the selected sorting method is applied to the font’s glyph order, which is stored in the font.

> To define the default sorting order for new fonts, use the {% internallink "preferences-window/sort" %}.
{: .note }

## Glyph sorting methods

### Smart Sort

The *Smart Sort* method uses a default sorting algorithm which combines matching rules with some preset values.

### Character Set

The *Character Set* method allows you to choose one of the character sets saved in the {% internallink 'preferences-window/character-set' text='Preferences' %} to sort the font.

{% image workspace/sort-glyphs-sheet_character-set.png %}

### Custom

The *Custom…* option allows you to create your own sorting method by combining different *sort descriptors*.

{% image workspace/sort-glyphs-sheet_custom.png %}

The table below provides more information about each sort descriptor:

<table>
  <tr>
    <th width="30%">sorting parameters</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>alphabetical</td>
    <td>Sort alphabetically.</td>
  </tr>
  <tr>
    <td>unicode</td>
    <td>Sort based on Unicode value.</td>
  </tr>
  <tr>
    <td>script</td>
    <td>Sort based on Unicode script.</td>
  </tr>
  <tr>
    <td>category</td>
    <td>Sort based on Unicode category.</td>
  </tr>
  <tr>
    <td>block</td>
    <td>Sort based on Unicode block.</td>
  </tr>
  <tr>
    <td>suffix</td>
    <td>Sort based on glyph name suffix.</td>
  </tr>
  <tr>
    <td>decompositionBase</td>
    <td>Sort based on the base glyph defined in the decomposition rules.</td>
  </tr>
  <tr>
    <td>weightedSuffix</td>
    <td>
      <p>Sort based on glyph names suffix.</p>
      <p>The ordering of the suffixes is based on the calculated “weight” of the suffix, which is calculated by noting what type of glyphs have the same suffix. The more glyph types, the more important the suffix.</p>
      <p>Additionally, glyphs with suffixes that have only numerical differences are grouped together. For example, <code>a.alt</code>, <code>a.alt1</code> and <code>a.alt319</code> will be grouped together.</p>
    </td>
  </tr>
  <tr>
    <td>ligature</td>
    <td>Sort into to groups: non-ligatures and ligatures. The determination of whether a glyph is a ligature or not is based on the Unicode value, common glyph names or the use of an underscore in the name.</td>
  </tr>
</table>

The following options are available for each individual sort descriptor:

Ascending
: Boolean representing if the glyphs should be in ascending or descending order.

Allow pseudo unicode
: Boolean representing if pseudo- or real Unicode values are used.

> - [defcon > UnicodeData > sortGlyphNames](http://ts-defcon.readthedocs.io/en/latest/objects/unicodedata.html#defcon.UnicodeData.sortGlyphNames)
{: .seealso }

- - -

> - [Sorting glyphs with a script](../../how-tos/sorting-glyphs#sorting-glyphs-with-a-script)
{: .seealso }
