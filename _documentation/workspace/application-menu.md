---
layout: page
title: Application Menu
tags:
  - UI
level: beginner
---

* Table of Contents
{:toc}

The main application menu shows items which apply to the app as a whole:

{% image workspace/application-menu.png %}

Each menu item reveals a submenu with options (see below).

> In RoboFont 1.8.4+, the version number is appended to the app name in the main application menu – this makes it easer to differentiate the two versions of RoboFont when they are both open at the same time.
{: .note }

> {% internallink "keyboard-shortcuts" %}
> : Several menu items have a corresponding keyboard shortcut.
>
> {% internallink "how-tos/adding-custom-items-to-application-menu" %}
> : Additional menu items can be added by extensions and custom tools.
{: .seealso }

## RoboFont

{% image workspace/application-menu_robofont.png %}

<table>
  <thead>
    <tr>
      <th width="8%">key</th>
      <th width="30%">item</th>
      <th width="62%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td>About RoboFont</td>
      <td>Open the <a href="../about-window">About Window</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Preferences…</td>
      <td>Open the <a href="../preferences-window">Preferences Window</a>.</td>
    </tr>
    <tr>
      <td>⌥</td>
      <td>Preferences Editor…</td>
      <td>Open the <a href="../preferences-editor">Preferences Editor</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>License…</td>
      <td>Open the <a href="../license-window">License Window</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Check for Updates…</td>
      <td>Check if a more recent version of RoboFont is available.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Services ▸</td>
      <td>Standard macOS menu to let users access functionality from one app in another.<br/>see <a href="http://developer.apple.com/macos/human-interface-guidelines/extensions/services/">Services</a></td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Hide RoboFont</td>
      <td>Hide RoboFont and all its windows, and activate the most recently used app.</td>
    </tr>
    <tr>
      <td></td>
      <td>Hide Other</td>
      <td>Hide all other open apps and their windows.</td>
    </tr>
    <tr>
      <td></td>
      <td>Show All</td>
      <td>Show all other open apps and their windows behind RoboFont’s windows.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Quit RoboFont</td>
      <td>Quit the application.</td>
    </tr>
    <tr>
      <td>⌥</td>
      <td>Quit and Keep Windows</td>
      <td>Quit the application. The windows that were open will be restored the next time you open RoboFont.</td>
    </tr>
  </tbody>
</table>

## File

{% image workspace/application-menu_file.png %}

<table>
  <thead>
    <tr>
      <th width="8%">key</th>
      <th width="30%">item</th>
      <th width="62%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td>New</td>
      <td>Create a new font.</td>
    </tr>
    <tr>
      <td></td>
      <td>Open…</td>
      <td>Open an existing document (font or script).</td>
    </tr>
    <tr>
      <td></td>
      <td>Open Recent</td>
      <td>Open a recently edited document. The submenu shows a list of recently opened files.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Save</td>
      <td>Save the document.</td>
    </tr>
    <tr>
      <td></td>
      <td>Save As…</td>
      <td>Save the document under another name.</td>
    </tr>
    <tr>
      <td></td>
      <td>Save All</td>
      <td>Save all open fonts.</td>
    </tr>
    <tr>
      <td></td>
      <td>Close</td>
      <td>Close the current document.</td>
    </tr>
    <tr>
      <td>⌥</td>
      <td>Close All</td>
      <td>Close all open documents.</td>
    </tr>
    <tr>
      <td></td>
      <td>Revert to Saved</td>
      <td>Revert font to its last saved version.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Generate Font</td>
      <td>Generate font binary for current font.</td>
    </tr>
    <tr>
      <td></td>
      <td>Test Install</td>
      <td>Install a test version of the current font.<br/>see <a href="../../how-tos/using-test-install">Using Test Install</a></td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td colspan="2"><strong>Features</strong> ▸</td>
    </tr>
    <tr>
      <td></td>
      <td>New Feature</td>
      <td>Create a stand-alone features document.</td>
    </tr>
    <tr>
      <td></td>
      <td>Export Feature</td>
      <td>Export the font’s features to an external <code>.fea</code> file.</td>
    </tr>
  </tbody>
</table>

## Edit

{% image workspace/application-menu_edit.png %}

<table>
  <thead>
    <tr>
      <th width="8%">key</th>
      <th width="30%">item</th>
      <th width="62%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td>Undo</td>
      <td>Undo the last action.</td>
    </tr>
    <tr>
      <td></td>
      <td>Redo</td>
      <td>Redo the last action.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Cut</td>
      <td>Cut the current selection to the clipboard.</td>
    </tr>
    <tr>
      <td></td>
      <td>Copy</td>
      <td>Copy the current selection to the clipboard.</td>
    </tr>
    <tr>
      <td></td>
      <td>Copy As Component</td>
      <td>Copy the selected glyph to the clipboard component.</td>
    </tr>
    <tr>
      <td></td>
      <td>Paste</td>
      <td>Paste the clipboard contents into the current document.</td>
    </tr>
    <tr>
      <td></td>
      <td>Paste and Match Style</td>
      <td>Paste the clipboard contents into the current document without formatting.</td>
    </tr>
    <tr>
      <td></td>
      <td>Delete</td>
      <td>Delete the current selection.</td>
    </tr>
    <tr>
      <td></td>
      <td>Select All Contours</td>
      <td>Select all contours in glyph.</td>
    </tr>
    <tr>
      <td>⌥</td>
      <td>Select All</td>
      <td>Select all objects (contours, anchors, components) in glyph.</td>
    </tr>
    <tr>
      <td></td>
      <td>Deselect</td>
      <td>Deselect current selection.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Transform Again</td>
      <td>Apply the last transformation again.</td>
    </tr>
  </tbody>
</table>

> The remaining menu items (*Find*, *Spelling*, *Speech* etc) are standard Edit menu items available in all macOS applications.
>
> see [Edit Menu (macOS Human Interface Guidelines)](http://developer.apple.com/macos/human-interface-guidelines/menus/menu-bar-menus/#edit-menu)
{: .seealso }

## Font

{% image workspace/application-menu_font.png %}

<table>
  <thead>
    <tr>
      <th width="8%">key</th>
      <th width="30%">item</th>
      <th width="62%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td>Add Glyphs</td>
      <td>Show the Add Glyphs sheet. See <a href="../../how-tos/adding-and-removing-glyphs">Adding and deleting glyphs</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Font Info</td>
      <td>Show the <a href="../font-info-sheet">Font Info sheet</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Sort</td>
      <td>Show the <a href="../sort-glyphs-sheet">Sort Glyphs sheet</a>.</td>
    </tr>
  </tbody>
</table>

## Glyph

{% image workspace/application-menu_glyph.png %}

<table>
  <thead>
    <tr>
      <th width="8%">key</th>
      <th width="30%">item</th>
      <th width="62%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td>Next Glyph</td>
      <td>Display the next glyph in the selected set.</td>
    </tr>
    <tr>
      <td></td>
      <td>Previous Glyph</td>
      <td>Display the previous glyph in the selected set.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Editing</td>
      <td>Selects the <a href="../glyph-editor/tools/editing-tool">Editing Tool</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Drawing</td>
      <td>Selects the <a href="../glyph-editor/tools/bezier-tool">Bezier Tool</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Slice</td>
      <td>Selects the <a href="../glyph-editor/tools/slice-tool">Slice Tool</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Transform</td>
      <td>
        <p>Set the current selection in Transform mode.</p>
        <ul>
          <li>if there is no selection, the whole glyph will be in in Transform mode</li>
          <li>if a tool does not support transformations, it will not be usable</li>
        </ul>
      </td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Lock Guides</td>
      <td>Locks the <a href="../glyph-editor/glyph-data/guidelines">Guidelines</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Lock Side Bearings</td>
      <td>Locks the left and right margins.</td>
    </tr>
    <tr>
      <td></td>
      <td>Lock Images</td>
      <td>Locks the <a href="../glyph-editor/glyph-data/images">Images</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td colspan="2"><strong>Layers</strong> ▸</td>
    </tr>
    <tr>
      <td></td>
      <td>Layer Up</td>
      <td>Jump to next <a href="../glyph-editor/glyph-data/layer">Layer</a> (up).</td>
    </tr>
    <tr>
      <td></td>
      <td>Layer Down</td>
      <td>Jump to previous <a href="../glyph-editor/glyph-data/layer">Layer</a> (down).</td>
    </tr>
    <tr>
      <td></td>
      <td>New Layer</td>
      <td>Create a new <a href="../glyph-editor/glyph-data/layer">Layer</a>.</td>
    </tr>
  </tbody>
</table>

## Python

{% image workspace/application-menu_python.png %}

<table>
  <thead>
    <tr>
      <th width="8%">key</th>
      <th width="30%">item</th>
      <th width="62%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td>Scripting Window</td>
      <td>Open a <a href="../scripting-window">Scripting Window</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Output Window</td>
      <td>Open the <a href="../output-window">Output Window</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Run</td>
      <td>Execute script in the <a href="../scripting-window">Scripting Window</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Indent</td>
      <td>Indent selected code.</td>
    </tr>
    <tr>
      <td></td>
      <td>Dedent</td>
      <td>Dedent selected code.</td>
    </tr>
    <tr>
      <td></td>
      <td>Comment</td>
      <td>Comment selected code.</td>
    </tr>
    <tr>
      <td></td>
      <td>Uncomment</td>
      <td>Uncomment selected code.</td>
    </tr>
    <tr>
      <td></td>
      <td>Word wrap</td>
      <td>Wrap words (reflow text in window).</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Jump to Line</td>
      <td>Jump to a given line number.</td>
    </tr>
    <tr>
      <td>⌥</td>
      <td>Jump to Definition</td>
      <td>Jump to definition match.</td>
    </tr>
    <tr>
      <td>⇧</td>
      <td>Jump Back</td>
      <td>Jump back to previous definition match.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Extension Builder</td>
      <td>Open the <a href="../extension-builder">Extension Builder</a>.</td>
    </tr>
  </tbody>
</table>

## Extensions

The menu will show a list of tools installed by {% internallink "documentation/extensions" %}.

> Not all extensions create an entry in the Extensions menu. For example, some extensions only add a tool to the Glyph Editor.
{: .note }

## Scripts

The menu will show a list of all `.py` files in the default script folder.

> - {% internallink "preferences-window/python" %}
{: .seealso }

> The Scripts menu is available in RoboFont 3 only. In RoboFont 1, scripts are listed at the bottom of the Extensions menu.
{: .note }

## Window

{% image workspace/application-menu_window.png %}

<table>
  <thead>
    <tr>
      <th width="8%">key</th>
      <th width="30%">item</th>
      <th width="62%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td></td>
      <td>Inspector</td>
      <td>Show/hide the <a href="../inspector">Inspector panel</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Show Previous Tab</td>
      <td>Show the previous tab in a tab-based window.</td>
    </tr>
    <tr>
      <td></td>
      <td>Show Next Tab</td>
      <td>Show the next tab in a tab-based window.</td>
    </tr>
    <tr>
      <td></td>
      <td>Move Tab to New Window</td>
      <td>Open the current tab in a new window.</td>
    </tr>
    <tr>
      <td></td>
      <td>Merge All Windows</td>
      <td>Combine all open windows in a single tabbed window.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Space Center</td>
      <td>Open the <a href="../space-center">Space Center</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>New Space Center</td>
      <td>Open an extra <a href="../space-center">Space Center</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td colspan="2"><strong>View</strong> ▸</td>
    </tr>
    <tr>
      <td></td>
      <td>Toggle Smart Find</td>
      <td>Show/hide the Find bar. See <a href="../../how-tos/searching-glyphs">Searching Glyphs</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Toggle Smart Sets</td>
      <td>Show/hide <a href="../smart-sets">Smart Sets</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Toggle Font Overview</td>
      <td>Show/hide <a href="../font-overview">Font Overview</a>. Available only in <a href="../../window-modes">Single Window mode</a>.</td>
    </tr>
    <tr>
      <td></td>
      <td>Toggle Path Browser</td>
      <td>Show/hide path browser for a <a href="../scripting-window">Scripting Window</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Minimize</td>
      <td>Minimize the active window to the Dock.</td>
    </tr>
    <tr>
      <td>⌥</td>
      <td>Minimize All</td>
      <td>Minimize all open windows to the Dock.</td>
    </tr>
    <tr>
      <td></td>
      <td>Enter Full Screen</td>
      <td>
        <p>Opens the window at full-screen size in a new space.</p>
        <p>see <a href="http://developer.apple.com/macos/human-interface-guidelines/app-architecture/fullscreen-mode/">Full-Screen Mode</a></p>
      </td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Zoom In</td>
      <td>Zoom in.</td>
    </tr>
    <tr>
      <td></td>
      <td>Zoom Out</td>
      <td>Zoom out.</td>
    </tr>
    <tr>
      <td></td>
      <td>Zoom To Fit</td>
      <td>Zoom the current glyph to fit the Glyph View. Available only in the <a href="../glyph-editor">Glyph Editor</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td></td>
      <td>Bring All to Front</td>
      <td>Bring all of the app’s open windows to the front, maintaining their onscreen location, size, and layering order.</td>
    </tr>
    <tr>
      <td>⌥</td>
      <td>Arrange in Front</td>
      <td>Bring all of the app’s open windows to the front, and tile them neatly.</td>
    </tr>
  </tbody>
</table>

> - [Menu Bar Menus > Window Menu (macOS Human Interface Guidelines)](http://developer.apple.com/macos/human-interface-guidelines/menus/menu-bar-menus/#window-menu)
{: .seealso }
