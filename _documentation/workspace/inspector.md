---
layout: page
title: Inspector
level: beginner
---

* Table of Contents
{:toc}

The Inpector panel is a floating window with several sections, each one giving access to a particular kind of data in the current glyph.

To open the Inspector, choose *Windows > Inspector* from the main application menu, or use the keyboard shortcut ⌘ + I.

{% image workspace/inspector.png %}

Each section of the Inspector can be collapsed/expanded by clicking on its title. Some sections can be resized using the division bar at the bottom.

> - {% internallink "building-tools/toolspace/mojo/accordion-window" text='Toolspace > mojo > Accordion View' %}
{: .seealso }

## Glyph

The *Glyph* section allows you to edit several basic attributes of the current glyph.

{% image workspace/inspector_glyph.png %}

<table>
  <tr>
    <th width="20%">option</th>
    <th width="80%">description</th>
  </tr>
  <tr>
    <td>Name</td>
    <td>
      <p>The name of the current glyph.</p>
      <p>If the name is changed, a pop-up sheet will appear to assist with renaming the glyph in referenced components, groups and kerning, as well as setting unicodes automatically.</p>
    </td>
  </tr>
  <tr>
    <td>Unicode</td>
    <td>The unicodes of the current glyph. Similarly to the glyph name, a pop-up sheet will appear to assist in setting a new unicode value.</td>
  </tr>
  <tr>
    <td>Width</td>
    <td>Set the width of the current glyph.</td>
  </tr>
  <tr>
    <td>Left</td>
    <td>Set the left margin of the current glyph.</td>
  </tr>
  <tr>
    <td>Right</td>
    <td>Set the right margin of the current glyph.</td>
  </tr>
  <tr>
    <td>Mark</td>
    <td>Set the mark color of the current glyph.</td>
  </tr>
</table>

> - {% internallink 'how-tos/adding-and-removing-glyphs' %}
> - {% internallink 'how-tos/using-production-names' %}
> - {% internallink 'how-tos/sorting-glyphs' %}
{: .seealso }

## Preview

The *Preview* section shows a filled in live preview of the current glyph.

{% image workspace/inspector_preview.png %}

## Layers

The Layers section offers an interface for creating and managing layers.

{% image workspace/inspector_layers.png %}

<table>
  <tr>
    <th width="25%">action</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>drag & drop</td>
    <td>Change layer order.</td>
  </tr>
  <tr>
    <td>click and hold</td>
    <td>Rename a layer.</td>
  </tr>
  <tr>
    <td>+ / -</td>
    <td>Add or remove a layer.</td>
  </tr>
  <tr>
    <td>display controls</td>
    <td>
      Control how the layer is displayed in the Glyph View.
      <dl>
        <dt>If the selected layer is the active layer:</dt>
        <dd>display controls affect the display of the active layer.</dd>
        <dt>If the selected layer is not the active layer:</dt>
        <dd>display controls affect the display of the layers in the background.</dd>
      </dl>
    </td>
  </tr>
</table>

> - {% internallink "glyph-editor/layers" text='Glyph Editor > Layers' %}
> - {% internallink "building-tools/api/fontParts/rlayer" text='FontParts > RLayer' %}
{: .seealso }

## Transform

The *Transform* section offers several tools to apply transformations to one or more glyphs.

{% image workspace/inspector_transform.png %}

### Alignment options

Use the icons at the top to define an origin point for the transformations, and to flip and align shapes.

<table>
  <tr>
    <th width="20%">option</th>
    <th width="80%">description</th>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_origin.png" /></td>
    <td>Select the origin point for the transformation. If no point is selected <code>(0,0)</code> will be used as origin.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_mirror-x.png" /></td>
    <td>Mirror horizontally.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_mirror-y.png" /></td>
    <td>Mirror vertically.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_align-bottom.png" /></td>
    <td>Align selected objects to the bottom.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_align-middle.png" /></td>
    <td>Center selected objects vertically.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_align-top.png" /></td>
    <td>Align selected objects to the top.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_align-left.png" /></td>
    <td>Align selected objects to the left.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_align-center.png" /></td>
    <td>Center selected objects horizontally.</td>
  </tr>
  <tr>
    <td><img src="../../../images/workspace/inspector_transform_align-right.png" /></td>
    <td>Align selected objects to the right.</td>
  </tr>
</table>

### Transformations

Set the individual transformation settings to define a transformation matrix.

<table>
  <tr>
    <th width="20%">option</th>
    <th width="80%">description</th>
  </tr>
  <tr>
    <td>Move</td>
    <td>Move selected objects by <code>(x,y)</code> values. Optionally equalize <code>x</code> and <code>y</code>.</td>
  </tr>
  <tr>
    <td>Scale</td>
    <td>Scale selected objects according to <code>(x,y)</code> percentage values. Optionally equalize <code>x</code> and <code>y</code>.</td>
  </tr>
  <tr>
    <td>Rotate</td>
    <td>Rotate selected objects by an angle (in degrees).</td>
  </tr>
  <tr>
    <td>Skew</td>
    <td>Skew selected objects by angles <code>α,β</code>. Optionally equalize <code>α</code> and <code>β</code>.</td>
  </tr>
  <tr>
    <td>Snap</td>
    <td>Snap selected objects to a grid.</td>
  </tr>
</table>

### Transform options

The *Transform* button opens a popu menu with options to control the scope of the transformation within the font.

{% image workspace/inspector_transform_apply.png %}

<table>
  <thead>
    <tr>
      <th width="45%">option</th>
      <th width="55%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Transform</td>
      <td>Apply all transformations to the current glyph. The transformation could be a combination of move, scale, skew and snap transformations.</td>
    </tr>
    <tr>
      <td>Transform Selected Glyphs</td>
      <td>Apply all transformations to glyph selection in the Font Overview.</td>
    </tr>
    <tr>
      <td>Transform All Glyphs</td>
      <td>Apply all transformations to all glyphs in the font.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Mirror Selected Glyphs Vertical</td>
      <td>Mirror all selected glyphs vertically.</td>
    </tr>
    <tr>
      <td>Mirror Selected Glyphs Horizontal</td>
      <td>Mirror all selected glyphs horizontally.</td>
    </tr>
    <tr>
      <td>Mirror All Glyphs Vertical</td>
      <td>Mirror all glyphs vertically.</td>
    </tr>
    <tr>
      <td>Mirror All Glyphs Horizontal</td>
      <td>Mirror all glyphs horizontally.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Transform Again</td>
      <td>Apply the same transformation again on the current glyph. (Handy if the transformation is set from a Transform action in the Glyph View.)</td>
    </tr>
    <tr>
      <td>Transform Again Selected Glyphs</td>
      <td>Apply the same transformation again on the selected glyphs.</td>
    </tr>
    <tr>
      <td>Transform Again All Glyphs</td>
      <td>Apply the same transformation again on all glyphs in the font.</td>
    </tr>
  </tbody>
</table>

The gear icon opens a second popup menu with options to control the scope of the transformation within each glyph.

{% image workspace/inspector_transform_options.png %}

<table>
  <thead>
    <tr>
      <th width="45%">option</th>
      <th width="55%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Transform Contours</td>
      <td>Apply transformations to contours.</td>
    </tr>
    <tr>
      <td>Transform Metrics</td>
      <td>Apply transformation to metrics.</td>
    </tr>
    <tr>
      <td>Transform Components</td>
      <td>Apply transformation to components.</td>
    </tr>
    <tr>
      <td>Transform Anchors</td>
      <td>Apply transformation to anchors.</td>
    </tr>
    <tr>
      <td>Transform Image</td>
      <td>Apply transformation to an image.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Align Off Curve Points</td>
      <td>Apply alignment to off curve points.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Set Italic Angle as Skew α Value</td>
      <td>Use the skew angle as the font’s Italic Angle.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Reset Panel</td>
      <td>Reset the panel to default settings.</td>
    </tr>
  </tbody>
</table>

> - {% internallink "glyph-editor/transform" text='Glyph Editor > Transform' %}
> - {% internallink "toolkit/transformations" text='Building Tools > Toolkit > Using transformations' %}
{: .seealso }

## Components

The *Components* section shows a list of all components in the current glyph.

{% image workspace/inspector_components.png %}

Double-click on any value to edit it.

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>✔︎</td>
    <td>Select/deselect component.</td>
  </tr>
  <tr>
    <td>Base Glyph</td>
    <td>The glyph referenced by the component.</td>
  </tr>
  <tr>
    <td>Transformation</td>
    <td>The component’s <em>transformation matrix</em> representing its translation, scale, rotation and skew settings.</td>
  </tr>
</table>

> - {% internallink "glyph-editor/components" text='Glyph Editor > Components' %}
> - {% internallink "building-tools/api/fontParts/rcomponent" text='FontParts > RComponent' %}
{: .seealso }

## Anchors

The *Anchors* section of the Inspector shows a list of all anchors in the current glyph.

{% image workspace/inspector_anchors.png %}

Double-click any value to edit it.

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>✔</td>
    <td>Select/deselect anchor.</td>
  </tr>
  <tr>
    <td>X / Y</td>
    <td>The position of the anchor.</td>
  </tr>
  <tr>
    <td>Name</td>
    <td>The name of the anchor.</td>
  </tr>
  <tr>
    <td>Color</td>
    <td>The color of the anchor. <em><em>(optional)</em></em></td>
  </tr>
</table>

> - {% internallink "glyph-editor/anchors" text='Glyph Editor > Anchors' %}
> - {% internallink "building-tools/api/fontParts/ranchor" text='FontParts > RAnchor' %}
{: .seealso }

## Guidelines

> [RoboFont 3.3]({{site.baseurl}}/downloads/)
{: .version-badge }

The *Guidelines* section shows a list of all global and local guidelines available in the current glyph.

{% image workspace/inspector_guidelines.png %}

Click and hold to edit any value. Double click on the color cell to edit the color.

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>X / Y</td>
    <td>The position of the guideline’s origin point.</td>
  </tr>
  <tr>
    <td>Angle</td>
    <td>The angle of the guideline in degrees.</td>
  </tr>
  <tr>
    <td>Name</td>
    <td>The name of the guideline. <em>(optional)</em></td>
  </tr>
  <tr>
    <td>Global</td>
    <td>
      <p>A checkbox indicating if the guideline is <em>global</em> (font-level) or <em>local</em> (glyph-level). Click on the checkbox to make a local guideline global, and vice versa.</p>
    </td>
  </tr>
  <tr>
    <td>Color</td>
    <td>The color of the guideline. <em>(optional)</em></td>
  </tr>
</table>

> - {% internallink "glyph-editor/guidelines" text='Glyph Editor > Guidelines' %}
> - {% internallink "building-tools/api/fontParts/rguideline" text='FontParts > RGuideline' %}
{: .seealso }

## Note

The *Note* section contains a simple text box for adding an optional note to the current glyph.

{% image workspace/inspector_note.png %}
