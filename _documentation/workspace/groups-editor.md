---
layout: page
title: Groups Editor
level: beginner
---

* Table of Contents
{:toc}

The Groups Editor makes it possible to create groups of glyphs.

Groups can be used for different purposes: spacing, kerning, organizing character sets, etc.

{% image workspace/groups-editor.png %}

> Groups are **not** used by the {% internallink 'workspace/features-editor' %}. Use OpenType classes instead.
{: .note }

## Creating groups

Create new groups by clicking on the + (plus sign) button at the bottom left of the sheet. Choose between a kerning group (1st or 2nd) or a regular group.

{% image workspace/groups-editor_add-group.png %}

Double-click on a group’s name to edit it.

## Adding glyphs to groups

First, open the font overview by clicking on the Show All Glyphs button.

{% image workspace/groups-editor_show-all-glyphs.png %}

Then, add glyphs to a group by dragging them from the font overview (bottom) into the current group (top).

## Stacked mode

The glyphs in each group can be visualized as a grid of glyph cells, or can be stacked on top of each other for easier comparison.

{% image workspace/groups-editor_stacked.png %}

The stacked mode is specially useful when creating spacing groups or kerning groups, when all glyphs in a group have a similar left or right side. Use the aligment option to match the glyphs to the appropriate side for comparison.

## Options

<table>
  <tr>
    <th width="35%">option</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>+</td>
    <td>Create a new group.</td>
  </tr>
  <tr>
    <td>-</td>
    <td>Delete the selected group.</td>
  </tr>
  <tr>
    <td>Show/hide all glyphs</td>
    <td>Toggle the font overview showing all glyphs in the font.</td>
  </tr>
  <tr>
    <td>Glyph cell mode</td>
    <td>Switch to glyph cells display mode.</td>
  </tr>
  <tr>
    <td>Glyph list mode</td>
    <td>Switch to glyphs list display mode.</td>
  </tr>
  <tr>
    <td>Stacked</td>
    <td>Display glyphs stacked on top of each other.</td>
  </tr>
  <tr>
    <td>Overview</td>
    <td>Display glyphs as a grid of glyph cells.</td>
  </tr>
</table>

## Actions

<table>
  <tr>
    <th width="35%">option</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Add glyphs to group by dragging from All Glyphs into the current group.</td>
  </tr>
  <tr>
    <td>double-click group</td>
    <td>Edit the group’s name.</td>
  </tr>
</table>
