---
layout: page
title: Smart Sets panel
tags:
  - glyph set
level: beginner
---

* Table of Contents
{:toc}

The Font Overview window includes a collapsible Smart Sets panel, which can be toggled with one of the Views icons in the Toolbar.

{% image workspace/smart-sets.png %}

A Smart Set is a list of saved Search & Find queries. Smart Sets can be grouped with Smart Groups.

There are two kinds of Smart Sets:

Query-based Smart Sets
: Dynamic Smart Sets defined by a query. Identified by a little gear icon.

List-based Smart Sets
: Static Smart Sets defined by a list of glyph names.

## Actions

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Show all glyphs in the selected Smart Set.</td>
  </tr>
  <tr>
    <td>double click</td>
    <td>Edit the Smart Set item, either as a search query or as a list of glyph names.</td>
  </tr>
  <tr>
    <td>right click (only for list-based Smart Sets)</td>
    <td>Show a contextual menu with a list of all missing glyphs, and an option to create them.</td>
  </tr>
  <tr>
    <td>⌥ + click</td>
    <td>Set all glyphs in the Smart Set as selected.</td>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Change the order of the Smart Sets.</td>
  </tr>
  <tr>
    <td>delete (del or backspace)</td>
    <td>Remove the selected Smart Set.</td>
  </tr>
</table>

## Options

{% image workspace/smart-sets_options.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Delete Item</td>
    <td>Delete the selected Smart Set.</td>
  </tr>
  <tr>
    <td>Export Sets</td>
    <td>Export Smart Sets to a <code>.roboFontSets</code> file.</td>
  </tr>
  <tr>
    <td>Load Sets</td>
    <td>Load Smart Sets from a <code>.roboFontSets</code> file.</td>
  </tr>
  <tr>
    <td>Export To Group</td>
    <td>Export the selected set to a Group.</td>
  </tr>
  <tr>
    <td>Create new Smart Set from glyph names</td>
    <td>Pops up a sheet where you can create a Smart Set based on a list of glyph names.</td>
  </tr>
  <tr>
    <td>New Smart Group</td>
    <td>Create a new Smart Group to store and sort Smart Sets.</td>
  </tr>
</table>

## Creating list-based Smart Sets

To create a new list-based Smart Set, choose ‘Create new set from glyph names’ from the options menu (bottom left of the panel). This will open a sheet with input fields for Set Name and for a space-separated list of glyph names.

{% image workspace/smart-sets_create-set-from-names.png %}

Press OK to create the Smart Set.

### Adding missing glyphs

Each list-based Smart Set displays a counter with the total amount of glyphs in the set. If available, a second counter (in parentheses) shows how many glyphs in the set are missing from the font.

{% image workspace/smart-sets_counters.png %}

To create missing glyphs in a Smart Set, right-click the Smart Set to open the list of missing glyph names, and double-click a glyph name to create the glyph.

{% image workspace/smart-sets_create-missing-glyphs.png %}

## Creating Query-based Smart Sets

Query-based Smart Sets are defined by one or more search queries. The glyph names in these sets are the result of the query applied to the current font.

The Search & Find panel can be toggled with one of the Views icons in the Toolbar, or with the keyboard shortcut ⌘ + F.

{% image workspace/search-query.png %}

{% comment %}
<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Selection to Set</td>
    <td></td>
  </tr>
  <tr>
    <td>Save Set</td>
    <td></td>
  </tr>
  <tr>
    <td>+</td>
    <td></td>
  </tr>
  <tr>
    <td>-</td>
    <td></td>
  </tr>
</table>
{% endcomment %}

> - {% internallink "using-smart-sets" %}
> - {% internallink "workspace/search-glyphs-panel" %}
> - {% internallink "adding-and-removing-glyphs" %}
> - {% internallink "defining-character-sets" %}
{: .seealso }
