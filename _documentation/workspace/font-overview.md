---
layout: page
title: Font Overview
tags:
  - glyph set
level: beginner
---

* Table of Contents
{:toc}

The Font Overview is the parent window for a font in the RoboFont UI. It displays all glyphs in the font as a grid of cells, and gives access to different tools and interfaces for editing different kinds of data in the font.

{% image workspace/font-overview.png %}

## Toolbar

{% image workspace/font-overview_toolbar.png %}

The toolbar icons allow you to navigate between the different editing interfaces in RoboFont:

<table>
  <thead>
    <tr>
      <th width='30%'>button</th>
      <th width='70%'>action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Font</td>
      <td>switch to the Font Overview</td>
    </tr>
    <tr>
      <td>Features</td>
      <td>Switch to the <a href='../features-editor'>Features Editor.</a></td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Font Info</td>
      <td>Show the <a href='../font-info-sheet'>Font Info sheet</a>.</td>
    </tr>
    <tr>
      <td>Space Center</td>
      <td>Open the <a href='../space-center'>Space Center</a>.</td>
    </tr>
    <tr>
      <td>Kern Center</td>
      <td>Show the <a href='../kern-center'>Kern Center</a>.</td>
    </tr>
    <tr>
      <td>Groups</td>
      <td>Show the <a href='../groups-editor'>Groups Editor</a>.</td>
    </tr>
    <tr>
      <td>Sort</td>
      <td>Show the <a href='../sort-glyphs-sheet'>Sort Glyphs sheet</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Mark</td>
      <td>Show a popup menu with mark colors.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Sets</td>
      <td>Toggle the <a href='../smart-sets-panel'>Smart Sets panel</a>.</td>
    </tr>
    <tr>
      <td>Find</td>
      <td>Toggle the <a href='../search-glyphs-panel'>Search Glyphs panel</a>.</td>
    </tr>
  </tbody>
</table>

> In {% internallink "workspace/window-modes#single-window-mode" text="Single Window mode" %}, the *Views* buttons are located at the bottom left of the window, and the toolbar also includes the {% internallink "workspace/glyph-editor" %}’s editing tools.
{: .note }

## Display Modes

The Font Overview has two display modes: *Tiled Glyphs* and *Glyph List*. Click on the icons at the bottom left of the window to switch between the two modes.

{% image workspace/font-overview_display-modes.png %}

### Tiles Mode

The Tiles Mode shows the font as a grid of glyph cells. The size of the cells can be adjusted by zooming in and out with the keyboard (using ⌘+ and ⌘-) or using the slider at the bottom right of the window.

{% image workspace/font-overview_glyph-cells.png %}

- Large cells show several layers of data: glyph name, glyph shape, width and margins, layers, vertical metrics. Smaller cells show only the glyph shape.

- Unsaved glyphs have their names highlighted until the font is saved again.

- The glyph’s mark color is used as the background color of the cell.

- A small `L` indicates that the glyph contains more than one layer.

- A small `N` indicates that the glyph contains a note.

### List Mode

The List Mode displays the font as a large table with the attributes of each glyph.

{% image workspace/font-overview_list-mode.png %}

Click on the column headers to sort the glyphs according to different attributes.

Individual columns can be toggled using the table’s contextual menu (right-click the table header to open it):

{% image workspace/font-overview_list-mode-columns.png %}

## Actions

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>single click</td>
    <td>Select single glyph.</td>
  </tr>
  <tr>
    <td>⌥ + single click (Multi‑Window mode)</td>
    <td>Opens a new Glyph Window.</td>
  </tr>
  <tr>
    <td>single click (Single Window mode)</td>
    <td>Select single glyph and send it to the Glyph View.</td>
  </tr>
  <tr>
    <td>double click (Single Window mode)</td>
    <td>Send the glyph to the Glyph View.</td>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>
      <b>Drag the selection on the same Font Overview:</b>
      <ul>
        <li>Drop between the glyph cells: Reorder the glyphs in the font.</li>
        <li>Drop on a glyph cell: Copy the dragged glyph into the destination glyph cell. Use ⌥ to copy as component.</li>
      </ul>
      <b>Drag the selection to other views:</b>
      <ul>
        <li>Other Font Overview: Add glyphs to the other font, and update the glyph order based on the drop position.</li>
        <li>Space Center: Set the glyph names as input for spacing. Use ⌥ to append the glyph names to the current input text.</li>
        <li>Smart Sets: Creates a new Smart Set with the dragged glyphs.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>⌘ + click + drag</td>
    <td>Modify the selection while moving the mouse.</td>
  </tr>
  <tr>
    <td>copy</td>
    <td>Copies the selected glyphs to the clipboard as glyph objects, and the glyph names as string representation.</td>
  </tr>
  <tr>
    <td>copy as component</td>
    <td>Copies the selected glyph as component (single glyph selection only).</td>
  </tr>
  <tr>
    <td>paste</td>
    <td>
      <ul>
        <li>If there is no selection, the copied glyphs are appended to the font.</li>
        <li>If the amount of selected glyphs is the same as the amount of copied glyphs, RoboFont will replace the selected glyphs with the copied glyphs.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>undo/redo</td>
    <td>Undo or redo the selected glyph(s).</td>
  </tr>
  <tr>
    <td>start typing a glyph name</td>
    <td>Tries to find and select that glyph.</td>
  </tr>
  <tr>
    <td>⌘ + J</td>
    <td>Opens a Jump To Glyph window.</td>
  </tr>
  <tr>
    <td>delete</td>
    <td>Remove the glyph from the font.</td>
  </tr>
  <tr>
    <td>⌥ + delete</td>
    <td>Remove the glyph from the font and the template glyph from the Font Overview.</td>
  </tr>
</table>

## Template glyphs

The Font Overview may show *template glyphs* – glyphs which are part of the current character set, but are not yet included in the font.

{% image workspace/font-overview_template-glyphs.png %}

Template glyphs are displayed in grey cells and show a preview glyph in a standard font. The font can be changed in the {% internallink 'workspace/preferences-window/character-set' %}.

To transform a template glyph into an actual glyph, double-click its glyph cell to open the {% internallink 'workspace/glyph-editor' %} and start drawing, or change some glyph data using the {% internallink 'workspace/inspector' %}.

## Load All Glyphs

When a font containing more glyphs than the *Max glyph count* limit is opened, a small button labeled *Load All Glyphs* will appear at the bottom left of the window

{% image workspace/font-overview_load-all-glyphs.png %}

> - {% internallink 'how-tos/working-with-large-fonts' %}
{: .seealso }

{% comment %}

- - -

> - {% internallink "how-tos/adding-and-removing-glyphs" %}
> - {% internallink "how-tos/renaming-glyphs" %}
> - {% internallink "how-tos/marking-glyphs" %}
> - {% internallink "how-tos/sorting-glyphs" %}
> - {% internallink "how-tos/searching-glyphs" %}
> - {% internallink "how-tos/using-smart-sets" %}
{: .seealso }

[AFDKO fea syntax]: http://adobe-type-tools.github.io/afdko/OpenTypeFeatureFileSpecification.html

{% endcomment %}