---
layout: page
title: Miscellaneous Preferences
treeTitle: Miscellaneous
level: beginner
---

* Table of Contents
{:toc}

The Misc section of the Preferences collects various settings which are not related to a specific task.

{% image workspace/preferences_misc.png %}

<table>
  <thead>
    <tr>
      <th width='35%'>option</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Single Window mode</td>
      <td>Switch between Single Window and Multi-Window modes. See <a href="../../window-modes">Window Modes</a>.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Maximum Tools in Toolbar</td>
      <td>Set the maximum amount of tools displayed in the Glyph Editor’s toolbar.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Mark Colors</td>
      <td>Manage mark colors and their labels. Use the +/- buttons to add/remove colors. Double-click a color or a color name to edit it.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Test Install Options</td>
      <td>Define if Autohint and Remove Overlap should be performed when running Test Install.</td>
    </tr>
    <tr>
      <td>Test Install Format</td>
      <td>Define the font format for Test Install: OpenType CFF (<code>.otf</code>) or OpenType TrueType (<code>.ttf</code>).</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Use embedded FDK for font generation</td>
      <td>Switch between the embedded AFDKO and a locally installed version.</td>
    </tr>
    <tr>
      <td>Save FDK parts next to UFO</td>
      <td>Save all files used by AFDKO’s <code>makeotf</code> program next the UFO, so they can be inspected afterwards.</td>
    </tr>
    <tr>
      <td>Auto save</td>
      <td>Automatically save a copy of the font every 10 minutes in the chosen directory.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Automatically Check for updates</td>
      <td>Automatically check for RoboFont updates during start up.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Warn when leaving RoboFont with unsaved fonts</td>
      <td>Make the RoboFont icon jump in the Dock when RoboFont becomes inactive and there are unsaved fonts.</td>
    </tr>
    <tr>
      <td>Reset all dialog warnings</td>
      <td>Reset all “Don’t ask again” dialogs.</td>
    </tr>
  </tbody>
</table>
