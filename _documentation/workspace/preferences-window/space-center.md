---
layout: page
title: Space Center Preferences
treeTitle: Space Center
level: beginner
tags:
  - spacing
---

* Table of Contents
{:toc}

The Space Center section contains settings related to the {% internallink "workspace/space-center" %}.

## Appearance

This sub-section allows you to customize the colors used by the Space Center.

To edit a color, double-click a color cell to open the Color Picker.

{% image workspace/preferences_space-center_appearance.png %}

<table>
  <tr>
    <th width='45%'>option</th>
    <th width='55%'>description</th>
  </tr>
  <tr>
    <td>Flexible input cell width</td>
    <td>Choose between flexible or fixed width columns in the Space Matrix.</td>
  </tr>
  <tr>
    <td>Show .notdef for non-existing glyphs</td>
    <td>Show a .notdef glyph for input glyphs which are not included in the font.</td>
  </tr>
</table>

## Input text

This sub-section allows you to store text strings for use in the Space Center.

{% image workspace/preferences_space-center_input-text.png %}

> Input strings may contain special wildcard glyph names such as `/?` (current glyph) and `/!` (selected glyphs). See {% internallink "space-center#text-input-field" text="Space Center > Text Input Field" %}.
{: .tip }

## Hot keys

{% image workspace/preferences_space-center_hotkeys.png %}
