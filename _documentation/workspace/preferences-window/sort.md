---
layout: page
title: Sorting Preferences
treeTitle: Sorting
level: beginner
tags:
  - character set
  - glyph names
---

* Table of Contents
{:toc}

The Sorting section of the Preferences contains settings related to the sorting of glyphs in the {% internallink "workspace/font-overview" %}.

This setting defines the default sorting order which is used for all new fonts. This global setting can be overruled by a local setting in the font.

{% image workspace/preferences_sort-smart.png %}

{% image workspace/preferences_sort-character-set.png %}

{% image workspace/preferences_sort-custom.png %}

> See {% internallink "sorting-glyphs" %} for a more detailed description of each sorting option.
{: .seealso }
