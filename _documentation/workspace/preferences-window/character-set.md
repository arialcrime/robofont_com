---
layout: page
title: Character Set Preferences
treeTitle: Character Set
tags:
  - character set
level: beginner
---

The Character Sets section contains settings related to the {% glossary character set %}s used to sort glyphs in the {% internallink "workspace/font-overview" %}.

{% image workspace/preferences_character-set.png %}

The window shows a list of all saved character sets (left column), and a list of all glyph names in the selected character set (right column).

## Options

<table>
  <thead>
    <tr>
      <th width='40%'>option</th>
      <th width='60%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Default charset new document</td>
      <td>The default character set for new fonts.</td>
    </tr>
    <tr>
      <td>+</td>
      <td>Create a new character set.</td>
    </tr>
    <tr>
      <td>-</td>
      <td>Delete the selected character set.</td>
    </tr>
    <tr>
      <td>Import from .enc file</td>
      <td>Import character set from an <code>.enc</code> file.</td>
    </tr>
    <tr>
      <td>Import from .ufo file</td>
      <td>Import character set from another <code>.ufo</code> font.</td>
    </tr>
    <tr>
      <td>Import from Current Font</td>
      <td>Import character set from the current font.</td>
    </tr>
    <tr>
      <td>Import from All Open Fonts</td>
      <td>Import the combined character set from all open fonts.</td>
    </tr>
  <tbody>
  </tbody>
    <tr>
      <td>Deleting glyphs also removes glyph name from</td>
      <td>Remove glyph names from kerning and groups when a glyph is deleted.</td>
    </tr>
  <tbody>
  </tbody>
    <tr>
      <td>Template glyph preview font</td>
      <td>The font used to draw the template glyph cells. The font must be installed on your system.</td>
    </tr>
    <tr>
      <td>Template glyphs missing unicode character</td>
      <td>If a glyph does not have an unicode value, this character will be used as replacement.</td>
    </tr>
  <tbody>
  </tbody>
    <tr>
      <td>New glyph width</td>
      <td>The default width for newly added glyphs.</td>
    </tr>
  <tbody>
  </tbody>
    <tr>
      <td>AGL/GNFUL path</td>
      <td>Optionally provide a <a href="http://github.com/adobe-type-tools/agl-aglfn">AGL</a> (Adobe Glyph List) or a <a href="http://github.com/LettError/glyphNameFormatter">GNFUL</a> (Glyph Name Formatted Unicode List) file to map glyph names to unicode values.</td>
    </tr>
  </tbody>
</table>

## Editing character sets

Use the +/- buttons to create or delete character sets.

To modify a character set, simply add, remove or modify glyph names from the list.

> - {% internallink "defining-character-sets" %}
{: .seealso }
