---
layout: page
title: Images
tags:
  - images
level: beginner
---

* Table of Contents
{:toc}

The Glyph View can show a background image for each layer. This feature can be used to import scans of drawings or of printed typefaces for digitisation.

## Adding images

To add an image to the current layer of the current glyph, simply drag & drop any `.png`, `.jpeg` or `.tiff` file into the Glyph View.

{% image workspace/image_editing.png %}

Each layer can contain only one image.

> To add multiple images to a glyph, use multiple layers (one for each image).
{: .tip }

Imported images are stored in an `images` directory inside the UFO3 font.

> In RoboFont 1 (UFO2), only the path to the image file is stored in the UFO (the image data is not). Use the option *Save images next to the UFO* to collect all images inside an `assets` folder next to the UFO file.
{: .note }

## Adjusting images

Images can moved, scaled and rotated with the {% internallink 'editing-tool' %}.

The contextual menu (activated with a right-click) offers some options to adjust the image:

{% image workspace/image_menu.png %}

### Options

<table>
  <thead>
    <tr>
      <th width="25%">action</th>
      <th width="75%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Brightness</td>
      <td>Adjust the brightness of the image.</td>
    </tr>
    <tr>
      <td>Contrast</td>
      <td>Adjust the contrast of the image.</td>
    </tr>
    <tr>
      <td>Saturation</td>
      <td>Adjust the saturation of the image.</td>
    </tr>
    <tr>
      <td>Sharpness</td>
      <td>Adjust the Sharpness of the image.</td>
    </tr>
    <tr>
      <td>Color</td>
      <td>Colorize the image. Use Red, Green, Blue & Alpha sliders to adjust the color.</td>
    </tr>
    <!-- this option is not available in the contextual menu
    <tr>
      <td>Black & White</td>
      <td>Toggle the selected image to black & white.</td>
    </tr>
    -->
  </tbody>
  <tbody>
    <tr>
      <td>Set Scale</td>
      <td>Opens a <a href="#adjusting-scale">sheet</a> to adjust image size and positioning.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Lock Image</td>
      <td>Locks the image, so it cannot be edited by mistake while editing the contours. Use the contextual menu (right-click) to unlock it.</td>
    </tr>
  </tbody>
</table>

### Adjusting scale

Before you can start digitising an image, you’ll need to adjust the scanned drawing to the glyph’s coordinate space.

The Set Scale sheet makes it easy to position and scale your image
by clicking on 3 reference points:

{% image workspace/image_scaling.png %}

1. The origin point, or `(0, 0)`.
2. A second point along the baseline. This will define the x-axis.
3. A third point to define the height of the image. This point can be at the x-height, cap-height, ascender or descender.

The point positions are editable and can be fine-tuned after the points have been created (just click on a point to move it).

When you’re done, click on the Scale button to apply the settings. The image will be transformed to match the 3 points.

## Deleting images

To delete an image, select it with the Editing Tool, and press the backspace key.
