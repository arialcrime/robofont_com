---
layout: page
title: Guidelines
tags:
  - guidelines
level: beginner
---

* Table of Contents
{:toc}

Guidelines are moveable lines which can be used as visual aids during the design process. They are defined by a position and an angle, and are not part of the glyph’s contours.

{% image workspace/guidelines.png %}

Guidelines can be of two kinds:

font-level (global)
: displayed in all glyphs

glyph-level (local)
: displayed in one glyph only

In UFO3, each guideline may also have a color attribute and a unique identifier.

> [RoboFont 3.3]({{site.baseurl}}/downloads/)
{: .version-badge }

Individual guideline colors are displayed in the Glyph View if available, and can be edited in the [Guidelines] section of the Inspector panel.

## Creating and editing guidelines

To create a new guideline, drag a line from one of the rulers into the Glyph View.

> Make sure *Rulers* and *Guides* are visible in the {% internallink "display-options#layers-of-information" %}.
{: .note }

There are different ways to edit a guideline:

1. Right-click the guideline to open a [contextual menu](#contextual-menu), and edit the guideline’s properties.

2. Use the [Guidelines] section of the Inspector to edit the guideline’s attributes, including its color.

## Actions

<table>
    <tr>
      <th width='35%'>action</th>
      <th width='65%'>description</th>
    </tr>
    <tr>
      <td>drag from rulers</td>
      <td>Create a guideline.</td>
    </tr>
    <tr>
      <td>⌘ + drag from rulers</td>
      <td>Create a global (font-level) guideline.</td>
    </tr>
    <tr>
      <td>⌥ + drag</td>
      <td>Rotate the guideline.</td>
    </tr>
    <tr>
      <td>mouse over</td>
      <td>Cursor displays guideline values.</td>
    </tr>
    <tr>
      <td>drag over point in glyph</td>
      <td>Snap the guideline to a point.</td>
    </tr>
    <tr>
      <td>drag out of the view</td>
      <td>Delete the guideline.</td>
    </tr>
  </table>

## Contextual menu

{% image workspace/guidelines_menu.png %}

<table>
  <thead>
    <tr>
      <th width='35%'>option</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Name</td>
      <td>Name of the guideline.</td>
    </tr>
    <tr>
      <td>Position</td>
      <td>The guideline’s center point as a <code>(x,y)</code> coordinate.</td>
    </tr>
    <tr>
      <td>Angle</td>
      <td>Angle of the guideline.</td>
    </tr>
    <tr>
      <td>Is Global</td>
      <td>Make the guideline global (font-level) or local (only this glyph).</td>
    </tr>
    <tr>
      <td>Display Measurements</td>
      <td>Display measurements where the guideline interesects the glyph.</td>
    </tr>
    <tr>
      <td>Magnetic</td>
      <td>Determine how fast a dragged selection should snap to the guideline.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Lock Guides</td>
      <td>Lock all guidelines.</td>
    </tr>
    <tr>
      <td>Lock Images</td>
      <td>Lock all images.</td>
    </tr>
    <tr>
      <td>Slice Glyph</td>
      <td>Slice the glyph at the intersections with the guideline.</td>
    </tr>
    <tr>
      <td>Delete Guide</td>
      <td>Delete the guideline.</td>
    </tr>
  </tbody>
</table>

> - [UFO3 specification > Guideline](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/#guideline)
> - {% internallink 'building-tools/api/fontParts/rguideline' text='FontParts > RGuideline' %}
{: .seealso }

[Guidelines]: ../../inspector#guidelines
