---
layout: page
title: Transform
tags:
level: beginner
---

* Table of Contents
{:toc}

The Transform mode offers a way to interactively transform a selection or the whole glyph.

## Activating Transform mode

Transform is available when the {% internallink "editing-tool" %} is active. To enter Transform mode, use the keyboard shortcut ⌘+T, or choose *Glyph > Transform* from the main application menu. Double-clicking the selection will also turn Transform mode on.

{% image workspace/transform.png %}

To exit Transform mode, double-click anywhere outside the transformation box.

## Transformations

When Transform is active, a frame with handles will appear around the current selection. Use the handles to scale, rotate and skew the selection.

### Translate

Move the selection around.

Translate is the default Transform setting (square corner points).

{% image workspace/transform-translate.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Translate the selection.</td>
  </tr>
  <tr>
    <td>click + drag corner or middle point</td>
    <td>Scale the selection.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Translate the selection on x, y or 45° axis.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag corner or middle point</td>
    <td>Constrain translation to x, y or 45° axis.</td>
  </tr>
</table>

### Scale

Scale the selection.

Activated when ⌘ (Command) is down (diamond corner points).

{% image workspace/transform-scale.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Start scaling the selection.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Constrain scale to x, y or 45° axis, or keep horizontal and vertical scale factors proportional.</td>
  </tr>
</table>

### Rotate

Rotate the selection.

Activated when ⌥ (Alt) is down (round corner points).

{% image workspace/transform-rotate.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Click to define the center of rotation, and drag to rotate around it.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Constrain rotation to 0, 45° or 90°.</td>
  </tr>
</table>

### Skew

Skew the selection.

Activated when ⌥ ⌘ (Alt + Command) are down (triangular corner points).

{% image workspace/transform-skew.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Click to define a reference point, and drag to skew around it.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Constrain skew angle to 0, 45° or 90°.</td>
  </tr>
</table>
