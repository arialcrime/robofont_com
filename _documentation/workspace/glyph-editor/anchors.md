---
layout: page
title: Anchors
tags:
  - anchors
level: beginner
---

Anchors are single points with a position and a name. They are commonly used as reference points for attaching components to base glyphs.

{% image workspace/anchors.png %}

In [UFO3][UFO3 > Anchor], each anchor may also have a color attribute and a unique identifier.

> [RoboFont 3.3]({{site.baseurl}}/downloads/)
{: .version-badge }

Individual anchor colors are displayed in the Glyph View if available, and can be edited in the [Anchors] section of the Inspector panel.

## Adding anchors

To add anchors to a glyph, follow these steps in the Glyph Editor:

1. Right-click on the canvas to activate the Editing Tool’s {% internallink "glyph-editor/tools/editing-tool#contextual-menus" text="contextual menu" %}.

2. Choose *Add Anchor* to open the Add Anchor sheet:

  {% image workspace/anchors_add.png %}

3. Give the anchor a name (required).

4. Click on the *Add* button or press Enter to create the anchor.

## Editing anchors

Individual anchor attributes can be edited using the [Anchors] section of the Inspector.

> - [UFO3 specification > Anchor][UFO3 > Anchor]
> - {% internallink 'building-tools/api/fontParts/ranchor' text='FontParts > RAnchor' %}
> - {% internallink "how-tos/building-accented-glyphs" %}
{: .seealso }

[UFO3 > Anchor]: http://unifiedfontobject.org/versions/ufo2/glyphs/glif/#anchor
[Anchors]: ../../inspector#anchors
