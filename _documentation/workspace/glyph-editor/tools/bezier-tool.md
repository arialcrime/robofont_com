---
layout: page
title: Bezier Tool
tags:
  - contours
level: beginner
---

{% image workspace/glyph-editor_bezier-tool.png %}

A tool to create and edit contours.

## Actions

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click (no selection)</td>
    <td>Add a point.</td>
  </tr>
  <tr>
    <td>⌥ + click (no selection)</td>
    <td>Add a line segment.</td>
  </tr>
  <tr>
    <td>click on selection</td>
    <td>When the point is the last point of an open contour, the contour will become the main contour for adding points.</td>
  </tr>
  <tr>
    <td>⇧ + click</td>
    <td>Add point constrained to x, y or 45° of previous on-curve point.</td>
  </tr>
  <tr>
    <td>drag</td>
    <td>Add BCPs. All new points will have a <code>smooth</code> attribute.</td>
  </tr>
  <tr>
    <td>⇧ + drag</td>
    <td>Move BCPs of last added point on x, y or 45° axis.</td>
  </tr>
  <tr>
    <td>⌥ + drag</td>
    <td>Remove the <code>smooth</code> attribute from BCP.</td>
  </tr>
  <tr>
    <td>⌥ + ⌘ + click</td>
    <td>Add a point on a contour.</td>
  </tr>
  <tr>
    <td>move over starting point</td>
    <td>Indicates if the contour can be closed.</td>
  </tr>
  <tr>
    <td>double click</td>
    <td>Add a point and close the contour.</td>
  </tr>
</table>

## Keyboard shortcuts

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>arrow keys</td>
    <td>Move last added point by 1 unit.</td>
  </tr>
  <tr>
    <td>⇧ + arrow keys</td>
    <td>Move last added point by 10 units.</td>
  </tr>
  <tr>
    <td>⇧ + ⌘ + arrow keys</td>
    <td>Move last added point by 100 units.</td>
  </tr>
</table>

{% comment %}
> - Add demo screencast.
{: .todo }
{% endcomment %}
