---
layout: page
title: About Window
level: beginner
---

The About window displays information about your current version of RoboFont.

{% image workspace/about-window.png %}

Next to credits, acknowledgments and copyrights, the window also includes version information for:

- RoboFont (version number, build number)
- embedded {% glossary AFDKO %} (version number)
- embedded GlyphNameFormatter (version number, git commit)

This information is important when {% internallink 'how-tos/submitting-bug-reports' %}.

<!--
{% comment %}

{% glossary GlyphNameFormatter %}
{% endcomment %}
-->
