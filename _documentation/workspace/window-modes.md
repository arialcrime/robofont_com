---
layout: page
title: Window Modes
tags:
  - UI
level: beginner
---

RoboFont contains many different windows and views, each one dedicated to a specific task. The application’s interface can be organized in two different modes:

- [Single Window](#single-window-mode)
- [Multi-Window](#multi-window-mode)

> The current window mode can be set in {% internallink "preferences-window/miscellaneous" text="Preferences > Miscellaneous" %}.
{: .note }

## Single Window Mode

In *Single Window* mode, all windows are collected as panels of one single application window. Individual panels can be shown/hidden using the Views icons in the bottom bar.

{% image workspace/window-modes_single.png %}

## Multi-Window Mode

In *Multi-Window* mode, windows are available separately and each window can be freely positioned. This can be useful when working with multiple monitors or with several fonts at the same time.

{% image workspace/window-modes_multi.png %}
