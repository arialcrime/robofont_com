---
layout: page
title: Introduction to Python
tree:
  - introduction
  - uses
  - terminal
  - robofont
  - drawbot
  - language
  - numbers-arithmetics
  - variables
  - strings
  - lists
  - tuples
  - sets
  - collections-loops
  - comparisons
  - conditionals
  - boolean-logic
  - dictionaries
  - builtin-functions
  - functions
  - objects
  - modules
  - standard-modules
  - external-modules
  - custom-modules
  - resources
treeCanHide: true
level: beginner
---

This section provides a quick introduction to the Python programming language. It covers the basics you need to know to get started with scripting in RoboFont.

{% comment %}
{% tree page.url levels=3 %}
{% endcomment %}

- [Introduction](introduction) <span class='green'>✔︎</span>
- [Uses of Python](uses) <span class='green'>✔︎</span>
- [Python in Terminal](terminal) <span class='green'>✔︎</span>
- [Python in RoboFont](robofont) <span class='green'>✔︎</span>
- [Python in DrawBot](drawbot) <span class='green'>✔︎</span>
- [Language basics](language) <span class='green'>✔︎</span>
- [Numbers and arithmetics](numbers-arithmetics) <span class='green'>✔︎</span>
- [Variables](variables) <span class='green'>✔︎</span>
- [Strings](strings) <span class='green'>✔︎</span>
- [Lists](lists) <span class='green'>✔︎</span>
- [Tuples](tuples) <span class='green'>✔︎</span>
- [Sets](sets) <span class='green'>✔︎</span>
- [Collections and Loops](collections-loops) <span class='green'>✔︎</span>
- [Comparisons](comparisons) <span class='green'>✔︎</span>
- [Conditionals](conditionals) <span class='green'>✔︎</span>
- [Boolean logic](boolean-logic) <span class='green'>✔︎</span>
- [Dictionaries](dictionaries) <span class='green'>✔︎</span>
- [Built-in functions](builtin-functions) <span class='green'>✔︎</span>
- [Functions](functions) <span class='green'>✔︎</span>
- [Objects](objects) <span class='green'>✔︎</span>
- [Modules](modules) <span class='green'>✔︎</span>
- [Standard modules](standard-modules) <span class='red'>✘</span>
- [External modules](external-modules) <span class='red'>✘</span>
- [Custom modules](custom-modules) <span class='red'>✘</span>
- [Additional resources](resources) <span class='red'>✘</span>

> This chapter is being updated to Python 3… almost done! :)
{: .note }
