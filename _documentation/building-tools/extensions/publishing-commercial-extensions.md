---
layout: page
title: Publishing extensions on the Extensions Store
treeTitle: Publishing on the Extensions Store
tags:
  - extensions
level: intermediate
draft: true
---

{% image building-tools/extensions/profit.jpg %}

1. create a private repository for your extension on [GitLab](http://gitlab.com/)
2. ???
3. profit!

> Describe the steps required for publishing extensions on the Extension Store.
{: .todo }
