---
layout: page
title: Boilerplate RoboFont Extension
treeTitle: Boilerplate Extension
tags:
  - extensions
level: intermediate
---

The [Boilerplate Extension] is a dummy RoboFont extension which serves as a template for creating new extensions. It is hosted on GitHub and configured as a [template repository] – so you can use it to create new repositories with the same directory structure and files, and a fresh commit history.

Different developers have {% internallink 'building-tools/extensions/packaging-patterns' text='different ways of organising extension sources' %}. The Boilerplate Extension demonstrates a simple scheme in which all files are stored in a single repository, and the extension is built from the source files using a script:

> rf-extension-boilerplate
> ├── source/
> │   ├── code/
> │   ├── documentation/
> │   └── resources/
> ├── build/myExtension.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── resources/
> │   ├── info.plist
> │   └── license
> ├── README.md
> ├── build.py
> ├── license.txt
> ├── myExtension.mechanic
> └── myExtensionMechanicIcon.png
{: .asCode }

`source/`
: contains the ‘master files’ which are edited by the developer

`myExtension.roboFontExt`
: the generated extension package which is distributed to users

`build.py`
: a script to generate the extension package from the source files

`README.md`
: a [markdown] file with information about the repository

`license.txt`
: defines the terms under which the source code is made available

`myExtension.yml`
: a [yaml] file with metadata for installation with Mechanic

`myExtensionMechanicIcon.png`
: an icon to identify the extension in the Mechanic extension index

[Boilerplate Extension]: http://github.com/roboDocs/rf-extension-boilerplate
[template repository]: http://help.github.com/en/articles/creating-a-repository-from-a-template
[yaml]: http://yaml.org/
[markdown]: http://en.wikipedia.org/wiki/Markdown
