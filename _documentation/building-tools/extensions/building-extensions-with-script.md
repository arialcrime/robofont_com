---
layout: page
title: Building extensions with a script
treeTitle: Building with a script
tags:
  - extensions
level: intermediate
---

If you are more comfortable writing code than using UI-based tools like the {% internallink '/building-tools/extensions/building-extensions-with-extension-builder' text='Extension Builder' %}, you can also build your extension with a script using the `ExtensionBundle` object from {% internallink "documentation/building-tools/api/mojo/mojo-extensions#mojo.extensions.ExtensionBundle" text='`mojo.extensions`' %}.

The example script below is part of the {% internallink "boilerplate-extension" text='Boilerplate Extension' %}, and is written to work with the other files in that repository. If your files are organized differently, you may need to make some changes to the script.

{% showcode building-tools/extensions/buildExtension.py %}
