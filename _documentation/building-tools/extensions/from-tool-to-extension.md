---
layout: page
title: From tool to extension
tags:
  - extensions
level: intermediate
---

* Table of Contents
{:toc}

*This page describes the steps required to convert an existing tool into an extension.*

> If you know in advance that you want to build an extension, you can create your tool from the beginning as one. Have a look at the {% internallink "boilerplate-extension" text="Boilerplate Extension" %} for a template.
{: .tip }

## Preparing to build an extension

### Organizing files and folders

In order to build an extension you’ll first need to organize your files according to the {% internallink "extension-file-spec#extension-folder-structure text='Extension Folder Structure' %} – separating code, documentation and resources:

{% image building-tools/extensions/extensionFolderStructure.png %}

code folder <span class='required'>required</span>
: ^
  This is where the actual extension code goes.

  It may contain multiple subfolders, which should be named using [reverse domain syntax without dots](../extension-file-spec).

  If a developer does not want to ship readable source code, it is possible to include only compiled `.pyc` files in the extension.

documentation folder <span class='not-required'>optional</span>
: ^
  Contains the documentation in html or [markdown] format. Markdown files are converted to html when the extension is built.

  Must include an `index.html` or `index.md` file.

resources folder <span class='not-required'>optional</span>
: Contains additional files used by the extension, such as images, compiled tools, etc.

[markdown]: http://en.wikipedia.org/wiki/Markdown

### Preparing extension metadata

Every extension must also include a {% internallink "extension-file-spec#infoplist text='metadata file' %} with information such as:

- extension name <span class='required'>required</span>
- developer credits <span class='required'>required</span>
- version number <span class='required'>required</span>
- license <span class='not-required'>optional</span>

This information is added when building the extension.

### Preparing metadata for Mechanic 2

If you wish to make your extension available to all RoboFont users, you’ll also need to provide {% internallink "extensions/extension-item-format" text='additional data for Mechanic 2' %}:

- extension repository <span class='required'>required</span>
- tags <span class='required'>required</span>
- icon <span class='not-required'>optional</span>

This information must be added to the Mechanic 2 server after the extension is built.

## Building the extension

When all the data is ready and it’s time to build the extension, there are two options:

- {% internallink "building-extensions-with-extension-builder" %}
- {% internallink "building-extensions-with-script" %}

{% comment %}
> To include only compiled code in the extension, select *Collect only .pyc files* if you are using the Extension Builder, or set `pycOnly=True` if you are building the extension with a script.
{: .tip }
{% endcomment %}

> [RoboFont 3.3]({{site.baseurl}}/downloads/)
{: .version-badge }

Use the `info.plist` attribute `expireDate` to set an expiration date for trial extensions.

After you build the extension, you will have two versions of the code: the ‘unpackaged’ source, and the built extension package. You will need to make a decision about your workflow:

1. Continue treating the ‘unpackaged’ code as the source, and the extension as ‘packaged code’ for distribution.

2. Retire the ‘unpackaged’ code, and treat the extension as the (only) source code from now on.

> - {% internallink 'building-tools/extensions/packaging-patterns' %}
{: .seealso }

## Creating a Git repository

If you haven’t done so yet, now is a good time to create a [Git] repository to manage the changes in your code over time. You can choose between a public repository (for free & open-source extensions) or a private one (for custom or commercial extensions).

[Git]: http://git-scm.com/

> - {% internallink 'how-tos/using-git' %}
{: .seealso }

## Distributing the extension

Extensions can be distributed to other RoboFont users using [Mechanic].

The [Mechanic extension] allows users to install and manage extensions of all kinds, and to get updates as soon as they are made available by the developer.

Open-source extensions
: can be added to the [Mechanic server], so they are listed in the [Mechanic] website

Commercial extensions
: can be published in the [Extension Store], where users can pay for a license

Custom extensions
: can be distributed directly using `.mechanic` files or custom extension streams

> - {% internallink 'building-tools/extensions/publishing-extensions' %}
> - {% internallink 'extensions/managing-extension-streams' %}
{: .seealso }

[Mechanic]: http://robofontmechanic.com/
[Mechanic server]: http://github.com/robofont-mechanic/mechanic-2-server/
[Mechanic extension]: http://github.com/robofont-mechanic/mechanic-2
[Extension Store]: http://extensionstore.robofont.com/certified-developers/

## Maintaining the extension

As others start using your extension, you may receive bug reports, feature requests, comments etc. If your extension is open-source, you may also receive contributions by other developers. Use the tools provided by your Git platform of choice to manage the development process using issues, merge requests, branches, etc.

### Publishing updates

To make an update of your extension available to users via Mechanic it is necessary to increase the extension’s version number. The next time Mechanic checks for updates, it will show an ‘update available’ notification next to the extension name.
