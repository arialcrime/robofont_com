---
layout: page
title: Tools with Observers
tree:
  - introduction
  - the-observer-pattern
  - list-layers
  - custom-glyph-preview
  - show-glyph-box
  - draw-reference-glyph
  - draw-info-text-in-glyph-view
  - glyph-editor-subview
  - stencil-preview
  - custom-inspector-panel
  - custom-label-in-glyph-cells
  - custom-glyph-window-display-menu
  - custom-font-overview-contextual-menu
  - open-component-in-new-window
  - multifont-glyph-preview
tags:
  - observers
treeCanHide: true
level: advanced
---

**The Observer pattern adds a layer of abstraction between objects and interface, resulting in cleaner, more flexible code.**

{% comment %}
{% tree page.url levels=1 %}
separating manually between theory and examples
{% endcomment %}

- {% internallink 'observers/the-observer-pattern' %}
- {% internallink 'observers/introduction' %}

## Examples

- {% internallink 'observers/list-layers' %}
- {% internallink 'observers/custom-glyph-preview' %}
- {% internallink 'observers/show-glyph-box' %}
- {% internallink 'observers/draw-reference-glyph' %}
- {% internallink 'observers/draw-info-text-in-glyph-view' %}
- {% internallink 'observers/glyph-editor-subview' %}
- {% internallink 'observers/stencil-preview' %}
- {% internallink 'observers/custom-inspector-panel' %}
- {% internallink 'observers/custom-label-in-glyph-cells' %}
- {% internallink 'observers/custom-glyph-window-display-menu' %}
- {% internallink 'observers/custom-font-overview-contextual-menu' %}
- {% internallink 'observers/open-component-in-new-window' %}
- {% internallink 'observers/multifont-glyph-preview' %}

> - {% internallink 'how-tos/building-a-custom-key-observer' %}
> - [RoboFont Forum > observers](http://forum.robofont.com/tags/observers)
{: .seealso }

{% comment %}
see also http://github.com/typemytype/RoboFontExamples/tree/master/observers
- Simple draw observer
- Simple window observer
- Reorder contours
- Show distance between selected points
- Stencil preview
- View glyphs in current glyph mask
- Preview expanded strokes
- Display point count
{% endcomment %}
