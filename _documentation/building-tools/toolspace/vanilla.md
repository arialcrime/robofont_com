---
layout: page
title: Scripts using vanilla
tags:
  - scripting
  - vanilla
tree:
  - vanilla-intro
  - window-with-action-buttons
  - window-with-move-sliders
  - simple-font-library-browser
  - sheet-example
  - bulk-list
treeCanHide: true
level: intermediate
---

In the {% internallink "toolspace/scripts" text="previous section" %} we’ve seen how to create simple scripts to do things to fonts and glyphs. These scripts had no user interface – if we wanted to change a setting or a value, we would have to modify them directly in the code.

**In this section we’ll see how to create interfaces for our scripts using {% glossary vanilla %}.**

An interface is useful when a tool is used very often, or when you are building tools for others. Rather than changing settings and values directly in the code, we can offer the user a window with visual controls: for example checkboxes for selecting options, input fields or sliders to adjust values, button to apply actions, etc.

{% tree page.url levels=1 %}

> - [Tal Leming: Building Stuff (RoboThon 2012)](http://vimeo.com/38358195)
> - [macOS Human Interface Guidelines](http://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/)
> - [RoboFont Forum: vanilla](http://forum.robofont.com/tags/vanilla)
{: .seealso }
