---
layout: page
title: Using RoboFont’s mojo
treeTitle: Using mojo
tags:
  - scripting
  - mojo
tree:
  - glyph-preview
  - multiline-view
  - accordion-window
  - sort-fonts-list
  - open-glyph-in-new-window
  - space-center
  - canvas
  - decompose-point-pen
treeCanHide: true
level: intermediate
---

The RoboFont user interface is built with {% internallink "building-tools/toolspace/vanilla/vanilla-intro" text='vanilla' %} and [defconAppKit], and adds several custom UI elements of its own. These RoboFont-specific objects are available via the {% internallink "building-tools/toolkit/mojo" text='mojo' %} module – so you can use them to build your own tools, and to modify the way RoboFont looks and works.

mojo includes a lot more besides UI components: it gives access to all internal *events* and *observers* which form the backbone of the application. We’ll look into that in the {% internallink 'toolspace/observers' text='next section' %}.

**mojo allows developers to unlock the full power of RoboFont.**

{% tree page.url levels=1 %}

[defconAppKit]: http://github.com/robotools/defconappkit
