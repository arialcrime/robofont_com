---
layout: page
title: Simple sheet example
tags:
  - vanilla
level: intermediate
---

A simple example showing how to use a vanilla `Sheet`.

{% image building-tools/vanilla/SheetExample.png %}

{% showcode /building-tools/vanilla/sheetDemo.py %}
