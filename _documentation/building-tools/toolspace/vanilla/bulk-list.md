---
layout: page
title: Batch apply function to fonts
tags:
  - vanilla
level: intermediate
---

This example shows a little script template for applying a function to a set of fonts.

The interface contains a simple list which accepts drag & drop of UFO files, and a button to apply a function to all the files.

<!-- {% image building-tools/vanilla/bulkList_0.png %} -->

{% image building-tools/vanilla/bulkList_1.png %}

To customize the script, add your own code to the `doThing()` class.

{% showcode /building-tools/vanilla/bulkList.py %}

Contributed by [Jackson Cavanaugh (Okay Type)](http://gist.github.com/okay-type/06d64fcd0d2d3bcd03744510604b29ad)

> - [Sharing a useful scriptlet (RoboFont Forum)](http://forum.robofont.com/topic/680/sharing-a-useful-scriptlet)
> - [Batch extension](http://github.com/typemytype/batchRoboFontExtension)
{: .seealso }
