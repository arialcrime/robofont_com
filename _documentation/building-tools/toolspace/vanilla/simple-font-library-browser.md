---
layout: page
title: Simple font library browser
tags:
  - scripting
  - vanilla
level: intermediate
---

Imagine that your UFO sources are stored in subfolders of a main folder, with one folder for each family:

> myLibrary
> ├── myFamily1
> │   ├── Bold.ufo
> │   ├── Condensed.ufo
> │   ├── Italic.ufo
> │   └── Regular.ufo
> ├── myFamily2
> │   └── ...
> └── myFamily3
>     └── ...
{: .asCode }

This example shows how to create a simple tool to browse through the family folders and open the selected fonts:

{% image building-tools/vanilla/FontLibraryBrowser.png %}

> The code could be extended to do more than just opening the selected fonts – for example setting font infos, generating fonts etc.
{: .tip }

Some things to notice in this example:

layout variables
: The buttons and lists are placed and sized using layout variables (padding, button height, column widths, etc). This makes it easier to change dimensions during development.

Get Folder dialog
: ^
  1. When the user clicks on the *get root folder…* button, a `getFolder` dialog is opened.
  2. After the folder has been selected, a list of all its subfolders is displayed in the left column.

list selection
: ^
  Items selection works differently in each list:

  - The list of families allows only one item to be selected at once.
  - The list of fonts allows selection of multiple items (default `List` behavior).

show fonts in folder
: As the selection in the left column changes, the right column is updated to show a list of UFOs in the selected family folder.

Here’s the code:

{% showcode /building-tools/vanilla/simpleFontLibraryBrowser.py %}
