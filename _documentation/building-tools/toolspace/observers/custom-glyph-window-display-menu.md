---
layout: page
title: Custom display menu in the Glyph Window
tags:
  - mojo
level: intermediate
---

This example shows how to add a custom pop-up menu next to the *Display…* menu in the bottom bar of the Glyph Window.

{% image building-tools/observers/customGlyphWindowDisplayMenu.png %}

The items in the custom menu can be toggled on/off:

{% image building-tools/observers/customGlyphWindowDisplayMenu-2.png %}

{% showcode building-tools/observers/customGlyphWindowDisplayMenu.py %}

{% comment %}
> - [Adding a custom “Display…” menu (RoboFont Forum)](http://forum.robofont.com/topic/579/)
{: .seealso }
{% endcomment %}