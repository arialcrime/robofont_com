---
layout: page
title: Open selected component in a new window
tags:
  - observers
level: advanced
---

This example shows how to use an observer to perform a certain action when a key is pressed. In this case, if a component is selected, pressing `Shift C` will open a new glyph window for editing the selected component’s base glyph.

{% showcode building-tools/observers/openComponentInNewWindow.py %}
