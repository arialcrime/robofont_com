---
layout: page
title: Glyph Editor subview
tags:
  - observers
level: advanced
---

This example shows how to draw or add controls directly into the {% internallink 'workspace/glyph-editor' %}.

{% image building-tools/observers/addGlyphEditorSubviewExample.png %}

{% showcode building-tools/observers/addGlyphEditorSubviewExample.py %}

> - [Adding text to CurrentGlyphView (RoboFont Forum)](http://forum.robofont.com/topic/543/adding-text-to-currentglyphview/9)
{: .seealso }
