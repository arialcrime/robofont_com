---
layout: page
title: Draw reference glyph
tags:
  - observers
level: advanced
---

This example shows a very simple tool which displays the current glyph in a system font at the bottom of the glyph window.

This is how the code works:

- an observer is added to the `draw` event
- when the event is triggered, the tool uses the unicode value of the current glyph to get the corresponding character
- finally, if there is a character for the glyph, text properties (fill, font, font size) are set, and the text placed

{% showcode building-tools/observers/drawReferenceGlyph.py %}