---
layout: page
title: Introduction to observers
tags:
  - scripting
  - observers
level: advanced
draft: true
---

* Table of Contents
{:toc}

RoboFont is built around a software design model known as {% internallink 'toolspace/observers/the-observer-pattern' %}:

- a *dispatcher* sends out *notifications* when certain *events* happen
- *observers* can *subscribe* and *unsubcribe* from specific events

There are 3 different types of notifications in RoboFont:

font data
: [defcon notifications](http://ts-defcon.readthedocs.io/en/latest/concepts/notifications.html)

user interface
: {% internallink 'building-tools/api/custom-observers' text='RoboFont notifications' %}

user interaction
: [NSEvent notifications](http://developer.apple.com/documentation/appkit/nsevent)

## Observing changes to font data

The base font objects used by RoboFont are provided by [defcon]. Defcon includes its own [object observing system][defcon notifications] which sends out notifications for changes to the font data.

Here’s a basic example showing how to subscribe to changes to the width of a glyph:

{% showcode building-tools/observers/defconNotificationExample.py %}

All font objects in defcon have `addObserver` and `removeObserver` methods.

Each font object posts a set of individual notifications about specific kinds of changes. For example, a glyph has separate notifications for changes to width, contours, components, guidelines, anchors, etc.

Use `help()` to find out which notifications are posted by each object:

```python
from defcon import Contour
help(Contour)
```

```console
Help on class Contour in module defcon.objects.contour:

class Contour(defcon.objects.base.BaseObject)
 |  This object represents a contour and it contains a list of points.
 |
 |  **This object posts the following notifications:**
 |
 |  ===============================
 |  Name
 |  ===============================
 |  Contour.Changed
 |  Contour.WindingDirectionChanged
 |  Contour.PointsChanged
 |  Contour.IdentifierChanged
 |  ===============================
 |
 |  ...
```

[defcon]: http://github.com/robotools/defcon
[defcon notifications]: http://ts-defcon.readthedocs.io/en/latest/concepts/notifications.html

## Observing user interface actions

Font data is edited using the RoboFont interface, which consists of several windows, menus, panels, etc. RoboFont has its own object observing system which sends out notifications for user interface actions.

Here’s a basic example showing how to subscribe to when the current glyph changes:

{% showcode building-tools/observers/mojoNotificationExample.py %}

See {% internallink 'building-tools/api/custom-observers' %} for a list all user interface notifications available.

> Use the [Event Observer] extension to see all interface notifications while you work.
{: .tip }

[Event Observer]: http://github.com/roboDocs/eventObserverExtension

## Observing user interaction

...

> - [mojo.events.BaseEventTool](/documentation/building-tools/api/mojo/mojo-events/#mojo.events.BaseEventTool)
{: .seealso }
