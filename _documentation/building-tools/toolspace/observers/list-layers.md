---
layout: page
title: List font layers
tags:
  - observers
level: advanced
---

This example shows a simple floating window which displays a list of layers in the current font. Whenever the current font changes, the font name and layers list in the UI are updated automatically.

{% image building-tools/ListLayersTool.png %}

{% showcode building-tools/observers/listLayersTool.py %}
