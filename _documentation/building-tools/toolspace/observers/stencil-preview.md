---
layout: page
title: Stencil preview
tags:
  - observers
level: advanced
---

This example shows a simple stencil preview tool. The result is produced by subtracting the background layer from the foreground using {% internallink 'toolkit/boolean-glyphmath' %}.

{% image building-tools/observers/stencilPreview_0.png %}

{% showcode building-tools/observers/stencilPreview.py %}

{% image building-tools/observers/stencilPreview_1.png %}

