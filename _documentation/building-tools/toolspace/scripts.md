---
layout: page
title: Simple scripts
tags:
  - scripting
tree:
  - why-scripting
  - scripting-environment
  - get-font
  - scripts-font
  - get-glyph
  - scripts-glyph
  - scripts-interpolation
treeCanHide: true
level: beginner
---

A script is a plain text file with some code in it. You run the script in the {% internallink 'workspace/scripting-window' %}, the code is executed, something happens. Simple!

Even complex tools usually start off as small scripts. Once the main problem is solved, it is easy to add more features incrementally.

**As a general rule: start small, make it work, then add the bells and whistles.**

{% tree page.url levels=1 %}

> - [RoboFont Forum: Scripting](http://forum.robofont.com/tags/scripting)
{: .seealso }
