---
layout: page
title: Using DrawBot as a module
tags:
  - scripting
  - drawBot
level: intermediate
---

DrawBot can also be used as Python module, without the UI: we can import the `drawBot` module into a script and use it to generate images.

> This works outside of RoboFont too, if the DrawBot module is installed in the system’s Python.
{: .note }

The example below creates a new image and saves it as a PDF in the Desktop folder.

{% showcode building-tools/drawbot/drawBotAsModule.py %}
