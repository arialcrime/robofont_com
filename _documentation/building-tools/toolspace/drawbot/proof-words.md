---
layout: page
title: Proof words
tags:
  - scripting
  - drawBot
level: intermediate
---

This example shows how to set a line of text using glyphs from a UFO font.

{% image building-tools/drawbot/proofWords.png %}

In order to set text on the page, we need to convert a string of characters into a list of glyph names. This is done using the font’s *character map*, a dictionary mapping unicode values to glyph names.

As each glyph is drawn, the origin position is shifted horizontally for the next glyph.

{% showcode building-tools/drawbot/proofWords.py %}

> This example could be extended to support line breaks, to allow the text to reflow inside the page.
{: .tip }
