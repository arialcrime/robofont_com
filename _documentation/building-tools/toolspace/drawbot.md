---
layout: page
title: Scripts using DrawBot
tags:
  - scripting
  - drawBot
tree:
  - proof-character-set
  - proof-glyphs
  - proof-spacing
  - proof-features
  - drawbot-module
  - drawbot-view
treeCanHide: true
level: intermediate
---

* Table of Contents
{:toc}

While designing a typeface, it is important to proof glyphs in context in order to evaluate things like shape consistency, spacing, character set, etc.

Using {% glossary DrawBot %}, we can generate all kinds of proofs with code from within RoboFont.

## Using DrawBot in RoboFont

DrawBot is not embedded in RoboFont, but it can be installed as an [extension][DrawBot extension]. The extension makes the DrawBot environment available in RoboFont, with full access to the RoboFont {% glossary API %} – so you can use for example `CurrentFont()` or `CurrentGlyph()` directly in your DrawBot scripts.

{% internallink 'toolspace/drawbot/drawbot-module' %}
: The extension installs the `drawBot` Python module, which can be imported and used in your scripts to generate graphics without the UI.

{% internallink 'toolspace/drawbot/drawbot-view' %}
: DrawBot has a `DrawView` object which can be used together with vanilla – so you can create custom interfaces with DrawBot-based views.

## Making proofs with DrawBot

There are two ways to create a font proof using DrawBot:

Drawing directly from UFOs
: You can draw glyphs from the current font into a DrawBot canvas using the `drawGlyph()` function. This approach is useful when drawing single glyphs or small amounts of text.

Setting text with test fonts
: You can also use RoboFont’s built-in {% internallink 'how-tos/using-test-install' text='test install' %} function to generate and install a test version of the font, and then use DrawBot’s text setting functions to set some text with the font. This approach is recommended for setting body text and for proofing kerning, OpenType features, hinting, etc.

### Examples

{% comment %}
{% tree page.url levels=1 %}
{% endcomment %}

- {% internallink 'proof-glyphs' %}
- {% internallink 'proof-words' %}
- {% internallink 'proof-character-set' %}
- {% internallink 'proof-features' %}
- {% internallink 'proof-spacing' %}

[DrawBot]: http://www.drawbot.com/
[DrawBot extension]: http://github.com/typemytype/drawBotRoboFontExtension

{% comment %}
convert text string to glyph names
: https://forum.robofont.com/topic/447/strings-into-names
{% endcomment %}
