---
layout: page
title: Scripts to do things to glyphs
tags:
  - scripting
  - FontParts
level: intermediate
---

* Table of Contents
{:toc}

A collection of scripts to do things to glyphs.

## Move points by a random amount of units

{% showcode building-tools/glyphs/movePointsRandom.py %}

> In RoboFont 1.8, use `glyph.update()` instead of `glyph.changed()`.
{: .note }

## Add anchor to selected glyphs

{% showcode building-tools/glyphs/addAnchors.py %}

> In RoboFont 1.8, use `glyph.box()` instead of `glyph.bounds()`.
{: .note }

## Drawing inside a glyph with a pen

{% showcode building-tools/glyphs/drawWithPen.py %}

## Rasterizing a glyph

{% showcode building-tools/glyphs/rasterizeGlyph.py %}

{% comment %}

copy between layers
: http://forum.robofont.com/topic/524/copy-glyph-from-default-layer-to-another

get selected objects
: http://forum.robofont.com/topic/529/find-out-what-part-of-a-glyph-is-selected/8

{% endcomment %}



