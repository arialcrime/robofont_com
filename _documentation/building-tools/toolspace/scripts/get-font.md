---
layout: page
title: How to get a font
tags:
  - scripting
  - FontParts
level: beginner
---

* Table of Contents
{:toc}

FontParts provides an API to open and create fonts and to interact with fonts which are currently open in the font editor’s interface.

*This page gives an overview of the different ways to get a {% internallink 'building-tools/api/fontParts/rfont' text='font object' %}.*

{% comment %}
> - [fontParts.world](http://fontparts.readthedocs.io/en/latest/objectref/fontpartsworld/index.html)
{: .seealso }
{% endcomment %}

## CurrentFont

We can get the font which is already open in RoboFont:

```python
f = CurrentFont()
print(f)
```

```console
<Font MyFont Regular>
```

- If several fonts are open, `CurrentFont` will return the one at the top.
- If no font is open, `CurrentFont` will return `None`.

## OpenFont

We can also open an existing font from disk:

```python
f = OpenFont()
```

If `OpenFont` is used without any arguments, it will open a dialog so you can locate the font file in your machine.

If you already know the path to the font file, you can pass it to `OpenFont`:

```python
f = OpenFont("/Users/me/Desktop/myfont.ufo")
```

> To get get the full path to a file without having to type it, drag the file from the Finder and drop it into the Scripting Window.
{: .tip }

### Opening a font without the interface

We can also open a font in memory only, without opening a font window for it – this makes some operations faster, and can be useful when working with very large fonts or batch processing.

```python
f = OpenFont("/Users/me/Desktop/myfont.ufo", showInterface=False)
print(f)
```

```console
<Font MyFont Regular>
```

Because the font has no interface, there is no visual feedback to show the result of your actions. Don’t forget to save the font after you’re done.

> In RoboFont 1.8, use `showUI` instead of `showInterface`.
{: .note}

It is also possible to open a font without the interface, make some changes to it, and only then open it in the UI:

```python
f = OpenFont("/Users/me/Desktop/myfont.ufo", showInterface=False)
# do something
f.openInterface()
```

## NewFont

We can use `NewFont` to create a new empty font (without or without an interface):

```python
f = NewFont()
print(f)
```

```console
<Font None None>
```

```python
f = NewFont(showInterface=False)
```

The new font can be created with a family name and style name:

```python
f = NewFont(familyName='HelloWorld', styleName='Regular')
```

## AllFonts

If there are not one but several fonts open in the UI, we can iterate over all of them at once using `AllFonts`:

```python
for f in AllFonts():
    print(f)
```

```console
<Font None None>
<Font MyOtherFont Regular>
<Font MyFont Bold>
<Font MyFont Regular>
```

### Getting some fonts only

The output from the script above shows fonts from different families, and of different styles. `AllFonts` offers some handy methods to narrow down the list of fonts:

```python
for f in AllFonts().getFontsByFamilyName('MyFont'):
    print(f)
```

```console
<Font MyFont Bold>
<Font MyFont Regular>
```

```python
for f in AllFonts().getFontsByStyleName('Regular'):
    print(f)
```

```console
<Font MyOtherFont Regular>
<Font MyFont Regular>
```

### Sorting the list of open fonts

We can also ask `AllFonts` to sort the list of fonts by one or more font info attributes:

```python
for f in AllFonts(sortOptions=["openTypeOS2WeightClass"]):
    print(f)
```
