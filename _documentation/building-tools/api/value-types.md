---
layout: page
title: Common Value Types
level: intermediate
tags:
    - FontParts
---

* Table of Contents
{:toc}

## String
{: #type-string }

Unicode (unencoded) or string. Internally everything is a unicode string.

## Integer/Float
{: #type-int-float }

Integers and floats are interchangable in FontParts (unless the specification states that only one is allowed).

## Coordinate
{: #type-coordinate }

An immutable iterable containing two [Integer/Float](#type-int-float) representing:

1. `x`
2. `y`

## Angle
{: #type-angle }

*Define the angle specifications here. Direction, degrees, etc. This will always be a float.*

## Identifier
{: #type-identifier }

A [String](#type-string) following the [UFO identifier conventions](http://unifiedfontobject.org/versions/ufo3/conventions/#identifiers).

## Color
{: #type-color }

An immutable iterable containing four [Integer/Float](#type-int-float) representing:

1. `red`
2. `green`
3. `blue`
4. `alpha`

Values are from `0` to `1.0`.

## Transformation Matrix
{: #type-transformation }

An immutable iterable defining a 2×2 transformation plus offset (aka [Affine transform]). The default is `(1, 0, 0, 1, 0, 0)`.

[Affine transform]: http://en.wikipedia.org/wiki/Affine_transformation

## Immutable List
{: #type-tuple }

This must be an immutable, ordered iterable like a tuple.
