---
layout: page
title: Standard modules
level: beginner
draft: true
---

* Table of Contents
{:toc}

There are many standard modules. We'll give a brief of overview of some of these modules below. For reference, here is a [full list of standard Python modules].

[full list of standard Python modules]: http://docs.python.org/3.6/library/

### os

The `os` module contains many useful functions for dealing with the file system. Here are a few examples:

Lists all the content of a folder:

    print(os.listdir(path))

Recursively list all the contents of a folder:

    path = '/my_path/'
    print(os.walk(path))

Returns the path where the script is running (*current working directory*):

    print(os.getcwd())

Create a new folder:

    new_folder = u"/Users/yourusername/Desktop/test"
    os.mkdir(new_folder)

Rename a folder or file:

    new_folder2 = u"/Users/yourusername/Desktop/test2"
    os.rename(new_folder, new_folder2)

Remove a folder:

    os.rmdir(new_folder2)

Remove a file:

    os.remove(path)

### os.path

The `os.path` module have functions for manipulating file and folder names:

    import os.path
    file = '/my-file.png'
    path = '/my-folder/'

Check if a file or folder exists:

    print(os.path.exists(file))
    print(os.path.exists(path))

Returns only the file name, without the folder path where it's contained:

    print(os.path.basename(file))

Returns the containing folder name:

    print(os.path.dirname(file))

Splits the whole file path as folder and file name:

    print(os.path.split(file))

Splits the file name as file name and extension:

    print(os.path.splitext(file))

For more information, visit the [os.path] documentation.

[os.path]: http://docs.python.org/3.6/library/os.path

### string

The `string` module offers common string manipulation tools.

``` python
import string
print(string.lowercase)
print(string.uppercase)
print(string.digits)
```

``` console
>>> abcdefghijklmnopqrstuvwxyz
>>> ABCDEFGHIJKLMNOPQRSTUVWXYZ
>>> 0123456789
```

### random

Import `random` and `randint` functions from the `random` module:

    from random import random, randint

The `random()` function returns a pseudo-random floating point number between `0.0` and `1.0`:

    print("a random number between 0.0 and 1.0:")
    print(random())

The `randint()` function, on the other hand, returns a pseudo-random number between two integers:

    print("a random number between 0 and 4:")
    print(randint(0, 4))
    print("a random number between 10 and 20:")
    print(randint(10, 20))

Random numbers can be used for randomly selecting between two or more options:

    print("choose randomly between A and B, 6 times:")
    for i in range(6):
        if random() > 0.5:
            print("A")
        else:
            print("B")

### math

The `math` module has lots of, of course, math operations and constants which are needed for advanced calculations as trigonometry.

Import all functions contained in the `math` module:

    from math import *

    print(pow(9, 2))
    print(sqrt(81))
    print(floor(4.80))
    print(ceil(4.80))

<!--

[math](http://docs.python.org/3.6/library/math.html)
: mathematical functions and constants

[os.path](http://docs.python.org/3.6/library/os.path.html)
: manipulating folders and file paths

[plistlib](http://docs.python.org/3.6/library/plistlib.html)
: reading and writing .plist files

[random](http://docs.python.org/3.6/library/random.html)
: generating pseudo-random numbers

[shutil](http://docs.python.org/3.6/library/shutil.html)
: high-level operations on files and collections of files (copy and rename)

[subprocess](http://docs.python.org/3.6/library/subprocess.html)
: running commands as a new process

[datetime](http://docs.python.org/3.6/library/datetime.html)
: manipulating dates and times

[zipfile](http://docs.python.org/3.6/library/zipfile.html)
: working with zip files

[itertools](http://docs.python.org/3.6/library/itertools.html)
: creating iterators for efficient looping

> Some other modules which could be useful:
>
> - [csv](http://docs.python.org/3.6/library/csv.html)
> - [difflib](http://docs.python.org/3.6/library/difflib.html)
> - [doctest](http://docs.python.org/3.6/library/doctest.html) / [unittest](http://docs.python.org/3.6/library/unittest.html)
> - [ftplib](http://docs.python.org/3.6/library/ftplib.html)
> - [string](http://docs.python.org/3.6/library/string.html)
> - [xml.etree.ElementTree](http://docs.python.org/3.6/library/xml.etree.elementtree.html)
{: .seealso }

-->

- - -

> - [Brief Tour of the Standard Library]
{: .seealso }

[Brief Tour of the Standard Library]: http://docs.python.org/3.6/tutorial/stdlib.html
[Python Standard Library]: http://docs.python.org/3.6/library/index.html





