---
layout: page
title: Python in RoboFont
level: beginner
---

* Table of Contents
{:toc}

## Scripting environment

RoboFont comes with its own embedded Python interpreter, so you don’t need to install anything else. All modules from the [Python Standard Library] are also included.

> - RoboFont {{site.data.versions.roboFont}} comes with Python {{site.data.versions.python}}
> - RoboFont 1.8.4 comes with Python 2.7.10
{: .note}

[Python Standard Library]: http://docs.python.org/3.6/library/

RoboFont has a {% internallink "workspace/scripting-window" %} where you can write, run and browse through your Python scripts:

{% image python/scripting-window.png %}

There’s also a separate {% internallink "workspace/output-window" %} where the output from interactive tools is printed.

{% image python/scripting-environment.png %}

## Batteries included

RoboFont’s APIs are all [open and documented](../../toolkit/). Users are encouraged to write Python scripts to automate and customize the application to suit their needs.

All [FontParts] world objects – `CurrentFont`, `CurrentGlyph`, `AllFonts`, etc. – are available out of the box for use in your scripts.

You can also use [vanilla] to create interfaces, [mojo] to access RoboFont’s own objects, [fontTools] to edit font binaries…

> - {% internallink 'building-tools/toolkit/libraries' text='List of embedded libraries' %}
{: .seealso }

[FontParts]: ../../toolkit/fontparts/
[vanilla]: ../../toolspace/vanilla/
[mojo]: ../../toolspace/mojo/
[fontTools]: http://github.com/fonttools/fonttools/

## Extensions platform

{% image extensions/extension-icon.png %}

In addition to the Scripting environment where you can write and execute code,  RoboFont also offers an [Extensions infrastructure][Extensions] which makes it easy for developers to build and distribute extensions, and for users to install and update them.

{% internallink 'workspace/extension-builder' %}
: A dedicated window for building and editing extensions.

{% internallink 'extensions/building-extensions-with-script' text='ExtensionBundle' %}
: A Python object that lets you build extensions with a script.

{% internallink 'extensions/introducing-mechanic-2' text='Mechanic 2' %}
: An extension to install and manage RoboFont extensions.

[Extensions]: ../../../extensions/
