---
layout: page
title: Strings
level: beginner
---

* Table of Contents
{:toc}

Strings are immutable ordered collections of Unicode code points, and are used to handle textual data.

> Python 2 had two separate object types to represent text: `str` and `unicode`. In Python 3 these two types have been unified in a single unicode `str` object type.
{: .note }

## Strings syntax

Strings are typically delimited by single or double quotes (also in other programming languages):

```python
'this is a string'
"this is also a string"
```

The quote character used to delimit a string cannot be used in the string itself.

If a quote character is used as text, use the other one to delimit the string:

```python
"don't worry be happy"
'national "airquoting" competition'
```

### Escaping quote characters

It is also possible to *escape* the quote character, so it is used literally (as a quote character, not as a string delimiter). In Python strings, you can escape a character by adding a backslash before it:

```python
>>> "this is a \"string\" too"
```

```console
'this is a "string" too'
```

### Special characters

The backslash character `\` is also used to invoke special characters such as the newline character `\n` and the tabulator `\t`:

```python
>>> print("hello\n\tworld")
```

```console
hello
    world
```

### Multi-line strings

Python strings can also be delimited with triple single or double quotes to allow using tabs and newline characters literally:

```python
>>> txt = '''hello
...     python
... world'''
>>> print(txt)
```

```console
hello
    python
world
```

> - [Strings](http://docs.python.org/3/tutorial/introduction.html#strings)
> - [Text Sequence Type — `str`](http://docs.python.org/3.6/library/stdtypes.html#text-sequence-type-str)
{: .seealso }

## Operations with strings

Strings can perform some operations using mathematical operators, even though they are not numbers. This is called [operator overloading].

[operator overloading]: http://en.wikipedia.org/wiki/Operator_overloading

### Adding strings

Strings can be added to another string:

```python
>>> 'a' + 'b'
```

```console
ab
```

```python
>>> a = "a string"
>>> b = "another string"
>>> a + " " + b
```

```console
a string another string
```

### Multiplying strings

Strings can be multiplied by integers…

```python
>>> "spam " * 5
```

```console
spam spam spam spam spam
```

…but not by floats (even if they look like an integer):

```python
>>> "spam " * 5.0
```

```console
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can't multiply sequence by non-int of type 'float'
```

## String formatting

The string formatting syntax is a mini-language to create strings by combining fixed and variable parts.

### f-Strings

Python 3 introduces a new string formatting notation – “formatted string literals”, or f-strings – which improves on the previous two notations available in Python 2. The ‘old’ ways of formatting strings are still supported.

F-strings are prefixed with an `f` and contain curly braces with expressions that are replaced with their values at runtime.

```python
>>> a = 'eggs'
>>> f'spam spam spam {a} spam'
```

```console
'spam spam spam eggs spam'
```

```python
>>> f'spam spam spam {a} spam {a} {a} spam'
```

```console
'spam spam spam eggs spam eggs eggs spam'
```

```python
>>> b = 'bacon'
>>> f'spam spam spam {a} spam {b} spam spam {a} spam'
```

```console
'spam spam spam eggs spam bacon spam spam eggs spam'
```

Because f-strings are evaluated at runtime, you can put any Python expressions between the curly braces:

```python
>>> f"A day has {24 * 60 * 60} seconds."
```

```console
'A day has 86400 seconds.'
```

If you pass a dictionary value to an f-string, watch out for which quotation marks you use. If single quotes are used to delimit the f-string, use double quotes for the dictionary, and vice-versa.

```python
>>> D = {'name' : 'Brian'}
>>> f"A man called {D['name']}..."
```

```console
'A man called Brian...'
```

Using the same quote character for the f-string and the dictionary will not work:

```python
>>> f"A man called {D["name"]}..."
```

```console
  File "<stdin>", line 1
    f"A man called {D["name"]}..."
                          ^
SyntaxError: invalid syntax
```

> - [Formatted string literals](http://docs.python.org/3.6/reference/lexical_analysis.html#f-strings)
> - [Python 3's f-Strings: An Improved String Formatting Syntax (Guide)](http://realpython.com/python-f-strings/)
> - [PEP 498 – Literal String Interpolation](http://www.python.org/dev/peps/pep-0498/)
{: .seealso }

### String.format

The ‘old’ `string.format` syntax is still supported and useful in some cases. In this notation, the formatting string has one or more pairs of braces which work as empty ‘containers’, and a `format` method which receives matching arguments.

If the braces are empty, the arguments will be passed to the string in the order they are given:

```python
>>> '{}, {}, {}'.format('a', 'b', 'c')
```

```console
'a, b, c'
```

If the order of the values needs to be different from the order of the arguments, you can use an index inside the braces to match the arguments by position:

```python
>>> '{2}, {1}, {0}'.format('a', 'b', 'c')
```

```console
'c, b, a'
```

<!--
Unpacking a sequence argument with `*` also works:

```python
>>> '{2}, {1}, {0}'.format(*'abc')
```

```console
'c, b, a'
```
-->

It’s also possible to use names to match placeholders to keyword arguments:

```python
>>> 'position: ({x}, {y})'.format(x=120, y=-115)
```

```console
'position: (120, -115)'
```

The presentation of the value at each replacement field can be controlled with a special “format specifications” notation.

For example, we can control how many digits are displayed after the period when formatting floats:

```python
>>> from math import pi
>>> '{:.4f}'.format(pi)
```

```console
'3.1416'
```

Here’s how we can pad `0`s to a string with fixed width:

```python
>>> '{:07d}'.format(4)
```

```console
'0000004'
```

> - [Format examples](http://docs.python.org/3.6/library/string.html#formatexamples)
> - [How To Use String Formatters in Python 3](http://www.digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3)
{: .seealso }

<!--
### Formatting strings

This syntax uses the `%` character as a placeholder, and defines the corresponding parts to be replaced after the string:

```python
>>> "spam spam %s spam" % 'eggs'
```

```console
spam spam eggs spam
```

In the example above only one part is used when formatting the string. In case there are two or more parts, they need to be wrapped in a tuple:

```python
>>> "spam %s spam %s spam spam" % ('eggs', 'bacon')
```

```console
spam eggs spam bacon spam spam
```

Notice the `s` after the `%` in `%s`. This means that the formatting expression treats the values as a string. This is the most used option, but other ones are also available.

### Formatting numbers

The option `%f` gives a few options for formatting decimal numbers:

```python
>>> "hello %f" % 50.4625
```

```console
hello 50.462500
```

As we can see from the output, `%f` uses 6 decimal places by default.

We can shorten the amount of digits after the dot by a specified amount. Here we are limiting it to two digits only:

```python
>>> "hello %.2f" % 50.4625
```

```console
hello 50.46
```

By using a `+` sign before the decimal dot, we can indicate that a plus/minus sign should be prepended to the number to indicate its polarity:

```python
>>> "hello %+.2f" % 1.5
```

```console
hello +1.50
```

```python
>>> "hello %+.2f" % -3.0
```

```console
hello -3.00
```
-->

## String methods

String objects have several useful methods – let’s have a look at some of them.

### string.lower / string.upper

A string can return an all-lowercase or all-uppercase copy of itself:

```python
>>> 'Hello World'.upper()
```

```console
HELLO WORLD
```

```python
>>> 'Hello World'.lower()
```

```console
hello world
```

### string.startswith / string.endswith

These two methods let you to quickly check if a string starts or ends with a given sequence of characters:

```python
>>> 'abracadabra'.startswith('abra')
```

```console
True
```

```python
>>> 'abracadabra'.endswith('abra')
```

```console
True
```

### string.find / string.replace

Strings have a `find` method which returns the index of the first match in a string, or `-1` if no match is found.

```python
>>> 'abracadabra'.find('c')
```

```console
4
```

```python
>>> 'abracadabra'.find('x')
```

```console
-1
```

There’s also a `replace` method which replaces all occurrences of a substring with another string:

```python
>>> 'abracadabra'.replace('a', 'x')
```

```console
'xbrxcxdxbrx'
```

### string.split / string.strip

The string methods `split` and `strip` are often useful when working with data from text files.

`split` allows us to break a string at a given character into a list of substrings.

```python
>>> 'the quick brown fox jumps'.split()
```

```console
['the', 'quick', 'brown', 'fox', 'jumps']
```

```python
>>> '20,20,30,-30,360,240'.split(',')
```

```console
['20', '20', '30', '-30', '360', '240']
```

`strip` removes extra whitespace at the beginning and at the end of a string.

```python
>>> txt = '\t\tthe quick brown fox jumps    '
>>> txt.strip()
```

```console
'the quick brown fox jumps'
```

To clear whitespace characters only at the beginning or only at the end of a string, use `lstrip` or `rstrip` instead:

```python
>>> txt.lstrip()
```

```console
'the quick brown fox    '
```

```python
>>> txt.rstrip()
```

```console
'\t\tthe quick brown fox'
```

> - [String Methods](http://docs.python.org/3.6/library/stdtypes.html#string-methods)
{: .seealso }

