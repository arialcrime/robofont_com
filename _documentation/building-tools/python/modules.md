---
layout: page
title: Modules
level: beginner
---

* Table of Contents
{:toc}

## Introduction

A module is an *organizational unit* of Python code. Each module has its own namespace and contains arbitrary Python objects. Code from a module can be loaded into a script using the `import` statement.

## Many modules to choose from

{% internallink 'building-tools/python/standard-modules' %}
: Python comes with many modules included and ready to use in your scripts. These standard modules cover a wide range of tools, including file system access, numerical and mathematical functions, unicode text processing, dealing with dates and calendars, reading / writing several types of data formats, serving web pages, generating random numbers… and a lot more!

{% internallink 'building-tools/python/external-modules' %}
: Python also allows users to install and use third-party modules from multiple sources. The PyPI (Python Package Index) provides an extensive list of libraries developed by the Python community, and many more can be found on code sharing platforms like GitHub. Third-party packages can be installed using a package manager like `pip`, or manually using setup scripts or `.pth` files.

{% internallink 'building-tools/python/custom-modules' %}
: If you can’t find an existing module which suits your needs, Python allows you to create your own, and use it alongside all your other modules. That’s how many useful tools start their life: someone scratches his / her own itch, and ends up creating something that can benefit lots of other people too.

## Importing modules

Before you can use a module, you need to *import* it into your script. This is done with the `import` command:

```python
import myModule
```

After this line, the module becomes available in your script.

If you wish to see what’s inside the module, use the built-in function `dir()`:

```python
print(dir(myModule))
```

> This example assumes that the module is installed.
>
> If you wish to use an external or custom module and haven’t installed it yet, follow the steps described in the [installation instructions](../external-modules/#installing-modules).
{: .note }

## Reloading a module

Imported modules are compiled and cached, so they load faster when imported again.

While working on your own modules, you will be constantly making changes to their contents. But because the module is cached, these changes will **not** be imported into your scripts right away – unless you explicitly tell Python to reload the module:

```python
import myModule
from importlib import reload
reload(myModule)
```

> In Python 2, the `reload` function is available as a built-in and does not have to be imported.
{: .note }

## Accessing the contents of a module

Python offers different ways to import the contents of a module into a script.

### Import module, access objects using dot notation

We can simply import the module, and access its objects using the dot notation:

```python
import myModule
myModule.myFunction()
```

This method keeps the namespaces of the module and of the script in which it is imported separated. This avoids namespace collisions in case your main script contains objects with the same name:

```python
import myModule

def myFunction():
    pass

# module function
myModule.myFunction()

# local function
myFunction()
```

### Import everything from a module

The following syntax allows us to import the names of *all*\* objects contained in a module into the namespace of the current script:

```python
from myModule import *
```

> \* With the exception of names which start with an underscore.
{: .note }

> While this approach can save some typing during development and in interactive sessions, it is generally discouraged because it pollutes the main namespace and has a negative impact on code readability.
{: .warning }

### Selective import

Rather than importing everything from a module, we can be more selective and import only the objects we need to use in the current script:

```python
from myModule import myFunction, myOtherFunction, someVariable
```

### Import objects under a different name

To avoid namespace collisions with the current script, we can also import module objects under a different name using the `as` keyword:

```python
from myModule import myFunction as myOtherFunction

def myFunction():
    pass

# module function
myOtherFunction()

# local function
myFunction()
```

- - -

> In all variations of the `from … import …` syntax, only the names of the objects are imported – the module name is not.
{: .note }

> - [Python Glossary > Module](http://docs.python.org/3.6/glossary.html#term-module)
> - [The import system](http://docs.python.org/3.6/reference/import.html#importsystem)
{: .seealso }
