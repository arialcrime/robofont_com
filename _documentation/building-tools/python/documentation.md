---
layout: page
title: Documenting your code
level: beginner
draft: true
draft-hidden: true
---

* Table of Contents
{:toc}

## Documentation strings

Functions, modules, classes and methods may include a string literal with documentation.

```python
def myFunc():
    '''This function prints the string "Hi!".'''
    print("Hi!")
```

### reST style docstrings

```python
def myFunc(param1, param2):
    """
    This is a reST style docstring.

    :param param1: this is a first param
    :param param2: this is a second param
    :returns: this is a description of what is returned
    :raises keyError: raises an exception
    """
    return something
```

### Google style docstrings

```python
def myFunc(param1, param2):
    """
    This is an example of Google style docstring.

    Args:
        param1: This is the first param.
        param2: This is a second param.

    Returns:
        This is a description of what is returned.

    Raises:
        KeyError: Raises an exception.
    """
    return something
```

> - [PEP 257 – Docstring Conventions](http://www.python.org/dev/peps/pep-0257/)
{: .seealso }

## Annotations

```python
def myFunc(param1: str, param2: bool) -> list:
    # ...
    return []
```

## The help() built-in function

```python
help()
```

## Sphinx

...

```text
cd myFolder/docs
make html
```
