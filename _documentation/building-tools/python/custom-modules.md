---
layout: page
title: Custom modules
level: beginner
draft: true
---

Finally, if all the available modules are not enough, you can create your own modules and use them in RoboFont.

## Why create your own library?

- custom tools specific to your workflow
- combining existing libraries into something new and useful
- private information specific to your workflow, foundry etc.

## Creating your own library

...

> Make simple custom library example.
{: .todo }

## Installing your custom library

See instructions for {% internallink "toolkit/libraries#external-libraries" text="installing external libraries" %}.
