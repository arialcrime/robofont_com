---
layout: page
title: Glyph Math
tags:
  - interpolation
level: intermediate
draft: false
---

> Adapted from the original RoboFab documentation.
{: .note }

* Table of Contents
{:toc}

`RGlyph` objects have methods that allow the objects to behave a bit like variables in simple math. These methods return new glyph objects in which each coordinate in each contour is the product of the two glyphs.

> For addition, subtraction, intersection etc. with the **surface area** of the glyphs, see {% internallink "boolean-glyphmath" %}.
{: .seealso }

## Objects

All glyph math operations in have new, orphaned, objects as result. For instance a subtraction of two glyphs will result in a new glyph object, but it won’t be part of the font. If you want the result to be part of the font, you have to add it explicitly (see the [example at the bottom of this page](#combinations)). There are several reasons for this:

- The result might not even come from glyphs in the same font, i.e. you can subtract a glyph in one font from a glyph in another font. Where should the result live? You decide.
- You might not want the result to be part of your font when you’re using it for further calculations. So: results from glyph math operations are orphan glyphs that do not belong to any font.
- The results need to use floating point (`19.234943`) numbers for precision.Depending on your RoboFont preferences, the numbers might be rounded to integer (`19`).

If you want to add a glyph to a font, use the `appendGlyph` method:

```python
someNewGlyph = aFont.newGlyph("someNewGlyph")
someNewGlyph.appendGlyph(resultFromGlyphMath)

# Note that you have to set the width,
# appendGlyph does not automatically take the value.
someNewGlyph.width = restultFromGlyphMath.width
```

## Subtraction

Subtraction returns a new glyph object with contours which represent the difference between the two previous glyphs. As a glyph itself, it’s not much to look at. If you draw the result of a subtraction it will probably look like a crumpled outline.

```python
f = CurrentFont()
g1 = f["a"]
g2 = f["b"]
# suppose g1 and g2 have compatible point structures
myRelativeGlyph = g1 - g2
```

## Addition

Addition returns a new glyph object with contours which are the product of the two previous glyphs. If you just add two “normal” glyphs from a font (or multiple fonts for that matter) it will look odd. But you can also easily add a relative glyph (a result of subtracting one glyph from another), which effectively means you’re applying the difference between two glyphs to a third. And that can be a very useful action.

```python
# continue with myRelativeGlyph from the previous example
newglyph = f["x"] + myRelativeGlyph
```

## Multiplication

When a normal glyph is multiplied, it looks as if the glyph has been scaled. For instance multiplying a glyph by `0.5` scales the shapes by 50%.

```python
# continue with myRelativeGlyph from the previous example
newglyph = f["x"] + 0.25 * myRelativeGlyph
```

## Division

Divisions works just like multiplications, you just need to make sure not to divide by zero.

```python
# continue with myRelativeGlyph from the previous example
newglyph = f["x"] + myRelativeGlyph / 4
```

## Combinations

These examples are simple enough, but when you combine them the operations can become really powerful. You could recreate font interpolation using glyph math, or construct new networks of interpolations, additions, shifts, deltas that were impossible to build.

{% showcode building-tools/interpolation/funWithGlyphMath.py %}

> This example is meant to run with the [RoboFab DemoFont] as the current font.
{: .note }

[RoboFab DemoFont]: http://github.com/robotools/robofab/tree/master/Data/DemoFont.ufo

<!--
## Implementation limits

In objectsFL (for use in FontLab), only RGlyph has glyphmath operators implemented. The result of a glyphmath operation in FontLab is always an object from objectsRF. In ObjectsRF most objects have *, + and - implemented. But considering the operators are mainly used for Glyph stuff, the RGlyph object is a bit more kitted out with division as well.
-->
