---
layout: page
title: Embedded libraries
level: intermediate
---

RoboFont is built on top of several font-related Python libraries which are embedded in the application and can also be used in your scripts.

*This page gives an overview of all third-party Python libraries embedded in RoboFont, with a short description of what each library can be used for.*

[booleanOperations](http://github.com/typemytype/booleanOperations)
: performing boolean operations on bezier paths

[compositor](http://github.com/robotools/compositor)
: a basic layout engine for testing OpenType features

[cu2qu](http://github.com/googlei18n/cu2qu/)
: converting from cubic to quadratic bezier curves in UFO fonts

[defcon](http://github.com/robotools/defcon)
: provides UFO based objects for use in font editing applications

[defconAppKit](http://github.com/robotools/defconappkit)
: building blocks for creating UIs for font editing applications

[dialogKit](http://github.com/typesupply/dialogkit)
: creating simple modal UIs that work across different font editors

[extractor](http://github.com/robotools/extractor)
: extracting data from binary fonts into UFO objects

[feaPyFoFum](http://github.com/typesupply/feapyfofum)
: writing OpenType features dynamically

[fontMake](https://github.com/googlefonts/fontmake)
: compiling binary fonts (OpenType, TrueType) from sources

[fontMath](http://github.com/robotools/fontmath)
: fast interpolation of glyphs, fonts and other kinds of font data

[fontPens](http://github.com/robotools/fontpens)
: a collection of classes implementing the pen protocol

[fontTools](http://github.com/fonttools/fonttools)
: manipulating binary fonts (OpenType, TrueType, etc)

[glyphConstruction](http://github.com/typemytype/glyphConstruction)
: building glyphs from other glyphs

[glyphNameFormatter](http://github.com/LettError/glyphNameFormatter)
: generating glyph name lists from unicode data

[markdown](http://github.com/Python-Markdown/markdown)
: generating HTML from markdown sources

[mutatorMath](http://github.com/LettError/MutatorMath)
: piecewise linear interpolation in multiple dimensions

[ufoNormalizer](http://github.com/unified-font-object/ufoNormalizer)
: normalizing XML and other data inside UFOs

[ufo2fdk](http://github.com/robotools/ufo2fdk)
: generating OTFs from UFOs with the AFDKO

[ufo2svg](http://github.com/typesupply/ufo2svg)
: converting UFOs to SVG fonts

[ufoProcessor](http://github.com/LettError/ufoProcessor)
: generating instances from UFO masters and designspace

[vanilla](http://github.com/robotools/vanilla)
: a toolkit for creating windows, dialogs and other native macOS UI elements

[woffTools](http://github.com/typesupply/wofftools)
: verifying and examining WOFF files

- - -

For information about which version of each library is embedded in the current version of RoboFont, see the {% internallink "technical-specification#embedded-libraries" %}.

See {% internallink "how-tos/overriding-embedded-libraries" %} if you need to use a different version of a library – for example the latest development version, or a private fork.

All {% internallink "building-tools/python/standard-modules" text='standard Python modules' %} are also available in RoboFont.

[Python Standard Library]: http://docs.python.org/3.6/library/index.html

Locally installed third-party packages, such as {% internallink "building-tools/python/external-modules" %} or your own {% internallink "building-tools/python/custom-modules" %}, are accessible in RoboFont too. Make sure to install them for the same version of Python as the one embedded in RoboFont.
