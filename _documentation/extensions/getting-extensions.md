---
layout: page
title: Where and how to get extensions
tags:
  - extensions
level: beginner
---

* Table of Contents
{:toc}

Extensions are developed and distributed by multiple authors. Some extensions are free and open-source, while others are only available commercially.

- **open-source extensions** can be found via the Mechanic website
- **commercial extensions** can be found in the Extension Store

If none of the available extensions suits your needs, you can also {% internallink "creating-extensions" text='create your own extensions' %} with Python (or hire someone to do it for you).

## Getting extensions with Mechanic 2

All types of extensions can be downloaded, installed and managed using the [Mechanic 2 extension].

> - {% internallink 'installing-extensions-mechanic' %}
{: .seealso }

## Getting extensions manually

Extensions can also be downloaded and installed manually (without using Mechanic).

Open-source extensions
: On the [Mechanic 2 website], follow the link to the extension’s repository, and download the zip file to your computer.

Commercial extensions
: After the purchase process in the [Extension Store] is completed, you will be sent an email with the download link to a zip file.

To install the extension in RoboFont, simply double-click the `.roboFontExt` file contained in the zip.

> - [Installing extensions manually](../installing-extensions)
{: .seealso }

[Extension Store]: http://extensionstore.robofont.com/
[Mechanic 2 website]: http://robofontmechanic.com/
[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
