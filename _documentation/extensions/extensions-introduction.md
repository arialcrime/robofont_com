---
layout: page
title: Introduction to Extensions
treeTitle: Introduction
tags:
  - extensions
level: beginner
---

* Table of Contents
{:toc}

## What are extensions?

Extensions are additional software packages which add specialized tools and functionality to RoboFont. They are an essential part of the RoboFont toolkit.

Technically speaking, a RoboFont extension is a macOS package with a `.roboFontExt` file extension, built according to the {% internallink "building-tools/extensions/extension-file-spec" %}.

Extensions can be {% internallink 'installing-extensions' text='installed manually' %} (it’s as easy as double-clicking a file) or {% internallink 'installing-extensions-mechanic' text='using Mechanic' %} (an extension to manage RoboFont extensions).

Once installed, extensions integrate seamlessly into the RoboFont interface, adding themselves to existing menus or toolbars.

## Why extensions?

**RoboFont does not try to do everything a font editor could do.** We think this approach is problematic and leads to:

- rigid tools, which force users to doing things one way
- cluttered interface, with too many options which confuse users
- bloated, monolithic software which is harder to maintain

**RoboFont takes a modular approach:** it aims at being a solid and extensible core application, which provides all the basic functionality required for type design and font production work – while also serving as a foundation on top of which others can build their own specialized tools.

**Extensions provide users with options.** Instead of a having a single tool which tries to solve all problems, RoboFont users have access to a flexible infra-structure which enables an *ecosystem of specialized tools* – built by multiple developers with different needs and different ideas.

> - {% internallink "introduction/design-principles" %}
{: .seealso }
