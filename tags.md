---
layout: page
title: Tags
excludeSearch: true
excludeNavigation: true
---

<div class="sorting-controls">
  <label>
    <span class="reset">Reset</span>
  </label>
</div>

{% assign items = "" | split:"|" %}
{% for page in site.documentation %}
    {% unless page.draft-hidden %}
        {% assign items = items | push: page %}
    {% endunless %}
{% endfor %}
{% for page in site.pages %}
    {% unless page.draft-hidden %}
        {% assign items = items | push: page %}
    {% endunless %}
{% endfor %}
{% for page in site.posts %}
    {% unless page.draft-hidden %}
        {% assign items = items | push: page %}
    {% endunless %}
{% endfor %}
{% assign items = items | sort:"title" %}

{% assign sections = "introduction workspace how-tos building-tools" | split: " " %}

<div class="sorter-checkboxes">
    <fieldset>
        {% for section in sections %}
            <label class="sorter-checkbox filter">
                <input type="checkbox" id="section-{{ section | slugify }}" value="section-{{ section | slugify }}"><span>{{ section }}</span>
            </label>
        {% endfor %}
    </fieldset>
    {% assign tags = "" | split:"|" %}
    {% for item in items %}
        {% for tag in item.tags %}
            {% assign tags = tags | push: tag %}
        {% endfor %}
    {% endfor %}
    {% assign tags = tags | uniq | sort %}
    <fieldset>
    {% for tag in tags %}
        <label class="sorter-checkbox filter">
            <input type="checkbox" id="{{ tag | slugify }}" value="{{ tag | slugify }}"><span style="border: 1px solid hsl({{ forloop.index | times: 300 | divided_by: forloop.length }}, 100%, 50%); background-color:hsla({{ forloop.index | times: 300 | divided_by: forloop.length }}, 100%, 50%, 0.2);">{{ tag }}</span>
        </label>
    {% endfor %}
    </fieldset>
</div>

<div class="sorter">
{% for item in items %}
    {% assign parts = item.url | split:"/" %}
    {% assign partsCount = parts | size %}
    {% assign section = "" %}
    {% case parts[1] %}
    {% when "documentation" %}
        {% if partsCount > 2 %}
            {% assign section = parts[2] %}
        {% endif %}
    {% when "news" %}
        {% assign section = "news" %}
    {% endcase %}
    {% assign tagCount = item.tags | size %}
    {% if tagCount > 0 %}
    {% assign slugifiedTags = "" | split:"|" %}
    {% for tag in item.tags %}
        {% assign slugifiedTag = tag | slugify %}
        {% assign slugifiedTags = slugifiedTags | push: slugifiedTag %}
    {% endfor %}
    <div class="sorter-item" data-title="{{ item.title }}" data-groups='["{{ slugifiedTags | join: '", "' }}"]' data-section='section-{{ section }}'>
        <div class="sorter-item-color{% unless item.tags %} item-no-tags{% endunless %}">
        {% for tag in tags %}
            {% if item.tags contains tag %}
                <div style="background-color:hsla({{ forloop.index | times: 300 | divided_by: forloop.length }}, 100%, 50%, 0.2);"></div>
            {% endif %}
        {% endfor %}
        </div>
        <div class="sorter-item-inner">
            <div class="sorter-item-title">
                <a href="{{ site.baseurl }}{{ item.url }}">{{ item.title }}</a>
            </div>
            <div class="sorter-item-info">
                <div class="sorter-item-subinfo">
                    <div class="sorter-item-section">
                        {{ section }}
                    </div>
                    <div class="sorter-item-tags">
                        {% for tag in item.tags %}
                            <div class="tag">{{ tag }}</div>
                        {% endfor %}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {% endif %}
{% endfor %}
</div>

<script src="{{ site.baseurl }}/js/shuffle.min.js"></script>
<script src="{{ site.baseurl }}/js/filter-helpers.js"></script>

<script type="text/javascript">

allSections = [ {% for section in sections %}"section-{{ section | slugify }}"{% unless forloop.last %}, {% endunless %}{% endfor %}]

function tagsFilterItems() {
    tags = []
    sections = []
    $(".sorter-checkbox :checked").each(function () {
        value = $(this).val()
        if ( allSections.indexOf(value) >= 0 ) {
            sections.push(value)
        }
        else {
            tags.push(value)
        }
    })
    shuffleInstance.filter(function (element) {
        groups = JSON.parse(element.getAttribute('data-groups'))
        category = element.getAttribute("data-section")
        if (sections.length > 0 && sections.indexOf(category) == -1) {
            return false
        }
        if (tags.length > 0) {
            for (i = 0; i < groups.length; i++) {
                item = groups[i]
                if ( tags.indexOf(item) >= 0) {
                    return true
                }
            }
        }
        else {
            return true
        }
        return false
    })
    return tags.concat(sections)
}

setFilterItems(tagsFilterItems)

</script>

> - [RoboFont Forum > Tags](http://forum.robofont.com/tags)
{: .seealso }
