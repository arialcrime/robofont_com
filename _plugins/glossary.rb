require_relative "helpers"


module Jekyll
  class Glossary < Liquid::Tag

    include HelpersTools

    def initialize(tag_name, text, tokens)
        super
        @key = text.strip()
    end

    def render(context)
        site = context.registers[:site]
        glossaryData = site.data["glossary"]
        if glossaryData.key?(@key)
            baseurl = context.registers[:site].baseurl
            converter = site.find_converter_instance(::Jekyll::Converters::Markdown)
            # convert to html from markdown
            info = converter.convert(glossaryData[@key].to_s)
            # remove all hard returns
            info = info.tr("\n","").tr("\r","")
            # inline html with <p> tags does not work fine
            info = info.gsub('</p><p>', '<br><br>').gsub('<p>', '').gsub('</p>', '')
            keySlug = Utils.slugify(@key)
            "<span class=\"info\">#{@key}<span class=\"tip\">#{info}</span><a class=\"hidden-link\" href=\"#{baseurl}/documentation/glossary/##{keySlug}\"></a></span>"
        else
            pinkWarning("Glossary not found: '#{@key}' in #{context.registers[:page]["url"]}")
            # return
            @key
        end
    end
  end
end

Liquid::Template.register_tag('glossary', Jekyll::Glossary)