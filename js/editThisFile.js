altWasDown = false
$(document).keydown(function(e) {
    if(e.keyCode == 69) {  // when e is pressed
        $(".control-key-hide").show()
        altWasDown = true
    }
});

$(document).keyup(function(e) {
    if(altWasDown) {
        $(".control-key-hide").hide()
        altWasDown = false
    }
});