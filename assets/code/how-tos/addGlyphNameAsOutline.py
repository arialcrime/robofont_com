'''
Add glyph name to selected glyph(s) -- as outlines.
Used for testing the results for calt substitution.

Many thanks to Frederik Berlaen, whose code this basically is.

'''

from compositor import Font as CompositorFont
from fontTools.pens.transformPen import TransformPen
from fontTools.misc.transform import Transform
from math import cos, radians

f = CurrentFont()
s = f.selection
italicAngle = f.info.italicAngle

shortNames = {
    '.alt': '.a',
    '.connect': '.c',
    '.begin': '.b',
    '.end': '.e',
    '.fina': '.f',
}


# load the binary font
sourceFont = CompositorFont("/Users/fgriessh/Library/Fonts/SCP/SourceCodePro-Regular.otf")


for gName in s:

    text = gName
    for suffix, shortsuffix in shortNames.items():
        text = text.replace(suffix, shortsuffix)

    # get the glyph
    destGlyph = f[gName]

    # process the text to glyph records, optionally enable some features
    glyphRecords = sourceFont.process(stringOrGlyphList=text)

    # setup a transform object
    t = Transform()

    # calculate width of glyph name text string
    textWidth = sum([glyphRecord.advanceWidth for glyphRecord in glyphRecords])

    # set a scale
    scaleFactor = .05

    # center the text in glyph metrics, and move it up on the italic angle to the desired y-position
    yShift = 300
    xShift = destGlyph.width/2-textWidth/2*scaleFactor + yShift * cos(radians(italicAngle))

    t = t.translate(xShift, yShift)
    t = t.scale(scaleFactor)

    for glyphRecord in glyphRecords:
        # set the placement of the glyph
        t = t.translate(glyphRecord.xPlacement, glyphRecord.yPlacement)

        # put the dest glyph pen into the transform pen
        pen = TransformPen(destGlyph.getPen(), t)

        # get the source glyph from the binary font
        sourceGlyph = sourceFont[glyphRecord.glyphName]

        # draw the binary glyph into the pen
        sourceGlyph.draw(pen)

        # reset the placement
        t = t.translate(-glyphRecord.xPlacement, -glyphRecord.yPlacement)
        # set the advance width
        t = t.translate(glyphRecord.advanceWidth, glyphRecord.advanceHeight)

        # some kerning -- do not need this since I am using a monospaced font.
        # t = t.translate(glyphRecord.xAdvance, glyphRecord.yAdvance)