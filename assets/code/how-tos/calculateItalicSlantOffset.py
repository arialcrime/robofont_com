import math
f = CurrentFont()
italicSlantOffset = math.tan(f.info.italicAngle * math.pi / 180) * (f.info.xHeight * 0.5)
print(round(italicSlantOffset))