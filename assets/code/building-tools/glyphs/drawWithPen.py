'''Draw inside a glyph using a pen.'''

# get the current font
font = CurrentFont()

# get a glyph
glyph1 = font['a']

# empty the glyph
glyph1.clear()

# get a pen to draw inside the glyph
pen = glyph1.getPen()

# draw a closed Bezier path
pen.moveTo((0, 0))
pen.lineTo((0, 200))
pen.curveTo((50, 150), (150, 150), (200, 200))
pen.lineTo((200, 0))
pen.closePath()

# get another glyph
glyph2 = font['b']
glyph2.clear()

# draw an open Bezier path
pen = glyph2.getPen()
pen.moveTo((0, 0))
pen.lineTo((200, 100))
pen.lineTo((0, 200))
pen.lineTo((200, 300))
pen.endPath()