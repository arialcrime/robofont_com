'''
Rasterize a glyph with a given grid size.
Transforms an outline shape into an array of boolean pixels.

converted from robofab.objects.objectsBase.BaseGlyph

'''

def rasterize(glyph, cellSize=50, xMin=None, yMin=None, xMax=None, yMax=None):
    """
    Slice the glyph into a grid based on the given cell size.

    Returns a list of lists containing bool values that indicate the
    black (True) or white (False) value of that particular cell.
    These lists are arranged from top to bottom of the glyph and proceed
    from left to right.

    Warning: This is an expensive operation!

    """

    if xMin is None or yMin is None or xMax is None or yMax is None:
        _xMin, _yMin, _xMax, _yMax = glyph.bounds
        if xMin is None:
            xMin = _xMin
        if yMin is None:
            yMin = _yMin
        if xMax is None:
            xMax = _xMax
        if yMax is None:
            yMax = _yMax

    hitXMax = False
    hitYMin = False
    xSlice = 0
    ySlice = 0
    halfCellSize = cellSize / 2.0
    bitmap = []

    while not hitYMin:
        bitmap.append([])
        yScan = -(ySlice * cellSize) + yMax - halfCellSize
        if yScan < yMin:
            hitYMin = True
        while not hitXMax:
            xScan = (xSlice * cellSize) + xMin - halfCellSize
            if xScan > xMax:
                hitXMax = True
            test = glyph.pointInside((xScan, yScan))
            if test:
                bitmap[-1].append(True)
            else:
                bitmap[-1].append(False)
            xSlice = xSlice + 1
        hitXMax = False
        xSlice = 0
        ySlice = ySlice + 1

    return bitmap

# get the current font
font = CurrentFont()

# get the current glyph
glyph = CurrentGlyph()

# define the rasterization area
cellSize = 40
xMin = 0
yMin = font.info.descender
xMax = glyph.width
yMax = font.info.ascender

# rasterize the current glyph
bitmap = rasterize(glyph, cellSize, xMin, yMin, xMax, yMax)

# preview the output as text
for line in bitmap:
    for bit in line:
        if bit:
            print('O ', end='')
        else:
            print('- ', end='')
    print()