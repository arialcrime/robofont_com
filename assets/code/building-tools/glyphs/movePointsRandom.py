'''Move every point in the current glyph by a random amount of units.'''

from random import randint

# get the current glyph
glyph = CurrentGlyph()

# set undo state
glyph.prepareUndo('move points randomly')

# loop over all contours in glyph
for contour in glyph:
    # loop over all points in contour
    for point in contour.points:
        # move point by a random distance
        point.x += randint(-20, 20)
        point.y += randint(-20, 20)

# update glyph
glyph.changed()

# restore undo state
glyph.performUndo()