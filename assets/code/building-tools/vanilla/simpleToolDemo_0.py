from vanilla import FloatingWindow, Button

class ToolDemo:

    def __init__(self):
        # create a floating window
        self.w = FloatingWindow((123, 70), "myTool")

        # define some layout variables
        x, y = 10, 10
        padding = 10
        buttonHeight = 20

        # add button for printing selected glyphs
        self.w.printButton = Button(
                (x, y, -padding, buttonHeight), # position & size
                "print") # button label

        # increase the y-position
        y += buttonHeight + padding

        # add button for painting the selected glyphs
        self.w.paintButton = Button(
                (x, y, -padding, buttonHeight), # position & size
                "paint") # button label

        # done - open the window
        self.w.open()

# open the dialog
ToolDemo()