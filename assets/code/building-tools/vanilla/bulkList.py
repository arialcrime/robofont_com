import os
from AppKit import NSFilenamesPboardType, NSDragOperationCopy
from vanilla import Window, List, Button


action = 'Do Thing'


class doThing():

    def __init__(self, path):

        f = OpenFont(path, showInterface=False)

        # do something here
        print(f.info.familyName, f.info.styleName)

        # f.save()
        f.close()


class getListofFiles():

    supportedFontFileFormats = ['.ufo']

    def __init__(self):

        L = 0  # left
        T = 0  # top
        W = 600 # width
        H = 300  # height
        p = 10 # padding
        buttonHeight = 20

        title = action + ' To These Files'
        self.w = Window((W, H), title, minSize=(W/3, H/3))
        self.w.fileList = List(
            (L, T, -0, -(p * 2 + buttonHeight)),
            [],
            columnDescriptions=[
                {"title": "?", "width": 25}, # status
                {"title": "Files", "allowsSorting": True}], # files
            showColumnTitles=False,
            allowsMultipleSelection=True,
            enableDelete=True,
            otherApplicationDropSettings = dict(
                type=NSFilenamesPboardType,
                operation=NSDragOperationCopy,
                callback=self.dropCallback),
            )
        self.w.open()

        buttonText = action
        buttonPos = (p, -(p + buttonHeight), -p, buttonHeight)
        self.w.button = Button(buttonPos, buttonText, callback=self.buttonCallback)
        self.w.open()

    def buttonCallback(self, sender):
        for path in self.w.fileList:
            doThing(path['Files'])
            path['?'] = '✅'

    def dropCallback(self, sender, dropInfo):
        isProposal = dropInfo["isProposal"]
        existingPaths = sender.get()
        paths = dropInfo["data"]
        paths = [path for path in paths if path not in existingPaths]
        paths = [path for path in paths if os.path.splitext(path)[-1].lower() in self.supportedFontFileFormats or os.path.isdir(path)]
        if not paths:
            return False
        if not isProposal:
            for path in paths:
                item = {}
                item['?'] = '⚪️'
                item['Files'] = path
                self.w.fileList.append(item)
        return True


try:
    getListofFiles()
except:
    message = 'well shit'
    os.system(f'say "{message}"')