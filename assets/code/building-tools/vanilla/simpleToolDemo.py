from vanilla import FloatingWindow, Button
from mojo.UI import Message
from mojo.roboFont import OpenWindow

class ToolDemo(object):

    def __init__(self):
        '''Initialize the dialog.'''

        self.w = FloatingWindow((123, 70), "myTool")

        x = y = padding = 10
        buttonHeight = 20

        self.w.printButton = Button(
                (x, y, -padding, buttonHeight),
                "print",
                callback=self.printGlyphsCallback)

        y += buttonHeight + padding

        self.w.paintButton = Button(
                (x, y, -padding, buttonHeight),
                "paint",
                callback=self.paintGlyphsCallback)

        self.w.open()

    def printGlyphsCallback(self, sender):
        '''Print the names of all selected glyphs.'''

        font = CurrentFont()

        if font is None:
            Message('no font open', title='myTool', informativeText='Please open a font first!')
            return

        if not len(font.selectedGlyphNames):
            Message('no glyphs selected', title='myTool', informativeText='Please select one or more glyphs first!')
            return

        for glyphName in font.selectedGlyphNames:
            print(glyphName)
        print()

    def paintGlyphsCallback(self, sender):
        '''Paint all selected glyphs.'''

        font = CurrentFont()

        if font is None:
            Message('no font open', title='myTool', informativeText='Please open a font first!')
            return

        if not len(font.selectedGlyphNames):
            Message('no glyphs selected', title='myTool', informativeText='Please select one or more glyphs first!')
            return

        for glyphName in font.selectedGlyphNames:
            font[glyphName].prepareUndo('set mark color')
            font[glyphName].markColor = 1, 0, 0, 0.35
            font[glyphName].performUndo()


if __name__ == '__main__':

    OpenWindow(ToolDemo)