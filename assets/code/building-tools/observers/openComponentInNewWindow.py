from mojo.UI import OpenGlyphWindow
from mojo.events import addObserver

class BaseGlyphJumper(object):

    def __init__(self):
        # add observer for 'key down' events
        addObserver(self, "keyDown", "keyDown")

    def keyDown(self, notification):

        # get data about the event
        event = notification["event"]

        # get the pressed character(s)
        char = event.characters()

        # do nothing if the pressed character is not 'C'
        if not char == "C":
            return

        # get the current glyph
        glyph = notification["glyph"]

        # get the parent font
        font = glyph.font

        # loop over all components the glyph
        for component in glyph.components:

            # skip components which are not selected
            if not component.selected:
                continue

            # skip if the component’s base glyph is not in the font
            if not component.baseGlyph in font:
                continue

            # get the component’s base glyph
            baseGlyph = font[component.baseGlyph]

            # open the base glyph in a new glyph window
            OpenGlyphWindow(baseGlyph, newWindow=True)

BaseGlyphJumper()
