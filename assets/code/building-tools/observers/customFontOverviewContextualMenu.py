from mojo.events import addObserver

class CustomFontOverviewContextualMenu(object):

   def __init__(self):
       addObserver(self, "fontOverviewAdditionContextualMenuItems", "fontOverviewAdditionContextualMenuItems")

   def fontOverviewAdditionContextualMenuItems(self, notification):
       myMenuItems = [
           ("option 1", self.option1Callback),
           ("option 2", self.option2Callback),
           ("submenu", [("option 3", self.option3Callback)])
       ]
       notification["additionContextualMenuItems"].extend(myMenuItems)

   def option1Callback(self, sender):
       print("option 1 selected")

   def option2Callback(self, sender):
       print("option 2 selected")

   def option3Callback(self, sender):
       print("option 3 selected")

CustomFontOverviewContextualMenu()