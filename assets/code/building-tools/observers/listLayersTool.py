from vanilla import FloatingWindow, TextBox, List
from mojo.events import addObserver, removeObserver
from defconAppKit.windows.baseWindow import BaseWindowController

class ListLayersTool(BaseWindowController):

    def __init__(self):
        self.w = FloatingWindow((200, 200), "layers")
        self.w.fontName = TextBox((10, 10, -10, 20), '')
        self.w.list = List((10, 40, -10, -10), [])
        addObserver(self, 'fontBecameCurrentCallback', "fontBecameCurrent")
        addObserver(self, 'fontDidCloseCallback', "fontDidClose")
        self.setUpBaseWindowBehavior()
        self.setFont(CurrentFont())
        self.w.open()

    def windowCloseCallback(self, sender):
        removeObserver(self, 'fontBecameCurrent')
        removeObserver(self, 'fontDidClose')
        super(ListLayersTool, self).windowCloseCallback(sender)

    def fontBecameCurrentCallback(self, notification):
        font = notification['font']
        self.setFont(font)

    def fontDidCloseCallback(self, notitication):
        self.setFont(CurrentFont())

    def setFont(self, font):
        if font is None:
            self.w.list.set([])
            self.w.fontName.set("")
        else:
            self.w.list.set(font.layerOrder)
            fontName = '%s %s' % (font.info.familyName, font.info.styleName)
            self.w.fontName.set(fontName)

ListLayersTool()