from vanilla import FloatingWindow, PopUpButton
from mojo.UI import CurrentGlyphWindow
from mojo.events import addObserver, removeObserver

class CustomGlyphWindowDisplayMenu:

    def __init__(self, menuTitle, menuItems, width=85):
        self.title = menuTitle
        self.items = menuItems
        self.width = width
        addObserver(self, "glyphWindowDidOpenObserver", "glyphWindowDidOpen")

    @property
    def window(self):
        return CurrentGlyphWindow()

    @property
    def bar(self):
        if not self.window:
            return
        return self.window.getGlyphStatusBar()

    def glyphWindowDidOpenObserver(self, info):
        if not self.bar:
            return

        if hasattr(self.bar, "menuButton"):
            del self.bar.menuButton

        menuItems = list(self.items.keys())
        menuItems.insert(0, self.title)
        self.bar.menuButton = PopUpButton((120, 0, self.width, 16), menuItems, sizeStyle="small", callback=self.callback)
        self.bar.menuButton.getNSPopUpButton().setPullsDown_(True)
        self.bar.menuButton.getNSPopUpButton().setBordered_(False)

        for i, menuItem in enumerate(self.bar.menuButton.getNSPopUpButton().itemArray()[1:]):
            value = list(self.items.values())[i-1]
            menuItem.setState_(value)

    def callback(self, sender):
        item = sender.getNSPopUpButton().selectedItem()
        item.setState_(not item.state())
        selection = []
        for i, menuItem in enumerate(self.bar.menuButton.getNSPopUpButton().itemArray()[1:]):
            if menuItem.state():
                selection.append(i)
        print(selection)

CustomGlyphWindowDisplayMenu('hello world', {'A': True, 'B': True, 'C': False})