from mojo.events import addObserver

class LockLayerWidths:

    glyph = None
    verbose = False

    def __init__(self):
        # subscribe to RoboFont notification `currentGlyphChanged`
        addObserver(self, "currentGlyphChangedObserver", "currentGlyphChanged")
        # Subscribe to defcon notification
        self.subscribeGlyph(CurrentGlyph())

    def __del__(self):
        self.unsubscribeGlyph()

    def subscribeGlyph(self, glyph):
        '''Subscribe to defcon notification `Glyph.WidthChanged`.'''
        self.unsubscribeGlyph()
        if glyph is not None:
            self.glyph = glyph
            self.glyph.addObserver(self, "widthChangedObserver", "Glyph.WidthChanged")

    def unsubscribeGlyph(self):
        '''Unsubscribe from defcon notification `Glyph.WidthChanged`.'''
        if self.glyph is not None:
            self.glyph.removeObserver(self, "Glyph.WidthChanged")
            self.glyph = None

    def currentGlyphChangedObserver(self, notification):
        '''Observer for RoboFont notification `currentGlyphChanged`.'''
        self.subscribeGlyph(notification["glyph"])

    def widthChangedObserver(self,  notification):
        '''Observer for defcon notification `Glyph.WidthChanged`.'''

        # get defcon notification
        glyph = notification.object
        width = notification.data['newValue']

        # wrap RGlyph around defcon glyph
        glyph = RGlyph(glyph)

        # copy current glyph width to other layers
        for layerName in glyph.font.layerOrder:
            if layerName == glyph.layer.name:
                continue
            g = glyph.getLayer(layerName)
            if self.verbose:
                print('copying width from %s.%s to %s.%s...' % (glyph.name, glyph.layer.name, glyph.name, layerName))
            g.width = width

        if self.verbose:
            print()

LockLayerWidths()