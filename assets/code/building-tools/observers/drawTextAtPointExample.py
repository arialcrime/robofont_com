import AppKit
from mojo.UI import CurrentGlyphWindow
from mojo.events import addObserver

class DrawTextAtPointExample:

    def __init__(self):
        addObserver(self, "drawText", "draw")

    def drawText(self, notification):
        glyphWindow = CurrentGlyphWindow()

        if not glyphWindow:
            return

        glyphView = glyphWindow.getGlyphView()

        textAttributes = {
            AppKit.NSFontAttributeName: AppKit.NSFont.systemFontOfSize_(11),
        }

        pts = [(200, 200),(300, 300), (360, 120), (450, 200)]

        for pt in pts:
            glyphView.drawTextAtPoint(
                'my custom text',
                textAttributes,
                pt,
                yOffset=0,
                drawBackground=True,
                centerX=True,
                centerY=True,
                roundBackground=False,
                backgroundXAdd=7,
                backgroundYadd=2)

DrawTextAtPointExample()