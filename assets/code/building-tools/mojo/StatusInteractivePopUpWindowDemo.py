from mojo.UI import StatusInteractivePopUpWindow
from vanilla import EditText, Button

class MyPopUpWindow(object):

    def __init__(self):
        self.w = StatusInteractivePopUpWindow((200, 80))
        self.w.value = EditText((10, 10, -10, 25), '123456789')
        self.w.button = Button((10, 45, -10, 25), 'hey', callback=self.buttonCallback)
        self.w.open()
        self.w.center()

    def buttonCallback(self, sender):
        print(self.w.value.get())

MyPopUpWindow()