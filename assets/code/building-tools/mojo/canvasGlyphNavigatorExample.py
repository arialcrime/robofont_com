from mojo.canvas import Canvas
from mojo.drawingTools import *
import mojo.drawingTools as mojoDrawingTools
from vanilla import Window, Slider
from mojo.events import addObserver,removeObserver
from defconAppKit.windows.baseWindow import BaseWindowController
import AppKit

class ExampleWindow(BaseWindowController):

    def __init__(self):
        self.size = 50
        self.offset = 0, 0

        self.w = Window((400, 400), minSize=(200, 200))
        self.w.slider = Slider((10, 5, -10, 22), value=self.size, callback=self.sliderCallback)
        self.w.canvas = Canvas((0, 30, -0, -0), canvasSize=(1000, 1000),delegate=self, hasHorizontalScroller=True, hasVerticalScroller=True)

        self.setUpBaseWindowBehavior()
        self.w.open()

        addObserver(self, "_mouseMoved", "mouseMoved")
        addObserver(self, "_mouseDown", "mouseDown")

    def windowCloseCallback(self, sender):
        removeObserver(self, "mouseMoved")
        removeObserver(self, "mouseDown")
        super(ExampleWindow, self).windowCloseCallback(sender)

    def sliderCallback(self, sender):
        self.size = sender.get()
        self.w.canvas.update()

    def draw(self):
        x, y = self.offset
        translate(x, y)
        scale(self.size * 0.1)
        glyph = CurrentGlyph()
        if CurrentGlyph():
            drawGlyph(CurrentGlyph())

    def acceptsMouseMoved(self):
        return True

    def mouseDown(self, event):
        '''Mouse down that is not from observer. Gives NSEvent for click relative to window.'''
        # see https://developer.apple.com/documentation/appkit/nsevent?language=objc
        print(type(event))
        view = self.w.canvas.getNSView()
        print(view.convertPoint_fromView_(event.locationInWindow(), None))

    def _mouseDown(self, notification):
        '''Mouse down from observer. Gives info as dict.'''
        print(notification["event"])
        print(notification["glyph"])
        print(notification["point"])
        print()

    def mouseMoved(self, event):
        '''Mouse moved that is not from observer. Gives NSEvent for mouse relative to window.'''
        print(event.locationInWindow())

    def _mouseMoved(self, notification):
        '''Mouse moved from observer. Gives info as dict.'''
        print(notification)

    def mouseDragged(self, event):
        '''Grab and drag the canvas.'''
        x, y = self.offset
        self.offset = x + event.deltaX(), y - event.deltaY()
        self.w.canvas.update()

ExampleWindow()