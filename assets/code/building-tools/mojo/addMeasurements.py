'''Add a measurement line to the current glyph.'''

from AppKit import NSPoint
from mojo.events import MeasurementTool

measurement = MeasurementTool.measurementClass()
measurement.startPoint = NSPoint(-100, 100)
measurement.endPoint = NSPoint(1000, 200)

g = CurrentGlyph()
g.naked().measurements.append(measurement)
g.changed()