from vanilla import FloatingWindow, CheckBox
from defconAppKit.windows.baseWindow import BaseWindowController
from mojo.events import installTool, uninstallTool, getToolOrder

class MyToolActivator(BaseWindowController):

    def __init__(self, tool):
        self.w = FloatingWindow((123, 44), 'MyTool')
        self.w.button = CheckBox((10, 10, -10, 24), 'activate', value=True, callback=self.activateToolCallback)
        self.tool = tool
        # install the tool
        installTool(self.tool)
        # initialize the window
        self.setUpBaseWindowBehavior()
        self.w.open()

    def activateToolCallback(self, sender):
        # if the tool is not installed, install it
        if sender.get():
            installTool(self.tool)
        # if the tool is installed, uninstall it
        else:
            uninstallTool(self.tool)

    def windowCloseCallback(self, sender):
        # if the tool is active, remove it
        tools = getToolOrder()
        if self.tool.__class__.__name__ in tools:
            uninstallTool(self.tool)
        super(MyToolActivator, self).windowCloseCallback(sender)

MyToolActivator(MyTool())