'''Set font infos from a dictionary into all open fonts.'''

# a dictionary with font infos
infoDict = {
    # attribute : data,
    'openTypeNameDesigner' : 'Claude Garamond',
    'year' : 1532,
    'copyright' : 'Copyright (c) 1532 Claude Garamond.',
    'note' : 'bonjour le monde',
}

# for each open font
for font in AllFonts():
    # iterate over all attributes and values
    for attr, value in infoDict.items():
        # set font info data
        setattr(font.info, attr, value)

# done!