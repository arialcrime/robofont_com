'''Set a fixed width to all selected glyphs in the current font. Enable undo/redo.'''

# define a width value
value = 400

# get the current font
font = CurrentFont()

# iterate over all selected glyphs in the font
for glyph in font.selectedGlyphs:

    # set undo state
    glyph.prepareUndo()

    # set glyph width
    glyph.width = value

    # restore undo state
    glyph.performUndo()