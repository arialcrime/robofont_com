'''
Batch generate OTFs from a folder of UFOs.

1. open a dialog to select a folder
2. open each UFO in folder (without the UI)
3. generate OpenType-CFF font from UFO

'''

import os
from vanilla.dialogs import getFolder

# select folder
folder = getFolder('Select a folder with UFOs')[0]

# iterate over all files in folder
for f in os.listdir(folder):

    # skip files which are not .ufos
    if os.path.splitext(f)[-1] != '.ufo':
        continue

    # get full ufo path
    ufoPath = os.path.join(folder, f)

    # open ufo (without UI) # use 'showUI' in RF 1.8
    font = OpenFont(ufoPath, showInterface=False)

    # make otf path
    otfPath = os.path.join(folder, f.replace('.ufo', '.otf'))

    # generate otf font
    font.generate(path=otfPath, format='otf', decompose=True, checkOutlines=True, releaseMode=True)

    # close the ufo
    font.close()