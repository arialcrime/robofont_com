'''Copy font info data from one open font to another.'''

# get the source font by name
f1 = AllFonts().getFontsByStyleName('Regular')[0]

# get the target font by name
f2 = AllFonts().getFontsByStyleName('Bold')[0]

# a list of font.info attributes to copy
attributes = [
    'familyName',
    'xHeight',
    'openTypeNameDesigner',
]

# copy font.info data from f1 to f2
for attr in attributes:
    # get value from source font
    value = getattr(f1.info, attr)
    # set value in target font
    setattr(f2.info, attr, value)